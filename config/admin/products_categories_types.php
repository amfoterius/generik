<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Типы категорий',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Тип',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product_category_type',


    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'name' => array(
            'title' => 'Название'
        ),
        'priority' => array(
            'title' => "Приоритет",
        )

    ),



    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text'
        ),
        'alias' => array(
            'title' => 'Alias',
            'type' => 'text'
        ),

        'priority' => [
            'type' => 'number',
            'title' => 'Priority',
        ],

        'image_banner' => array(
            'title' => 'Banner',
            'type' => 'image',
            'location' => public_path() . '/uploads/product_category_type-banners/',
            'naming' => 'random',
        ),

        'meta_title' => array(
            'title' => 'Meta title',
            'type' => 'text'
        ),
        'meta_description' => array(
            'title' => 'Meta description',
            'type' => 'text'
        ),
        'meta_keywords' => array(
            'title' => 'Meta keywords',
            'type' => 'text'
        ),



    ),

    'sort' => array(
        'field' => 'priority',
        'direction' => 'desc',
    ),

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,


];