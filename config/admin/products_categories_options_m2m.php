<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Опции категорий m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Опция m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product_category_option_m2m',


    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'category' => array(
            'title' => "Категория",
            'relationship' => 'category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),
        'option' => array(
            'title' => "Опция",
            'relationship' => 'option', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),
        'value' => array(
            'title' => 'Value'
        ),

    ),



    'edit_fields' => array(
        'category' => array(
            'type' => 'relationship',
            'title' => 'Категория',
            'name_field' => 'name',
        ),
        'option' => array(
            'type' => 'relationship',
            'title' => 'Опция',
            'name_field' => 'name',
        ),
        'value' => array(
            'title' => 'Value',
            'type' => 'textarea'
        ),


    ),

    'sort' => array(
        'field' => 'category',
        'direction' => 'asc',
    ),

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,


];