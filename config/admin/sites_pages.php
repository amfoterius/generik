<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 22.01.2016
 * Time: 16:20
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Страницы Сайта',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Страница',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Site\Site_page',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(

        'name' => array(
            'title' => 'Название'
        ),


    ),

    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text'
        ),


    ),


];