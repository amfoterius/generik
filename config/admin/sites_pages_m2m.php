<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 22.01.2016
 * Time: 16:20
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Страницы Сайта m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Страница m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Site\Site_page_m2m',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(

        'site' => array(
            'title' => "Сайт",
            'relationship' => 'site', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'page' => array(
            'title' => "Страница",
            'relationship' => 'page', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),


    ),

    'edit_fields' => array(
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),

        'page' => array(
            'type' => 'relationship',
            'title' => 'Страница',
            'name_field' => 'name',
        ),

        'meta_title' => array(
            'title' => 'Meta title',
            'type' => 'textarea'
        ),

        'meta_description' => array(
            'title' => 'Meta description',
            'type' => 'textarea'
        ),

        'meta_keywords' => array(
            'title' => 'Meta keywords',
            'type' => 'textarea'
        ),

        'text' => array(
            'title' => 'Text',
            'type' => 'textarea'
        ),

        'text_1' => array(
            'title' => 'Text 1',
            'type' => 'textarea'
        ),




    ),

    'filters' => [
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),
        'page' => array(
            'type' => 'relationship',
            'title' => 'Страница',
            'name_field' => 'name',
        ),
    ]


];