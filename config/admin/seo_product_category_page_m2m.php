<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Оптимизация страниц категорий m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Оптимизация Страницы m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Seo\Product_category_page_m2m',


    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'page_m2m_id' => [
            'title' => "Подстаницы ID",

        ],

        'category_name' => array(
            'title' => "Категория",
            'relationship' => 'page_m2m.category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),
        'page_name' => array(
            'title' => "Страница",
            'relationship' => 'page_m2m.page', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'site' => array(
            'title' => "Сайт",
            'relationship' => 'site', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),


    ),



    'edit_fields' => array(
        'meta_title' => array(
            'title' => 'Meta title',
            'type' => 'text'
        ),
        'meta_description' => array(
            'title' => 'Meta description',
            'type' => 'text'
        ),
        'meta_keywords' => array(
            'title' => 'Meta keywords',
            'type' => 'text'
        ),

    ),

    'filters' => array(
        'site' => array(
            'title' => 'Сайт',
            'type' => 'relationship',
            'name_field' => 'name',
        ),

        'page_m2m' => array(
            'title' => 'Подстраница',
            'type' => 'relationship',
            'name_field' => 'id',
        ),


    ),


    'sort' => array(
        'field' => 'site_id',
        'direction' => 'asc',
    ),

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 877,


];