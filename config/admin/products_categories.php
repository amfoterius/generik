<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Категории товаров',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Категория',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product_category',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 1000,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'id' => array(
            'title' => "ID",
        ),
        'img_big' => array(
            'title' => 'Image',
            'output' => '<img src="/files/images/product_category/img_big/(:value)" height="50" />',
        ),
        'name' => array(
            'title' => 'Название'
        ),
        'priority' => array(
            'title' => "Приоритет",
        )

    ),



    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text'
        ),
        'type' => array(
            'type' => 'relationship',
            'title' => 'Тип',
            'name_field' => 'name',
        ),
        'alias' => array(
            'title' => 'Alias',
            'type' => 'text'
        ),

        'seo_description' => array(
            'title' => 'Описание',
            'type' => 'textarea'
        ),

        'text' => array(
            'title' => 'Text',
            'type' => 'wysiwyg'
        ),

        'partners_frame_url' => array(
            'title' => 'Партнерка',
            'type' => 'text'
        ),


        'img_big' => array(
            'title' => 'Image Big',
            'type' => 'image',
            'location' => public_path() . '/files/images/product_category/img_big/',
            'naming' => 'random',
        ),

        'img_small' => array(
            'title' => 'Image Small',
            'type' => 'image',
            'location' => public_path() . '/files/images/product_category/img_small/',
            'naming' => 'random',
        ),

        'buy' => [
            'title' => 'В наличие',
            'type' => 'bool',
        ],

        /*

        'priority' => [
            'type' => 'number',
            'title' => 'Priority',
        ],

        'instruction' => array(
            'title' => 'Инструкция',
            'type' => 'textarea'
        ),

        'description' => array(
            'title' => 'Описание',
            'type' => 'textarea'
        ),

        /*
        'meta_title' => array(
            'title' => 'Meta title',
            'type' => 'text'
        ),
        'meta_description' => array(
            'title' => 'Meta description',
            'type' => 'text'
        ),
        'meta_keywords' => array(
            'title' => 'Meta keywords',
            'type' => 'text'
        ),
        */


    ),

    'sort' => array(
        'field' => 'api_id',
        'direction' => 'asc',
    ),

    'filters' => array(
        'type' => array(
            'type' => 'relationship',
            'title' => 'Type',
            'name_field' => 'name',
        ),
        'name' => array(
            'title' => 'Name',
        ),
    ),

    'query_filter'=> function($query)
    {
        //$query->where('api_id', '>=', '10000')->where('img_big', null);
    },



];