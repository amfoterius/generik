<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Заказы',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Заказ',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Order',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(



        'site' => array(
            'title' => "Сайт",
            'relationship' => 'site', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'api_id' => array(
            'title' => 'Номер в системе'
        ),

        'fio' => array(
            'title' => 'Имя покупателя'
        ),

        'tel' => array(
            'title' => 'Номер телефона'
        ),



    ),

    'edit_fields' => array(
        'email' => array(
            'title' => 'Email',
            'type' => 'text'
        ),



    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),
        'api_id' => array(
            'title' => 'Номер в системе',
        ),
    ),

    'action_permissions'=> array(
        'delete' => function($model)
        {
            return false;
        },

        'edit' => function($model)
        {
            return false;
        }
    ),

    'sort' => array(
        'field' => 'created_at',
        'direction' => 'desc',
    ),

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,


];