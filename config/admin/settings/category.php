<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 16.12.2015
 * Time: 16:21
 */

return array(
    /**
     * Settings page title
     *
     * @type string
     */
    'title' => 'Категория',
    /**
     * The edit fields array
     *
     * @type array
     */
    'edit_fields' => array(

        'meta_title' => array(
            'title' => 'Meta Title',
            'type' => 'textarea',
            'limit' => 500,
        ),

        'meta_description' => array(
            'title' => 'Meta Description',
            'type' => 'textarea',
            'limit' => 500,
        ),

        'meta_keywords' => array(
            'title' => 'Meta Keywords',
            'type' => 'textarea',
            'limit' => 500,
        ),

        'first_block' => array(
            'title' => 'First block',
            'type' => 'wysiwyg',

        ),









    ),

    'rules' => array(

    ),

);