<?php
/**
 * Created by PhpStorm.
 * User: �������������
 * Date: 02.12.2015
 * Time: 16:29
 */

return array(
    /**
     * Settings page title
     *
     * @type string
     */
    'title' => 'Site Settings',
    /**
     * The edit fields array
     *
     * @type array
     */
    'edit_fields' => array(
        'site_name' => array(
            'title' => 'Site Name',
            'type' => 'text',
            'limit' => 50,
        ),

    ),

    'rules' => array(
        'site_name' => 'required|max:50',
    ),

);