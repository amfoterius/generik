<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Товары',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Товар',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(


        'image' => array(
            'title' => 'Image',
            'output' => '<img src="/files/images/product/image/(:value)" height="50" />',
        ),

        'category' => array(
            'title' => "Категория",
            'relationship' => 'category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'name' => array(
            'title' => 'name'
        ),

        'kolvo' => array(
            'title' => 'Количество'
        ),

        'price_per_tablet' => array(
            'title' => 'Цена за штуку'
        ),

        'price_per_item' => array(
            'title' => 'Цена за упаковку'
        ),


    ),

    'edit_fields' => array(

        'category' => array(
            'type' => 'relationship',
            'title' => 'Категория',
            'name_field' => 'name',
        ),

        'name' => [
            'type' => 'text',
            'title' => 'name',


        ],

        'image' => array(
            'title' => 'Картинка',
            'type' => 'image',
            'location' => public_path() . '/files/images/product/image/',
            'naming' => 'random',
        ),

        'kolvo_int' => [
            'type' => 'number',
            'title' => 'Шт, число'


        ],

        'kolvo' => [
            'type' => 'text',
            'title' => 'Шт, прописью'


        ],

        'price_per_item' => array(
            'type' => 'number',
            'title' => 'Цена за штуку',
            'symbol' => 'р.', //optional, defaults to ''
            'decimals' => 2, //optional, defaults to 0
            'thousands_separator' => ',', //optional, defaults to ','
            'decimal_separator' => '.', //optional, defaults to '.'
        ),

        'price' => array(
            'type' => 'number',
            'title' => 'Цена',
            'symbol' => 'р.', //optional, defaults to ''
            'decimals' => 2, //optional, defaults to 0
            'thousands_separator' => ',', //optional, defaults to ','
            'decimal_separator' => '.', //optional, defaults to '.'
        ),

        'description' => array(
            'type' => 'wysiwyg',
            'title' => 'Описание',
        ),



        'publish' => array(
            'type' => 'bool',
            'title' => 'Публикация',
        ),


    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'id',
        'category' => array(
            'type' => 'relationship',
            'title' => 'Категория',
            'name_field' => 'name',
        ),
        'name' => array(
            'title' => 'Name',
        ),
    ),

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,


];