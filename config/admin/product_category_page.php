<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Опции категорий',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Опция',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product_category_page',


    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'name' => array(
            'title' => 'Название'
        ),
        'alias' => array(
            'title' => "Alias",
        )

    ),



    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text'
        ),

        'alias' => array(
            'title' => 'Alias',
            'type' => 'text'
        ),

        'base_content' => array(
            'title' => 'Base content',
            'type' => 'textarea'
        ),



    ),



    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 1000,


];