<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Картинки категорий товаров',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Картинка',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product_category_image',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(

        'category' => array(
            'title' => "Категория",
            'relationship' => 'category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'image' => array(
            'title' => 'Image',
            'output' => '<img src="/files/images/product_category_image/image/(:value)" height="50" />',
        ),

        'priority' => array(
            'title' => "Приоритет",
        )


    ),



    'edit_fields' => array(

        'category' => array(
            'type' => 'relationship',
            'title' => 'Категория',
            'name_field' => 'name',
        ),


        'image' => array(
            'title' => 'Картинка',
            'type' => 'image',
            'location' => public_path() . '/files/images/product_category_image/image/',
            'naming' => 'random',
        ),

        'priority' => [
            'type' => 'number',
            'title' => 'Приоритет',
        ],




    ),



];