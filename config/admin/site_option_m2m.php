<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 22.01.2016
 * Time: 16:20
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Опции сайтов m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Опция сайта m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Site\Site_option_m2m',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(

        'site' => array(
            'title' => "Сайт",
            'relationship' => 'site', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'option' => array(
            'title' => "Опция",
            'relationship' => 'option', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),


    ),

    'edit_fields' => array(
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),

        'option' => array(
            'type' => 'relationship',
            'title' => 'Опция',
            'name_field' => 'name',
        ),

        'value' => array(
            'title' => 'Значение',
            'type' => 'textarea'
        ),




    ),


    'filters' => [
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),
        'option' => array(
            'type' => 'relationship',
            'title' => 'Опция',
            'name_field' => 'name',
        ),
        'value' => array(
            'title' => 'Значение',
        ),
    ]


];