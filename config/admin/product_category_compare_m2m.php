<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 22.01.2016
 * Time: 16:20
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Категории для сравнения m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Категория для сравнения m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product_category_compare_m2m',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'base_category' => array(
            'title' => "Category",
            'relationship' => 'base_category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'compare_category' => array(
            'title' => "Compare",
            'relationship' => 'compare_category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'priority' => [
            'title' => "Priority",

        ],




    ),

    'edit_fields' => array(

        'base_category' => array(
            'type' => 'relationship',
            'title' => 'Category',
            'name_field' => 'name',
        ),

        'compare_category' => array(
            'type' => 'relationship',
            'title' => 'Compare',
            'name_field' => 'name',
        ),

        'priority' => array(
            'type' => 'number',
            'title' => 'Priority',
            'decimals' => 0, //optional, defaults to 0
        )

    ),


    'filters' => [
        'base_category' => array(
            'type' => 'relationship',
            'title' => 'Category',
            'name_field' => 'name',
        ),
        'compare_category' => array(
            'type' => 'relationship',
            'title' => 'Compare',
            'name_field' => 'name',
        ),

    ],

    'sort' => array(
        'field' => 'priority',
        'direction' => 'desc',
    ),


];