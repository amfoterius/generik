<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Опции отзывов',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Опция',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Review_option',


    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'name' => array(
            'title' => 'Название'
        ),
        'priority' => array(
            'title' => "Приоритет",
        )

    ),



    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text'
        ),

        'priority' => [
            'type' => 'number',
            'title' => 'Priority',
        ],
    ),

    'sort' => array(
        'field' => 'priority',
        'direction' => 'desc',
    ),

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,


];