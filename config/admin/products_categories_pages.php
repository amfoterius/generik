<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    'title' => 'Подстраницы категории',


    'single' => 'Страница',


    'model' => 'App\Product_category_page',


    'columns' => array(

        'name' => array(
            'title' => 'Название'
        ),

        'alias' => array(
            'title' => 'Alias'
        ),


    ),

    'edit_fields' => array(

        'name' => array(
            'type' => 'text',
            'title' => 'Name',
        ),

        'alias' => array(
            'type' => 'text',
            'title' => 'Alias',
        ),

        'priority' => [
            'type' => 'number',
            'title' => 'Priority',
        ],


    ),

    'sort' => array(
        'field' => 'priority',
        'direction' => 'desc',
    ),


    'form_width' => 600,


];