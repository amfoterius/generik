<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 22.01.2016
 * Time: 16:20
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Категории сайты m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Категория сайты m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product_category_site_m2m',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(

        'site' => array(
            'title' => "Сайт",
            'relationship' => 'site', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'category' => array(
            'title' => "Категория",
            'relationship' => 'category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'updated_at' => [
            'title' => 'Дата обновления'
        ]


    ),

    'edit_fields' => array(
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),

        'category' => array(
            'type' => 'relationship',
            'title' => 'Страница',
            'name_field' => 'name',
        ),

        'description' => array(
            'title' => 'description',
            'type' => 'textarea'
        ),

        'text' => array(
            'title' => 'Text',
            'type' => 'textarea'
        ),

        'video' => array(
            'title' => 'Видео',
            'type' => 'textarea'
        ),

        'video_url' => array(
            'title' => 'Ссылка видео',
            'type' => 'text'
        ),


        'video_description' => array(
            'title' => 'Описание видео',
            'type' => 'textarea'
        ),

        'video_duration' => array(
            'title' => 'Продолжительность видео',
            'type' => 'text'
        ),


        'video_image' => array(
            'title' => 'Изображение',
            'type' => 'image',
            'location' => public_path() . '/uploads/product_category_site_m2m-video_image/',
            'naming' => 'random',
        ),

        'banner_image' => array(
            'title' => 'Изображение',
            'type' => 'image',
            'location' => public_path() . '/uploads/product_category_site_m2m-banner_image/',
            'naming' => 'random',
        ),




    ),

    'filters' => [
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),
        'category' => array(
            'type' => 'relationship',
            'title' => 'Категория',
            'name_field' => 'name',
        ),
    ]


];