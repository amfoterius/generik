<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 22.01.2016
 * Time: 16:20
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Товары в группе m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Товар в группе m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product_category_group_list_m2m',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'group' => array(
            'title' => "Group",
            'relationship' => 'group', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'category' => array(
            'title' => "Category",
            'relationship' => 'category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),
        'priority' => [
            'title' => "Priority",

        ],


    ),

    'edit_fields' => array(
        'group' => array(
            'type' => 'relationship',
            'title' => 'Group',
            'name_field' => 'name',
        ),

        'category' => array(
            'type' => 'relationship',
            'title' => 'Category',
            'name_field' => 'name',
        ),

        'priority' => array(
            'type' => 'number',
            'title' => 'Priority',
            'decimals' => 0, //optional, defaults to 0
        )





    ),


    'filters' => [
        'group' => array(
            'type' => 'relationship',
            'title' => 'Group',
            'name_field' => 'name',
        ),
        'category' => array(
            'type' => 'relationship',
            'title' => 'Category',
            'name_field' => 'name',
        ),

    ],

    'sort' => array(
        'field' => 'priority',
        'direction' => 'desc',
    ),


];