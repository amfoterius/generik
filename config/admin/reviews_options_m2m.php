<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Опции категорий m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Опция m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Review_option_m2m',


    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'review' => array(
            'title' => "Отзыв",
            'relationship' => 'review', //this is the name of the Eloquent relationship method!
            'select' => "(:table).product_id",
        ),
        'option' => array(
            'title' => "Категория",
            'relationship' => 'option', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),
        'stars' => array(
            'title' => 'Value'
        ),

    ),



    'edit_fields' => array(
        'review' => array(
            'type' => 'relationship',
            'title' => 'Отзыв',
            'name_field' => 'id',
        ),
        'option' => array(
            'type' => 'relationship',
            'title' => 'Опция',
            'name_field' => 'name',
        ),
        'stars' => array(
            'title' => 'Value',
            'type' => 'text'
        ),


    ),


    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,


];