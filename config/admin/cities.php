<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Города',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Город',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\City',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(


        'name' => array(
            'title' => 'Количество'
        ),

        'alias' => array(
            'title' => 'Alias'
        ),



    ),

    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text'
        ),

        'name_r' => array(
            'title' => 'Name R',
            'type' => 'text'
        ),

        'name_v' => array(
            'title' => 'Name V',
            'type' => 'text'
        ),

        'name_p' => array(
            'title' => 'Name P',
            'type' => 'text'
        ),

        'alias' => array(
            'title' => 'Alias',
            'type' => 'text'
        ),



    ),




];