<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Страницы категорий m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Страница m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Product_category_page_m2m',


    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'site' => array(
            'title' => "Категория",
            'relationship' => 'site', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),
        'category' => array(
            'title' => "Категория",
            'relationship' => 'category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),
        'page' => array(
            'title' => "Опция",
            'relationship' => 'page', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'updated_at' => [
            'title' => 'Обновлден'
        ]

    ),



    'edit_fields' => array(
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),
        'category' => array(
            'type' => 'relationship',
            'title' => 'Категория',
            'name_field' => 'name',
        ),
        'page' => array(
            'type' => 'relationship',
            'title' => 'Page',
            'name_field' => 'name',
        ),
        'content' => array(
            'title' => 'Content',
            'type' => 'textarea'
        ),

        'meta_title' => array(
            'title' => 'Meta title',
            'type' => 'text'
        ),
        'meta_description' => array(
            'title' => 'Meta description',
            'type' => 'text'
        ),
        'meta_keywords' => array(
            'title' => 'Meta keywords',
            'type' => 'text'
        ),


    ),

    'sort' => array(
        'field' => 'updated_at',
        'direction' => 'desc',
    ),

    'filters' => [
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),
        'category' => array(
            'type' => 'relationship',
            'title' => 'Категория',
            'name_field' => 'name',
        ),
        'page' => array(
            'type' => 'relationship',
            'title' => 'Page',
            'name_field' => 'name',
        ),
    ],

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 877,


];