<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 22.01.2016
 * Time: 16:20
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Сайты',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Сайт',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Site\Site',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(

        'name' => array(
            'title' => 'Название'
        ),

        'url' => array(
            'title' => 'URL'
        ),

        'url_local' => array(
            'title' => 'URL local'
        ),

    ),

    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text'
        ),

        'url' => array(
            'title' => 'URL',
            'type' => 'text'
        ),

        'url_local' => array(
            'title' => 'URL local',
            'type' => 'text'
        ),
        'template' => array(
            'title' => 'Template',
            'type' => 'number'
        ),
        'icon' => array(
            'title' => 'Icon',
            'type' => 'image',
            'location' => public_path() . '/favicons/',
            'naming' => 'random',
        ),

        'logo' => array(
            'title' => 'Logo',
            'type' => 'image',
            'location' => public_path() . '/logos/',
            'naming' => 'random',
        ),

        'metrika' => array(
            'title' => 'Код метрики',
            'type' => 'textarea'
        ),

        'search_code' => array(
            'title' => 'Код поиска',
            'type' => 'textarea'
        ),

        'video_delivery_code' => array(
            'title' => 'Код видео доставка',
            'type' => 'textarea'
        ),

    ),


];