<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 22.01.2016
 * Time: 16:20
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Банеры сайтов m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Банер сайта m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Site\Site_banner_m2m',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'image' => array(
            'title' => 'Image',
            'output' => '<img src="/uploads/sites/banners/(:value)" height="50" />',
        ),

        'site' => array(
            'title' => "Сайт",
            'relationship' => 'site', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'banner' => array(
            'title' => "Банер",
            'relationship' => 'banner', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),


    ),

    'edit_fields' => array(
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),

        'banner' => array(
            'type' => 'relationship',
            'title' => 'Баннер',
            'name_field' => 'name',
        ),

        'image' => array(
            'title' => 'Изображение',
            'type' => 'image',
            'location' => public_path() . '/uploads/sites/banners/',
            'naming' => 'random',
        ),
        'image_m' => array(
            'title' => 'Изображение Моб',
            'type' => 'image',
            'location' => public_path() . '/uploads/sites/banners-m/',
            'naming' => 'random',
        ),



    ),


    'filters' => [
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),
        'banner' => array(
            'type' => 'relationship',
            'title' => 'Банер',
            'name_field' => 'name',
        ),

    ]


];