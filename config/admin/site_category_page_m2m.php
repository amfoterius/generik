<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Cтраницы категорий m2m',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'Cтраницы категорий m2m',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Site\Site_category_page_m2m',


    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(

        'site' => array(
            'title' => "Сайт",
            'relationship' => 'site', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'page' => array(
            'title' => "Подстраница",
            'relationship' => 'page', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'updated_at' => [
            'title' => "Дата обновления",
        ]


    ),



    'edit_fields' => array(
        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name',
        ),

        'page' => array(
            'type' => 'relationship',
            'title' => 'Подстраница',
            'name_field' => 'name',
        ),

        'base_text' => array(
            'type' => 'textarea',
            'title' => 'Текст',
        ),

        'meta_title' => array(
            'title' => 'Meta title',
            'type' => 'textarea'
        ),
        'meta_description' => array(
            'title' => 'Meta description',
            'type' => 'textarea'
        ),
        'meta_keywords' => array(
            'title' => 'Meta keywords',
            'type' => 'textarea'
        ),

    ),

    'filters' => array(
        'site' => array(
            'title' => 'Сайт',
            'type' => 'relationship',
            'name_field' => 'name',
        ),

        'page' => array(
            'title' => 'Подстраница',
            'type' => 'relationship',
            'name_field' => 'id',
        ),


    ),


    'sort' => array(
        'field' => 'site_id',
        'direction' => 'asc',
    ),

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 877,


];