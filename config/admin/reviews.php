<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.10.2015
 * Time: 2:19
 */

return [

    'title' => 'Отзывы',


    'single' => 'Отзыв',


    'model' => 'App\Review',


    'columns' => array(

        'category' => array(
            'title' => "Category",
            'relationship' => 'category', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'name' => array(
            'title' => 'Name',
            'type' => 'text'
        ),


    ),

    'edit_fields' => array(

        'created_at' => array(
            'type' => 'date',
            'title' => 'Date',
            'date_format' => 'yy-mm-dd', //optional, will default to this value
        ),

        'name' => array(
            'type' => 'text',
            'title' => 'Name',
        ),

        'site' => array(
            'type' => 'relationship',
            'title' => 'Сайт',
            'name_field' => 'name'
        ),

        'category' => array(
            'type' => 'relationship',
            'title' => 'Категория',
            'name_field' => 'name'
        ),
        'positive' => array(
            'type' => 'textarea',
            'title' => 'positive',
        ),
        'negative' => array(
            'type' => 'textarea',
            'title' => 'negative',
        ),
        'text' => array(
            'type' => 'textarea',
            'title' => 'text',
        ),

        'publish' => array(
            'type' => 'bool',
            'title' => 'publish',
        )




    ),

    'sort' => array(
        'field' => 'publish',
        'direction' => 'asc',
    ),


    'form_width' => 800,


];