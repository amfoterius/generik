$ ->

  $.ajax
    url: URL + 'ajax/compare/getOnCompare'
    method: 'GET'

    success: (data)->
      for n in data
        $("#compare-" + n).addClass("hide")
        $("#compare-" + n + "-active").removeClass("hide")


  # add to cart

  $(".add-to-cart").on "click", (e) ->
    c = $("#cart-count").text();
    $("#on-cart-img").attr("src", "")
    el = $(this)

    $("#cart-count, #cart-count-mobile").text(parseInt(c) + 1)
    .animate(
      "font-size": "25px"
      "background-color": "#5DBA5D"

    , 500)

    .animate(
      "font-size": "12px"
      "background-color": "#777777"

    , 100)


    $.ajax
      url: URL + 'ajax/cart/add'
      method: 'POST'
      data:
        "product_id": $(this).data('id')
        "product_count": 1
        "is_one_click": 0

      success: (data)->
        $("#on-cart-img").attr("src", URL + "uploads/api/images/categories/" + el.data("img-src"))
        $("#on-cart-name").text(el.data("name"))
        $("#on-cart-kolvo").text(el.data("kolvo"))
        $("#on-cart-price").text(el.data("price"))

        summ = 0

        for i, p of data.products

          summ = summ + parseInt(p)

        $("#on-carl-length").text(summ)


  $(".add-to-cart-product").on "click", (e) ->
    c = $("#cart-count").text();
    $("#on-cart-img").attr("src", "")
    el = $(this)

    $("#cart-count, #cart-count-mobile").text(parseInt(c) + 1)
    .animate(
      "font-size": "25px"
      "background-color": "#5DBA5D"

    , 500)

    .animate(
      "font-size": "12px"
      "background-color": "#777777"

    , 100)


    $.ajax
      url: URL + 'ajax/cart/add'
      method: 'POST'
      data:
        "product_id": $(this).data('id')
        "product_count": 1
        "is_one_click": 0

      success: (data)->
        $("#on-cart-img").attr("src", URL + "uploads/api/images/categories/" + el.data("img-src"))
        $("#on-cart-name").text(el.data("name"))
        $("#on-cart-kolvo").text(el.data("kolvo"))
        $("#on-cart-price").text(el.data("price"))

        summ = 0

        for i, p of data.products

          summ = summ + parseInt(p)

        $("#on-carl-length").text(summ)

  # remove on cart
  $(".remove-on-cart").on "click", (e) ->
    tr = $(this).parent().parent()

    $.ajax
      url: URL + 'ajax/cart/remove'
      method: 'POST'
      data:
        "product_id": $(this).data('product-id')
      success: ->
        tr.remove()
        summ -= parseInt(tr.find('td .summ').text())
        summDiscount = summ * 0.05
        $("#summ-discount").text(summDiscount)


        if $('.payment input:checked').data('discount') == 1
          $("#row-discount").removeClass('hide')
          $('#cart-summ').text(summ - summDiscount)
        else
        $("#row-discount").addClass('hide')
        $('#cart-summ').text(summ)


  $("#select-delivery").change((e) ->
    isSelect = false

    dId = parseInt($(this).val())
    $("#select-billing").show()
    $("#select-billing option").each((index) ->
      if parseInt($(this).data('delivery-id')) == dId

        if !isSelect
          $(this).attr('selected', true)
          isSelect = true

        $(this).show()

      else
        $(this).hide()
    )
  )

  $("#form-order").on('submit', (e) ->

    mess = ''
    error = false

    if $("input[name='fio']").val() == ''
      $("input[name='fio']").css('border-color', 'red')
      mess += 'Не заполнено поле ФИО<br>'
    else
      $("input[name='fio']").css('border-color', 'rgb(204, 204, 204)')


    if $("input[name='tel']").val() == ''
      $("input[name='tel']").css('border-color', 'red')
      mess += 'Не заполнено поле ФИО<br>'
    else
      $("input[name='tel']").css('border-color', 'rgb(204, 204, 204)')


    if error
      e.preventDefault()
      $("#modal-error-text").html(mess)
      $("#modal-error").modal('show');
      return false


  )


  $(".buy-for-one-click").on "click", (e) ->
    modal = $("#buy-for-one-click");

    if parseInt($(this).data('id')) > 0
      $("#buy-for-one-click").find(".input-product-id").val($(this).data('id'))
      $("#buy-for-one-click").find(".product-name").text($(this).data('name') + ", " + $(this).data('kolvo'))


  $(".next-chunk").on "click", (e) ->
    $(this).hide()



  $('#button-new-order').click ->
    msg = $('#form-order').serialize()


    $.ajax
      url: URL + 'ajax/cart/new_order'
      method: 'POST'
      data:
        msg


  $(".compare").on "click", (e) ->
    if $(this).hasClass('active')

      c = $("#compare-count").text();
      $('#compare-' + $(this).data('id') + '-active').addClass('hide');
      $('#compare-' + $(this).data('id')).removeClass('hide');


      $("#compare-count, #compare-count-mobile").text(parseInt(c) - 1)
      .animate(
        "font-size": "25px"
        "background-color": "#5DBA5D"

      , 500)

      .animate(
        "font-size": "12px"
        "background-color": "#777777"

      , 100)

      $.ajax
        url: URL + 'ajax/compare/remove'
        method: 'POST'
        data:
          id: $(this).data('id')



    else
      c = $("#compare-count").text();
      linkCompare = URL + "сравнение"


      $('#compare-' + $(this).data('id') + '-active').removeClass('hide');
      $('#compare-' + $(this).data('id')).addClass('hide');

      el = $(this)

      $("#compare-count, #compare-count-mobile").text(parseInt(c) + 1)
      .animate(
        "font-size": "25px"
        "background-color": "#5DBA5D"

      , 500)

      .animate(
        "font-size": "12px"
        "background-color": "#777777"

      , 100)

      $.ajax
        url: URL + 'ajax/compare/add'
        method: 'POST'
        data:
          id: $(this).data('id')

        success: (data) ->
          imgUrl = URL + "uploads/api/images/categories/" + el.data("img-src")
          name = el.data('name')
          el = "
                  <div class='alert alert-info alert-dismissable'>
                      <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                      <img src='#{imgUrl}' alt='' style='width: 50px'>
                      Вы добавили к сравнению #{name}
                  </div>"

          $(el).prependTo("#alerts").animate(
            {
              opacity: "hide",
              height: "hide"
            },
            3000)

          console.log(el)



  $('.payment').on "click", (e) ->
    $('.payment input').prop('checked', false)


    $(this).find('input').prop('checked', true)



    if $('.payment input:checked').data('discount') == 1
      $("#row-discount").removeClass('hide')
      $('#cart-summ').text(summ - summDiscount)
    else
      $("#row-discount").addClass('hide')
      $('#cart-summ').text(summ)



  $('.review-vote').on('click', (e) ->

    count = parseInt($(this).data('count'))
    if $(this).hasClass('btn-success')
      $(this).removeClass('btn-success').addClass('btn-default')
      count -= 1;

    else
      $(this).removeClass('btn-default').addClass('btn-success')
      count += 1;

    $(this).data('count', count).find('.count').text(count)

    $.ajax
      url: URL + 'ajax/review/vote?review_id=' + $(this).data('review')
      method: 'GET'

      success: (data) ->
        console.log(data)
  )



  $('.review-category-page-vote').on('click', (e) ->

    count = parseInt($(this).data('count'))
    console.log($(this).data('count'))
    if $(this).hasClass('btn-success')
      $(this).removeClass('btn-success').addClass('btn-default')
      count -= 1;

    else
      $(this).removeClass('btn-default').addClass('btn-success')
      count += 1;

    $(this).data('count', count).find('.count').text(count)

    $.ajax
      url: URL + 'ajax/review/vote_category_page?review_id=' + $(this).data('review')
      method: 'GET'

      success: (data) ->
        console.log(data)

  )


  $('#new-review-page-add').on('click', (e) ->

    $.ajax
      url: URL + 'ajax/review/add_review_category_page'
      method: 'POST'
      data:
        category_page_id: $(this).data('category-page-id'),
        site_id: $(this).data('site-id'),
        name: $("#new-review-page-name").val(),
        text: $("#new-review-page-text").val()

      success: (data) ->
        $("#new-review-page-name").val('')
        $("#new-review-page-text").val('')
        $("#reviews").prepend(data.block)




  )



