@extends('base')




@section('header')

@endsection

@section('breadcrumb')

    <ol class="breadcrumb">
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="[domain]каталог" itemprop="url">
                <span itemprop="title">Каталог</span>
            </a>
        </li>
        <li class="active">
            [curr_type.name]
        </li>
    </ol>

@endsection


@section('content')
    <div class="container">
        <div class="row">

            <div id="content" class="col-sm-12">
                <h1>{{ $curr_type->name or 'Каталог' }} {{ config('api.none_city') ? "" : "в [curr_city.name_p]" }}</h1>
                <hr>
                <!--<p><a href="{{ url("") }}" id="compare-total">Сравнение товаров (0)</a></p>-->
                <br>

                <div class="row" itemscope="" itemtype="http://schema.org/ItemList">
                    <meta itemprop="numberOfItems" content="{{ count($curr_type->categories) }}">
                    @foreach($curr_type->categories as $category)
                        @include('components.category_block_large', ['category' => $category, 'compare'=>$compare])
                    @endforeach
                    <div class="clearfix visible-lg"></div>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"></div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center" style="font-weight: bolder; font-size: 110%;">
                        Понравилось у нас? Поделись с друзьями
                        <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
                        <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                        <div class="ya-share2" style="margin-top: 5px;" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp"></div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection




@section('footer')
    @parent
@endsection