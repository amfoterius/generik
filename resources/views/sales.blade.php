@extends('base')

@section('header')
    @parent
@endsection


@section('content')
    <div class="container" style="margin-top: 60px">
        <div class="post_content entry-content">
            <div class="h1 text-center">Скидки и предложения</div>
            <div class="row">
                <div class="panel panel-success">
                    <div class="panel-body">
                        <i class="glyphicon glyphicon-gift"></i>
                        При покупке 20 таблеток, 2 таблетки Виагры в подарок
                    </div>
                </div>

                <div class="panel panel-success" style="margin-top: 60px">
                    <div class="panel-body">
                        <i class="glyphicon glyphicon-ok-sign"></i>
                        При покупке 50 таблеток, доставка бесплатно
                    </div>
                </div>

            </div>


        </div>
    </div>


@endsection