<h1 class="page-heading product-listing">
    <span class="cat-name">{{ $curr_category->name }}&nbsp;</span>
    <span class="heading-counter"> {{ $products->total() }} товаров.</span>
</h1>

<div id="subcategories">
    <p class="subcategory-heading h3">Подкатегории</p>
    <ul class="clearfix">
        @foreach($curr_category->childrens as $children)
            <li class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img data-src="holder.js/300x200" style="width: 300px; height: 200px" alt="...">

                    <div class="caption">
                        <h3>Ярлык эскиза</h3>

                        <p>...</p>

                        <p><a href="#" class="btn btn-primary" role="button">Кнопка</a> <a href="#"
                                                                                           class="btn btn-default"
                                                                                           role="button">Кнопка</a></p>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>

</div>


@include('components.products_filter_row', ['products'=>$products])

<ul class="product_list grid row">

    @foreach($products as $product)

    @endforeach

</ul>


@include('components.products_filter_row', ['products'=>$products])