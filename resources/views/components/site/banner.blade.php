@if(!is_null($banner) && !empty($banner->image_m))

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 not-mobile banner" style="margin-top: 60px; margin-bottom: 0px">
        <img src="[domain]{{ $banner->getImageUrl('image') }}" width="100%"
             alt="">
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 only-mobile banner" style="margin-top: 60px; margin-bottom: 0px">
        <img src="[domain]{{ $banner->getImageUrl('image_m') }}" width="100%"
             alt="">
    </div>

@elseif(!is_null($banner))

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner" style="margin-top: 60px; margin-bottom: 0px">
        <img src="[domain]{{ $banner->getImageUrl('image') }}" width="100%"
             alt="">
    </div>

@endif