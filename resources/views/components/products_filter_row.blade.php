<div class="content_sortPagiBar clearfix">
    <div class="sortPagiBar clearfix">

        <!--
        <ul class="display hidden-xs">
            <li class="display-title">View:</li>
            <li id="grid" class="selected">
                <a rel="nofollow" href="{{ url('') }}" title="Grid">
                    <i class="icon-th-large"></i>
                    Grid
                </a>
            </li>
            <li id="list">
                <a rel="nofollow" href="{{ url('') }}" title="List">
                    <i class="icon-th-list"></i>
                    List
                </a>
            </li>
        </ul>
        -->


        <form id="productsSortForm" action="{{ url('') }}" class="productsSortForm">
            <div class="select selector1">
                <label for="selectProductSort">Сортировать по</label>

                <div class="selector" id="uniform-selectProductSort" style="width: 192px;"><span
                            style="width: 180px; -webkit-user-select: none;">По названию товара, от А до Я</span><select
                            id="selectProductSort" class="selectProductSort form-control">
                        <option value="name:asc" selected="selected">--</option>
                        <option value="price:asc">Цена, по возрастанию</option>
                        <option value="price:desc">Цена, по убыванию</option>
                        <option value="name:asc" selected="selected">По названию товара, от А до Я
                        </option>
                        <option value="name:desc">По названию товара, от Я до А</option>
                        <option value="quantity:desc">По наличию</option>
                        <option value="reference:asc">Артикул, по возрастанию</option>
                        <option value="reference:desc">Артикул, по убыванию</option>
                    </select></div>
            </div>
        </form>


    </div>
    <div class="top-pagination-content clearfix">
        <form method="post" action="{{ url('') }}" class="compare-form">

            <!--
            <button type="submit" class="btn btn-default button button-medium bt_compare bt_compare" disabled="disabled">
                <span>Сравнить
                (
                <strong class="total-compare-val">0</strong>
                )
                <i class="icon-chevron-right right"></i>
                </span>
            </button>
            -->


            <input type="hidden" name="compare_product_count" class="compare_product_count"
                   value="0">
            <input type="hidden" name="compare_product_list" class="compare_product_list" value="">
        </form>

        <div id="pagination" class="pagination clearfix">

            {!! $products->render() !!}

        </div>
        <div class="product-count">
            Показано
            {{ ($products->currentPage() - 1) * 12 + 1 }}
            -
            {{ ($products->currentPage()) * 12 + 1 <= $products->total() ? ($products->currentPage()) * 12 + 1 : $products->total()}}
            из
            {{ $products->total() }} товаров
        </div>

    </div>
</div>