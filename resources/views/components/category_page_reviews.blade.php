
<div id="reviews">
    @foreach($curr_category_page->reviews()->where('publish', 1)->orderBy('created_at', 'desc')->get() as $review)
        @include('components.page_review_block', [
        'review' => $review,
         'category_page_review_likes' => $session_category_page_review_likes,
         ])
    @endforeach
</div>
