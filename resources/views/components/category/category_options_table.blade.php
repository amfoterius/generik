<table class="table table-hover">
    <thead>

    </thead>


    <tbody>
    <tr>
        <td>
            <strong>Рейтинг</strong>
        </td>
        <td class="">
            @include('components.rating_stars', ['rating' => $category->avg_rating()])
        </td>
    </tr>
    @foreach($category->options_m2m()->whereHas('option', function($query){

            $query->where('publish', 1);

    })->get() as $option_m2m)


        <tr>
            <td>
                <strong>{{ $option_m2m->option->name }}</strong>
            </td>
            <td class="">
                {!! $option_m2m->value !!}
            </td>
        </tr>

    @endforeach
    <tr>
        <td>
            <strong>Наличие</strong>
        </td>
        <td class="">

            @if($category->buy)
                <span class="text-success text-justify"><i class="glyphicon glyphicon-ok"></i> Есть в наличии</span>

            @else
                <span class="text-danger text-justify"><i class=""></i> Нет в наличии</span>
            @endif
        </td>
    </tr>
    </tbody>
</table>