<div class="products-grid" itemprop="offers" itemscope
     itemtype="http://schema.org/AggregateOffer">

    <?php $req_for_items = $category->products()->select(DB::raw('max(price_per_item) as max_price, min(price_per_item) as min_price, count(*) as c'))->first() ?>
    <meta itemprop="lowPrice" content="{{ $req_for_items->min_price }}">
    <meta itemprop="highPrice" content="{{ $req_for_items->max_price }}">
    <meta itemprop="offerCount" content="{{ $req_for_items->c }}">
    <meta itemprop="priceCurrency" content="RUB">


    <table class="cart_table">
        <thead>
        <tr>
            <th width="20%">Кол-во</th>
            <th width="15%">Цена <br>за ед.</th>
            <th width="15%">Цена</th>
            <th width="10%">Бонусы</th>
            <th width="30%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($category->products as $product)
            <tr itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <meta itemprop="priceCurrency" content="RUB">
                <td>{{ $product->kolvo }}</td>
                <td>
                    @if($product->price_per_item < $req_for_items['max_price'])
                        <strong style="text-decoration: line-through">
                            {{ number_format($req_for_items['max_price'], 0, ",", " " ) }}
                        </strong>
                        <br>
                        <strong style="color: red">
                            {{ number_format($product->price_per_item, 0, ",", " " ) }}
                            р
                        </strong>


                    @else
                        <strong>
                            {{ number_format($product->price_per_item, 0, "," , " " ) }}
                            р
                        </strong>

                    @endif
                </td>
                <td>
                    @if($product->price_per_item < $req_for_items['max_price'])
                        <strong style="text-decoration: line-through">
                            {{ number_format($req_for_items['max_price'] * $product->kolvo, 0, "," , " " ) }}
                        </strong>
                        <br>
                        <strong style="color: red" itemprop="price">
                            {{ number_format($product->price, 0, "," , " " ) }} р
                        </strong>

                    @else
                        <strong itemprop="price">
                            {{ number_format($product->price, 0, "," ,  " ") }} р
                        </strong>
                    @endif
                </td>
                <td class="product-icons-line-mini">

                    @if($category->buy && $product->kolvo_int >= 50)
                        <span class="delivery active"
                              data-help="Бесплатная доставка"></span>
                    @endif

                    @if($category->buy && $product->kolvo_int >= 20)
                        <span class="present active"
                              data-help="Подарок: 2 таб Виагры"></span>
                    @endif

                    @if(!$category->buy)
                        Нет в наличии
                        <a href="{{ is_null($page_analogs = $category->pages_m2m()->where('page_id', 3)->first()) ? '#аналоги' : $page_analogs->getUrl() }}"
                           class="btn btn-primary">Аналоги</a>


                    @endif


                </td>
                <td>

                    <a class="btn btn-success btn-sm add-to-cart {{ $category->buy ? "" : "disabled" }}"
                       data-id="{{ $product->id }}"
                       data-toggle="modal"
                       data-target="#on-cart"
                       data-img-src="{{ $category->img_big }}"
                       data-name="{{ $category->name }}"
                       data-price="{{ $product->price }}"
                       data-kolvo="{{ $product->kolvo }}">
                        <i class="glyphicon glyphicon-plus"></i>
                        Купить
                    </a>
                    <a class="btn btn-primary btn-sm buy-for-one-click {{ $category->buy ? "" : "disabled" }}"
                       data-toggle="modal"
                       data-target="#buy-for-one-click"
                       data-id="{{ $product->id }}"
                       data-name="{{ $product->name }}"
                       data-kolvo="{{ $product->kolvo  }}">
                        <i class="glyphicon glyphicon-hand-up"></i>
                        <span>В один клик</span>
                    </a>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>