<div class="sidebar affix hidden-xs hidden-sm" data-spy="affix" role="complementary"
     style="background: aliceblue; border-radius: 5px; padding: 10px 0 10px 0;" data-offset-top="200">
    <div class="row" style="padding-left: 38px;">Навигация по странице</div>
    <ul class="nav bs-docs-sidenav">
        <li style="border-top: 1px solid #ccc;">
            <a class="nav-page" href="#купить">
                <i class="glyphicon glyphicon-shopping-cart"></i>
                Купить
            </a>
        </li>

        @if(!empty($category->video))
            <li style="border-top: 1px solid #ccc;">
                <a class="nav-page" href="#видео">
                    <i class="glyphicon glyphicon-expand"></i>
                    Видео
                </a>
            </li>
        @endif

        <li style="border-top: 1px solid #ccc;">
            <a class="nav-page" href="#отзывы">
                <i class="glyphicon glyphicon-comment"></i>
                Отзывы
            </a>
        </li>

        <li style="border-top: 1px solid #ccc;">
            <a class="nav-page" href="#аналоги">
                <i class="glyphicon glyphicon-list-alt"></i>
                Аналоги
            </a>
        </li>

    </ul>
    <a class="back-to-top" href="#top">Наверх</a>
</div>