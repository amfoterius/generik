<div class="col-md-12 category-block-large">
    <div class="row" style="">
        <div class="col-md-3">
            <div class="image">
                <a itemprop="url"
                   href="{{ $category->url }}">
                    <img itemprop="image"
                         src="[domain]{{ $category->getImageUrl('img_big') }}"
                         alt="{{ $category->name }}" title="{{ $category->name }}"
                         class="img-responsive" style="margin: 0 auto">
                </a>
            </div>
            <div>
                <div>
                    <div class="row name text-center" style="height: 30px">
                        <a href="{{ $category->url }}" class="h3"
                           itemprop="name" style="font-size: 21px">
                            {{ str_limit($category->name, '50') }}
                        </a>
                    </div>
                    <div class="text-center">
                        @include('components.rating_stars', ['rating'=> $category->rating <= 3 ? 4 : 5])
                    </div>


                    <div class="row text-center" style="margin-bottom: 5px">
                        <div id="compare-{{ $category->id }}-active"
                             class="{{ in_array($category->id, $compare->toArray()) ? '' : 'hide' }}">
                            <span class="compare active"
                                  data-id="{{ $category->id }}" data-img-src="{{ $category->img_big }}"
                                  data-name="{{ $category->name }}">
                            <i class="icon glyphicon glyphicon-ok"></i>
                            Добавлено к сравнению.
                            </span>
                            <a id="compare-button-{{ $category->id }}" class="btn btn-primary btn-xs"
                               href="[domain]сравнение">Перейти</a>

                        </div>
                        <div id="compare-{{ $category->id }}"
                             class="{{ in_array($category->id, $compare->toArray()) ? 'hide' : '' }}">
                            <span class="compare"
                                  data-id="{{ $category->id }}" data-img-src="{{ $category->img_big }}"
                                  data-name="{{ $category->name }}">Добавить к сравнению.</span>

                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="col-md-6 right-panel">
            @foreach($category_options as $option)
                @if($val = $option->categories_m2m()->where('category_id', $category->id)->value('value'))
                    <div class="row option">
                        <div class="col-md-5 name">{{ $option->name }}</div>
                        <div class="col-md-7 value">{!! $val !!}</div>
                    </div>
                @endif
            @endforeach

        </div>
        <div class="col-md-3 right-panel">

            @foreach($category->pages_m2m()->where('site_id', $curr_site->id)->get() as $page_m2m)
                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12">
                        <a href="{{ $page_m2m->getUrl() }}">
                            <i class="glyphicon {{ $page_m2m->page->icon_glyphicon }}"></i>
                            {{ $category->name." ".mb_strtolower($page_m2m->page->name) }}
                        </a>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-center">
            <div class="row">
                <div class="col-md-12">от
                    <span class="price-new text-primary"
                          style="font-size: x-large">
                            <strong>{{ $category->min_price_item()['price'] }} RUB</strong>
                        </span></div>
            </div>

            <div class="row">

                <div class="col-md-12 col-xs-12 col-sm-12 text-center btns" style="margin-top: 8px">
                    @if($category->buy && !empty($category->partners_frame_url))
                        <div class="btn-group btns" style="width: 100%;">
                            <a href="{{ $category->getUrl() }}#купить" class="btn btn-sm btn-success"
                               style="width: 100%;">
                                <i class="glyphicon glyphicon-ok"></i>
                                Купить
                            </a>
                        </div>
                    @else
                        <div class="btn-group">
                            <a class="btn btn-success btn-sm add-to-cart {{ $category->api_id > 10000 ? 'disabled' : '' }}"
                               data-id="{{ $category->min_price_item()['id'] }}"
                               data-toggle="modal"
                               data-target="#on-cart"
                               data-img-src="{{ $category->img_big }}"
                               data-name="{{ $category->name }}"
                               data-price="{{ $category->min_price_item()['price'] }}"
                               data-kolvo="{{ $category->min_price_item()['kolvo'] }}">
                                <i class="glyphicon glyphicon-plus"></i>
                                В корзину
                            </a>
                            <a class="btn btn-primary btn-sm buy-for-one-click {{ $category->api_id > 10000 ? 'disabled' : '' }}"
                               data-toggle="modal"
                               data-target="#buy-for-one-click"
                               data-id="{{ $category->min_price_item()['id'] }}"
                               data-name="{{ $category->min_price_item()['name'] }}"
                               data-kolvo="{{ $category->min_price_item()['kolvo']  }}">
                                <i class="glyphicon glyphicon-hand-up"></i>
                                <span>&nbsp;Заказать в один клик</span>
                            </a>
                        </div>
                    @endif


                </div>
            </div>


        </div>
    </div>

</div>
<div class="col-md-1"></div>