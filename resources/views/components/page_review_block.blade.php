<div class="row" style="font-size: 130%; border-bottom: solid 1px #aaa; padding-top: 10px">
    <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="col-md-12">
            <strong>{{ $review->name }} ({{ $review->created_at->format('d-m-Y') }})</strong>

        </div>
        <div class="col-md-12">
            {{ $review->text }}
        </div>

    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
        <button class="btn {{ array_search("".$review->id, $category_page_review_likes) !== false ? 'btn-success' : 'btn-default' }}
        btn-lg lead review-category-page-vote" data-review="{{ $review->id }}" data-count="{{ $review->votes }}" style="width: 100%">
            <span>Полезный отзыв</span>
            <i class="glyphicon glyphicon-thumbs-up"></i>
            <span class="count">{{ $review->votes or 0 }}</span>
        </button>
    </div>




</div>