<?php
$curr_category_reviews = $curr_category->reviews()
    ->where('publish', 1)->where('site_id', $curr_site->id)->where('created_at', '<=', \Carbon\Carbon::now())->orderBy('stars', 'desc')->get();

?>


@if(isset($options) && array_key_exists('hide_head', $options) && $options['hide_head'])
    <div class="row" style="margin-top: 80px">
        <div class="col-md-12 h2 text-center">
            <a href="{{ $curr_category->getUrl() }}/добавить-отзыв" class="btn btn-info">
                <i class="glyphicon glyphicon-plus"></i>
                Новый отзыв
            </a>
        </div>

    </div>
@else
    <div class="row" id="отзывы" style="margin-top: 80px">
        <h2 class="text-center">
            {{ $title or $curr_category->name . ' oтзывы' }}
            <a href="{{ $curr_category->getUrl() }}/добавить-отзыв" class="btn btn-info">
                <i class="glyphicon glyphicon-plus"></i>
                Новый
            </a>
        </h2>
    </div>
@endif




<div class="row" style="margin-top: 20px">


    @if($curr_category_reviews->count() > 0)
        @foreach($curr_category_reviews as $review)

            @if(strlen($review->positive) > 0 || strlen($review->negative) > 0 || strlen($review->text))
                <div class="panel panel-default review">

                    <div class="panel-heading h5" style="margin-top: 0">
                        <div class="row">
                            <div class="col-md-8">
                                <span>{{ $review->name == "" ? 'Гость' : $review->name }}</span>
                                <span style="color: #555">
                                                <!--({{ $review->created_at->format('d-m-Y') }})-->
                                            </span>
                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>


                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">

                                <div class="row">
                                    <div class="col-md-12">
                                        <dl class="dl-horizontal">
                                            @foreach($review->options_m2m as $option_m2m)
                                                @if($option_m2m->stars != 0)

                                                    <dt style="width: 95px">{{ $option_m2m->option->name }}</dt>
                                                    <dd style="margin-left: 115px;">
                                                        @include('components.rating_stars', ['rating' => $option_m2m->stars,])
                                                    </dd>
                                                @endif
                                            @endforeach
                                        </dl>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">

                                        <button class="btn {{ array_search("".$review->id, $session_review_votes) !== false ? 'btn-success' : 'btn-default' }}
                                                btn-lg lead review-vote" data-review="{{ $review->id }}"
                                                data-count="{{ $review->votes }}" style="width: 100%">
                                            <span>Полезный отзыв</span>
                                            <i class="glyphicon glyphicon-thumbs-up"></i>
                                            <span class="count">{{ $review->votes }}</span>
                                        </button>

                                    </div>
                                </div>


                            </div>
                            <div class="col-md-8">
                                @if(strlen($review->positive) > 0)

                                    <p>

                                    <div class="h4 text-success">Положительные качества:</div>
                                    <div style="padding-left: 30px;">{!! $review->positive !!}</div>

                                    </p>

                                @endif

                                @if(strlen($review->negative) > 0)

                                    <p>

                                    <div class="h4 text-danger">Отрицательные качества:</div>
                                    <div style="padding-left: 30px;">{!! $review->negative !!}</div>

                                    </p>
                                @endif
                                @if(strlen($review->text) > 0)
                                    <p>
                                    <div style="padding-left: 30px;">{!! $review->text !!}</div>

                                    </p>

                                @endif

                            </div>
                        </div>


                    </div>
                </div>
            @endif
        @endforeach

    @else
        <div class="row">
            <div class="col-md-12 h3 text-center">
                На данный момент на сайте нет отзывов о {{ $curr_category->name_p }}, Вы будете
                первыми

            </div>
        </div>

    @endif
</div>