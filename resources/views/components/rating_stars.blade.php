<p class="text-primary" style="color: #FAB600">

    @for($i = 1; $i <= 5; $i++)
        @if($i <= round($rating))
            <i class="glyphicon glyphicon-star"></i>
        @else
            <i class="glyphicon glyphicon-star-empty"></i>
        @endif
    @endfor

</p>
