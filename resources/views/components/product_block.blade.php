<?php


if (!$category->is_init) $category->init();

?>


<div class="col-lg-{{ $settings['col-lg'] or '3' }} col-md-4 col-sm-6 col-xs-12 product-block" style="padding-top: 20px;"
     itemscope="" itemprop="itemListElement" itemtype="http://schema.org/Product">
    <!--
    <div itemprop="aggregateRating"
         itemscope="" itemtype="http://schema.org/AggregateRating">
        <meta itemprop="ratingValue" content="3.5">
        <meta itemprop="reviewCount" content="11">
    </div>
    -->
    <meta itemprop="productID" content="{{ $category->id }}">
    <meta itemprop="description" content="{{ $category->name }}. {{ $category->type->name }}">


    <?php $req_for_items = $category->req_for_items ?>
    <div itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
        <meta itemprop="lowPrice" content="{{ $req_for_items->min_price_per_item }}">
        <meta itemprop="highPrice" content="{{ $req_for_items->max_price_per_item }}">
        <meta itemprop="offerCount" content="{{ $req_for_items->c }}">
        <meta itemprop="priceCurrency" content="RUB">
        @foreach($category->products as $product)
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <meta itemprop="price" content="{{ $product->price_per_item }}">
                <meta itemprop="priceCurrency" content="RUB">
            </div>
        @endforeach


    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <a itemprop="url" href="{{ $category->url }}">

                <img itemprop="image"
                     src="[domain]{{ $category->getImageUrl('img_big') }}"
                     alt="{{ $category->name }}" title="{{ $category->name }}"
                     class="img-responsive" style="margin: 0 auto; height: 160px">
                <!--<span class="label label-success" style="display: block; position: absolute">100% Натуральный состав</span>-->

            </a>


        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 text-center" style="height: 50px">
            <a href="{{ $category->url }}"
               class="h3"
               itemprop="name" style="font-size: 21px">
                {{ str_limit($category->name, '50') }}
            </a>

        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 text-center">
            @include('components.rating_stars', ['rating' => $category->rating])
        </div>

        <div class="col-md-12 col-xs-12 col-sm-12 text-center">
            @if($category->buy)
                <span class="text-info" style="font-weight: bold">В наличии</span>
            @else
                <span class="text-success" style="font-weight: bold">Нет в наличии</span>

            @endif

        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 text-center" style="height: 45px;">
            @if(isset($settings['show_type']) and $settings['show_type'])

                <a href="{{ $category->type->getUrl() }}">
                    {{ str_limit($category->type->name, '50') }}
                </a>
            @endif

        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 price text-center">
            @if($req_for_items->min_price)
                цена
                <span class="price-new text-primary"
                      style="font-size: x-large">
                        <strong>{{ number_format($category->min_price_item->price, 0, "." , " " ) }} р</strong>
                    </span>

            @else
                <span style="margin-top: 5px">Временно нет в наличии</span>

            @endif
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 text-center" style="height: 30px">

            <div id="compare-{{ $category->id }}-active"
                 class="{{ in_array($category->id, $compare->toArray()) ? '' : 'hide' }}">
                <span class="compare active"
                      data-id="{{ $category->id }}" data-img-src="{{ $category->img_big }}"
                      data-name="{{ $category->name }}">
                            <i class="icon glyphicon glyphicon-ok"></i>
                            Добавлено к сравнению.
                </span>
                <a id="compare-button-{{ $category->id }}" class="btn btn-primary btn-xs"
                   href="[domain]сравнение">Перейти</a>

            </div>
            <div id="compare-{{ $category->id }}"
                 class="{{ in_array($category->id, $compare->toArray()) ? 'hide' : '' }}">
                <span class="compare"
                      data-id="{{ $category->id }}" data-img-src="{{ $category->img_big }}"
                      data-name="{{ $category->name }}">Добавить к сравнению.</span>

            </div>

        </div>


        <div class="col-md-12 col-xs-12 col-sm-12 text-center product-block-pages">
            @foreach($category->pages_m2m->where('site_id', $curr_site->id) as $page_m2m)
                <a href="{{ $category->url . '/' . $category->alias . '_'
                . $page_m2m->page->alias . (config('api.none_city') ? '' : '-в-[curr_city.name_p_lw]') }}">
                    <i class="glyphicon {{ $page_m2m->page->icon_glyphicon }}"></i>
                    {{ $page_m2m->page->name }}
                </a>
            @endforeach

        </div>

        <div class="col-md-12 col-xs-12 col-sm-12 product-icons-line" style="">
            @if($category->buy)
                <a href="{{ $req_for_items['max_kolvo'] >= 50 ?  $category->url . '#products-grid' : '#' }}"
                   class="delivery {{ $req_for_items['max_kolvo'] >= 50 ? 'active' : '' }}"
                   data-help="{{ $req_for_items['max_kolvo'] >= 50 ? 'Имеется предложение с бесплатной доставкой' : 'Нет предложения с бесплатной доставкой' }}"></a>
            @endif

            @if($category->buy)
                <a href="{{ $req_for_items['max_kolvo'] >= 20 ?  $category->url . '#products-grid' : '#' }}"
                   class="present {{ $req_for_items['max_kolvo'] >= 20 ? 'active' : '' }}"
                   data-help="{{ $req_for_items['max_kolvo'] >= 20 ? 'Имеется предложение с подарком 5 таб Виагры' : 'Нет предложения с подарком' }}"></a>
            @endif


            <a href="{{ $category->buy ?  $category->url . '#products-grid' : '#' }}"
               class="discount {{ $category->buy ? 'active' : '' }}"
               data-help="{{ $category->buy ? 'Скидки до '. $category->max_discount().'%' : 'Товара нет в наличии' }}"></a>


            <a href="{{ isset($category->sites_m2m->where('site_id', $curr_site->id)->first()->video_url) ?  $category->url . '#Видео' : '#' }}"
               class="video {{ isset($category->sites_m2m->where('site_id', $curr_site->id)->first()->video_url) ? 'active' : '' }}"
               data-help="{{ isset($category->sites_m2m->where('site_id', $curr_site->id)->first()->video_url) ? 'Имеется видеообзор препарата' : 'По данному препарату нет видео обзора... пока' }}"></a>
        </div>

        @if($category->buy && !empty($category->partners_frame_url))
            <div class="col-md-12 col-xs-12 col-sm-12 text-center btns" style="margin-top: 8px">
                <div class="btn-group btns" style="width: 100%;">
                    <a href="{{ $category->getUrl() }}#купить" class="btn btn-sm btn-success" style="width: 100%;">
                        <i class="glyphicon glyphicon-ok"></i>
                        Купить
                    </a>
                </div>
            </div>
        @else
            <div class="col-md-12 col-xs-12 col-sm-12 text-center btns" style="margin-top: 8px">
                <!-- TODO:Оптимизировать -->
                <div class="btn-group btns">
                    <a class="btn btn-success btn-sm add-to-cart {{ $category->api_id > 10000 ? 'disabled' : '' }}"
                       data-id="{{ $category->min_price_item->id }}"
                       data-toggle="modal"
                       data-target="#on-cart"
                       data-img-src="{{ $category->img_big }}"
                       data-name="{{ $category->name }}"
                       data-price="{{ $category->min_price_item->price }}"
                       data-kolvo="{{ $category->min_price_item->kolvo }}" style="font-size: 0.8em;">
                        <i class="glyphicon glyphicon-plus"></i>
                        В корзину
                    </a>
                    <a class="btn btn-primary btn-sm buy-for-one-click {{ $category->buy ? '' : 'disabled' }}"
                       data-toggle="modal"
                       data-target="#buy-for-one-click"
                       data-id="{{ $category->min_price_item->id }}"
                       data-name="{{ $category->min_price_item->name }}"
                       data-kolvo="{{ $category->min_price_item->kolvo  }}" style="font-size: 0.8em;">
                        <i class="glyphicon glyphicon-hand-up"></i>
                        <span>&nbsp;Заказать в один клик</span>
                    </a>
                </div>


            </div>

        @endif
    </div>

</div>