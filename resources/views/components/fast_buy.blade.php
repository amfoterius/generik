<div class="row">
    <div class="col-md-12 h2">
        {{ $category->name }}. Быстрый заказ
    </div>
</div>

<div id="fast-buy" class="row">
    <div class="col-md-12">
        <div class="alert alert-success">

            <form action="[domain]ajax/cart/new_order" role="form" method="post">
                <div class="row">
                    <input type="hidden" name="_token" value="[token]">
                    <input type="hidden" name="callback" value="1">

                    <div class="col-md-4">
                        <select name="product" id="" class="form-control input-sm">
                            @foreach($category->products as $k => $product)
                                <option value="{{ $product->id }}" {{ $k == 0 ? 'selected' : '' }}>{{ $product->kolvo }}
                                    / {{ number_format($product->price, 0 , "," , $thousands_sep = " " ) }}
                                    р
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 fio" style="">
                        <input name="fio" type="text" class="form-control input-sm"
                               placeholder="Ваше имя" required>
                    </div>
                    <div class="col-md-3">
                        <input name="tel" type="text" class="form-control input-sm"
                               placeholder="Номер телефона" required>
                    </div>
                    <div class="col-md-2 submit" style="">
                        <button type="submit" class="btn btn-success input-sm"
                                style="width: 100%"><i
                                    class="glyphicon glyphicon-ok"></i> Заказать
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-info">
                        Доставка Почтой РФ 1-ого класса 300 р, осуществляется до 8 дней
                    </div>
                </div>
            </form>

        </div>

    </div>
</div>