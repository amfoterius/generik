<?php
if (!isset($curr_category)) {
    $curr_category = new \App\Product_category();
    $curr_category->id = 0;
    $curr_category->type_id = 0;
}


?>


@if(isset($title))
    <div class="row">
        <div class="col-md-12 text-center">
            <h2>{{ $title or '' }}</h2>
        </div>
    </div>
@endif


<div class="row">

    <div class="col-md-12" itemscope="" itemtype="http://schema.org/ItemList">
        <meta itemprop="numberOfItems" content="{{ count($categories) }}">
        @foreach($categories as $k => $category)
            @if($category->id != $curr_category->id)
                @include('components.product_block',
                                        ['category'=>$category, 'position' => $k, 'settings'=>['col-lg'=>4]])
            @endif
        @endforeach
    </div>
</div>
