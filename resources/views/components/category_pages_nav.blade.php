@if(isset($pages) && count($pages))
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default navbar-static" style="padding-top: 60px">
                <!--Desktop/Tablet Menu-->
                <div class="">
                    <div>

                        <div class="btn-group btn-group-justified">
                            <!--Home-->
                            <?php //TODO: refact ?>


                            @if(!is_null($curr_category_page))
                                <div class="btn-group">

                                    <a href="{{ $curr_category->getUrl() }}" type="button" class="btn btn-nav">

                                    <span style="color: red; font-weight: bolder">
                                        <span class="glyphicon glyphicon-info-sign"></span>

                                        <p id="home">Общая информация</p>

                                     </span>


                                    </a>
                                </div>
                                @else
                                        <!--
                            <div class="btn-group">

                                <button type="button" class="btn btn-nav active"
                                        disabled="disabled">
                                    <span class="lead" style="color: red; font-weight: bolder">
                                        <span class="glyphicon glyphicon-shopping-cart"></span>

                                        <p id="home">Главная / Купить</p>

                                    </span>

                                </button>
                            </div>
                            -->

                            @endif



                            @foreach($pages as $page)

                                @if(!is_null($curr_category_page) && $curr_category_page->id == $page->id)
                                    <div class="btn-group">

                                        <a href="#" type="button" class="btn btn-nav active" style="color: #cccccc"
                                           disabled="disabled">
                                        <span>
                                            <span class="glyphicon {{ $page->page->icon_glyphicon }}"></span>

                                            <p id="services">{{ $page->page->name }} {{ $page->page->id == 4 ? '('.$curr_category->reviews()->where('publish', 1)->where('site_id', $curr_site->id)->count().')' : '' }}</p>
                                        </span>
                                        </a>

                                    </div>

                                @else
                                    <div class="btn-group">

                                        <a href="{{ $page->getUrl() }}" type="button" class="btn btn-nav" style="color: red">
                                        <span>
                                            <span class="glyphicon {{ $page->page->icon_glyphicon }}"></span>

                                            <p id="services">{{ $page->page->name }} {{ $page->page->id == 4 ? '('.$curr_category->reviews()->where('publish', 1)->where('site_id', $curr_site->id)->count().')' : '' }}</p>
                                        </span>
                                        </a>

                                    </div>


                                @endif

                            @endforeach


                        </div>


                    </div>
                </div>


            </nav>

        </div>
    </div>

@endif