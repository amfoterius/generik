{{-- Compare analogs --}}
<div class="row">
    <div class="col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="hidden-xs hidden-sm">
                </th>
                @foreach($categories as $category)
                    <th class="text-center" style="">
                        <div></div>
                        <a href="{{ $category->getUrl() }}" style="margin-top: 5px">
                            @if(!empty($category->img_small))
                                <img src="[domain]{{ $category->getImageUrl('img_small') }}"
                                     class="img-responsive" alt=""
                                     style="width: 50px; height: 50px; float: left; ">
                            @else
                                <img src="[domain]{{ $category->getImageUrl('img_big') }}"
                                     class="img-responsive" alt=""
                                     style="width: 50px; height: 50px; float: left; ">
                            @endif

                            <span>{{ $category->name }}</span>

                        </a>
                    </th>
                @endforeach
            </tr>
            </thead>
            <div id="collapse-compare" class="collapse">
                <tbody>
                <!--
                                        <tr>
                                            <td style="font-size: smaller; font-weight: bold;">
                                                Действие
                                            </td>
                                            @foreach($categories as $category)
                        <td style="font-size: large">
                            <a href="{{ $category->type->getUrl() }}">{{ $category->type->name }}</a>
                                                </td>
                                            @endforeach
                        </tr>
                        -->
                <tr class="visible-xs visible-sm hidden-md hidden-lg">
                    <td colspan="{{ $categories->count() }}" class="text-center lead">
                        <strong>Цена от</strong>

                    </td>
                </tr>

                <tr>
                    <td class="hidden-xs hidden-sm" style="font-size: smaller; font-weight: bold;">
                        Цена от
                    </td>
                    @foreach($categories as $category)
                        <td class="text-center" style="font-size: large">{{ $category->min_price_item()['price'] }}
                            RUB
                        </td>
                    @endforeach
                </tr>

                <tr class="visible-xs visible-sm hidden-md hidden-lg">
                    <td colspan="{{ $categories->count() }}" class="text-center lead">
                        <strong>Общий рейтинг</strong>

                    </td>
                </tr>


                <tr>
                    <td class="hidden-xs hidden-sm" style="font-size: smaller; font-weight: bold;">
                        Общий рейтинг
                    </td>
                    @foreach($categories as $category)
                        <td class="text-center" style="font-size: large">@include('components.rating_stars', ['rating'=>$category->avg_rating()])</td>
                    @endforeach
                </tr>

                @foreach($category_options as $option)
                    <tr class="visible-xs visible-sm hidden-md hidden-lg">
                        <td colspan="{{ $categories->count() }}" class="text-center lead">
                            <strong>{{ $option->name }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td class="hidden-xs hidden-sm">{{ $option->name }}</td>
                        @foreach($categories as $category)
                            <td class="text-center">{!! $category->options_m2m()->where('option_id', $option->id)->first()->value or '' !!} </td>
                        @endforeach
                    </tr>
                @endforeach

                </tbody>
            </div>
        </table>
    </div>
</div>
{{-- END Compare analogs --}}