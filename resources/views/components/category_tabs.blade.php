<div class="row">

    <div class="col-md-12">

        <ul class="nav nav-pills nav-stacked">
            <li class="{{ !isset($curr_page) ? "active" : "" }}">
                <a href="{{ route('category', [$curr_type->alias, mb_strtolower(str_replace(" ", "-", $curr_city->name_p)), $curr_category->alias, mb_strtolower(str_replace(" ", "-", $curr_city->name_p))]) }}">
                    {{ $curr_category->name }}, главная
                </a>
            </li>

            @foreach($curr_category->pages as $page_m2m)

                @if(isset($curr_page) && $page_m2m->id == $curr_page->id)
                    <li class="active">
                        <a>
                            {{ $curr_category->name . ", " . mb_strtolower($page_m2m->page->name) }}
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{ route('category_page', [$curr_type->alias, mb_strtolower(str_replace(" ", "-", $curr_city->name_p)), $curr_category->alias, mb_strtolower(str_replace(" ", "-", $curr_city->name_p)), $page_m2m->page->alias]) }}">
                            {{ $curr_category->name . ", " . mb_strtolower($page_m2m->page->name) }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>

</div>