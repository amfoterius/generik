<div id="best-sellers_block_right" class="block products_block">
    <h4 class="title_block">
        <a href="{{ url('') }}" title="">{{ $title }}</a>
    </h4>

    <div class="block_content" style="">
        <ul class="block_content products-block" style="">
            @foreach($products as $product)

                <li class="clearfix">
                    <a href="{{ url('') }}" title="" class="products-block-image content_img clearfix">
                        <img class="replace-2x img-responsive"
                             src="[domain]{{ $product->getImageUrl('image') }}" alt="">
                    </a>

                    <div class="product-content">
                        <h5>
                            <a class="product-name" href="{{ url('') }}" title="">
                                {{ $product->name }}
                            </a>
                        </h5>


                        <p class="product-description">{{  str_limit($product->description, 70) or ''}}</p>

                        <div class="price-box">
                            <span class="price">{{ $product->curr_price }}</span>
                        </div>
                    </div>
                </li>

            @endforeach

        </ul>
        <div class="lnk">
            <a href="{{ url('') }}" title="All best sellers" class="btn btn-default button button-small">
<span>
All best sellers
<i class="icon-chevron-right right"></i>
</span>
            </a>
        </div>
    </div>
</div>