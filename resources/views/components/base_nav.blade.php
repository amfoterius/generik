<?php

$curr_site = \App\Site\Site::find(config('api.site_id'));

switch($curr_site->id) {

    case 8:
        $pages = [
                'Каталог' => '[domain]каталог',
                'Контакты' => '[domain]контакты',
                'Доставка' => '[domain]доставка',
        ];
        break;

    default:
        $pages = [
                'Каталог' => '[domain]каталог',
                'Доставка' => '[domain]доставка',
                'Контакты' => '[domain]контакты',
        ];

}

?>


<nav class="navbar navbar-default" role="navigation" style="margin-top: 10px;">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>


            <a class="navbar-brand {{ $curr_site->id == 9 ? 'logo' : '' }}" href="/">
                <span>{{ $curr_site->id == 9 ? '' : $curr_site->name }}</span>
            </a>


            <a class="navbar-brand" href="{{ url('города') }}"
               style="color: #337ab7; text-decoration: dashed; margin-left: 8px; padding-top: 55px;">
                {{ config('api.none_city') ? 'Выбрать город' : 'в [curr_city.name_p]' }}
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-top: 40px;">
            <ul class="nav navbar-nav">
                @foreach($pages as $name => $url)
                <li class="">
                    <a href="{{ $url }}">
                        {{ $name }}
                    </a>
                </li>
                @endforeach

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <!--
                <li>
                    <a href="[domain]сравнение"
                       class="{{ $compare->count() ? "text-primary-inherit" : "" }}">
                        Сравнение<span id="compare-count" class="badge pull-right"> {{ $compare->count() }}</span>
                    </a>

                </li>
                -->
                <li>
                    <a href="[domain]корзина"
                       class="{{ $cart_count ? "text-primary-inherit" : "" }}">
                        <i class="menu-icon glyphicon glyphicon-shopping-cart"></i>
                        Корзина <span id="cart-count" class="badge pull-right">[cart_count]</span>
                    </a>

                </li>
            </ul>


        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>