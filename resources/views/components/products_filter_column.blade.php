
<div id="layered_block_left" class="block">
    <p class="title_block">Каталог</p>

    <div class="block_content">
        <form action="#" id="layered_form">
            <div>
                <div class="layered_filter">
                    <div class="layered_subtitle_heading">
                        <span class="layered_subtitle">Categories</span>

                    </div>
                    <ul id="ul_layered_category_0" class="col-lg-12 layered_filter_ul">
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_category_4"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_category_4" id="layered_category_4"
                                            value="4"></span></div>
                            <label for="layered_category_4">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Aliquam congue
                                    <span> (20)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_category_10"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_category_10" id="layered_category_10"
                                            value="10"></span></div>
                            <label for="layered_category_10">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Pellentesque sed
                                    <span> (20)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_category_16"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_category_16" id="layered_category_16"
                                            value="16"></span></div>
                            <label for="layered_category_16">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Enim adipiscing
                                    <span> (9)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_category_22"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_category_22" id="layered_category_22"
                                            value="22"></span></div>
                            <label for="layered_category_22">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Aliquet sit amet
                                    <span> (3)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_category_28"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_category_28" id="layered_category_28"
                                            value="28"></span></div>
                            <label for="layered_category_28">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Elliptical Trainers
                                    <span> (3)</span>
                                </a>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="layered_filter">
                    <div class="layered_subtitle_heading">
                        <span class="layered_subtitle">Наличие</span>

                    </div>
                    <ul id="ul_layered_quantity_0" class="col-lg-12 layered_filter_ul">
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_quantity_1"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_quantity_1" id="layered_quantity_1"
                                            value="1"></span></div>
                            <label for="layered_quantity_1">
                                <a href="{{ url('') }}" rel="nofollow">
                                    По наличию
                                    <span> (20)</span>
                                </a>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="layered_filter">
                    <div class="layered_subtitle_heading">
                        <span class="layered_subtitle">Состояние</span>

                    </div>
                    <ul id="ul_layered_condition_0" class="col-lg-12 layered_filter_ul">
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_condition_new"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_condition_new" id="layered_condition_new"
                                            value="new"></span></div>
                            <label for="layered_condition_new">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Новые
                                    <span> (20)</span>
                                </a>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="layered_filter">
                    <div class="layered_subtitle_heading">
                        <span class="layered_subtitle">Производитель</span>

                    </div>
                    <ul id="ul_layered_manufacturer_0" class="col-lg-12 layered_filter_ul">
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_manufacturer_8"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_manufacturer_8"
                                            id="layered_manufacturer_8" value="8"></span></div>
                            <label for="layered_manufacturer_8">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Duis semper congue metus
                                    <span> (3)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_manufacturer_2"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_manufacturer_2"
                                            id="layered_manufacturer_2" value="2"></span></div>
                            <label for="layered_manufacturer_2">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Fusce a viverra nulla
                                    <span> (3)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_manufacturer_3"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_manufacturer_3"
                                            id="layered_manufacturer_3" value="3"></span></div>
                            <label for="layered_manufacturer_3">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Fusce vitae auctor erat
                                    <span> (3)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_manufacturer_4"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_manufacturer_4"
                                            id="layered_manufacturer_4" value="4"></span></div>
                            <label for="layered_manufacturer_4">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Lorem ipsum dolor
                                    <span> (3)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_manufacturer_5"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_manufacturer_5"
                                            id="layered_manufacturer_5" value="5"></span></div>
                            <label for="layered_manufacturer_5">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Nullam aliquam odio tellus
                                    <span> (3)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_manufacturer_6"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_manufacturer_6"
                                            id="layered_manufacturer_6" value="6"></span></div>
                            <label for="layered_manufacturer_6">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Sed tincidunt lacinia elit
                                    <span> (3)</span>
                                </a>
                            </label>
                        </li>
                        <li class="nomargin hiddable col-lg-12">
                            <div class="checker" id="uniform-layered_manufacturer_7"><span><input
                                            type="checkbox" class="checkbox"
                                            name="layered_manufacturer_7"
                                            id="layered_manufacturer_7" value="7"></span></div>
                            <label for="layered_manufacturer_7">
                                <a href="{{ url('') }}" rel="nofollow">
                                    Ut feugiat lobortis ligula
                                    <span> (2)</span>
                                </a>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="layered_price" style="">
                    <div class="layered_subtitle_heading">
                        <span class="layered_subtitle">Цена</span>

                    </div>
                    <ul id="ul_layered_price_0" class="col-lg-12 layered_filter_ul">
                        <li class="nomargin  layered_list"
                            onclick="$('#layered_price_range_min').val(24);$('#layered_price_range_max').val(27);reloadContent();">
                            - От 24 $ до 27 $
                        </li>
                        <li class="nomargin  layered_list"
                            onclick="$('#layered_price_range_min').val(27);$('#layered_price_range_max').val(30);reloadContent();">
                            - От 27 $ до 30 $
                        </li>
                        <li class="nomargin  layered_list"
                            onclick="$('#layered_price_range_min').val(30);$('#layered_price_range_max').val(33);reloadContent();">
                            - От 30 $ до 33 $
                        </li>
                        <li class="nomargin  layered_list"
                            onclick="$('#layered_price_range_min').val(33);$('#layered_price_range_max').val(37);reloadContent();">
                            - От 33 $ до 37 $
                        </li>
                        <li style="display: none;">
                            <input class="layered_price_range" id="layered_price_range_min"
                                   type="hidden" value="24">
                            <input class="layered_price_range" id="layered_price_range_max"
                                   type="hidden" value="37">
                        </li>
                    </ul>
                </div>
            </div>
            <input type="hidden" name="id_category_layered" value="3">
        </form>
    </div>
    <div id="layered_ajax_loader" style="display: none;">
        <p>
            <img src="{{ url('') }}" alt="">
            <br>Загрузка ...
        </p>
    </div>
</div>