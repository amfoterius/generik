<div class="hidden-xs list-categories">
    @foreach($types as $type)
        @if($type->categories->count() > 0)
            <div class="h3 type-name">
                <a href="{{ $type->getUrl() }}">{{ $type->name }}</a>
            </div>
            <div class="row categories" style="list-style-type: none">
                @foreach($type->categories as $category)
                    @if($category->buy)
                        <?php $req_for_items = $category->products()->select(DB::raw('max(price_per_item) as max_price, min(price_per_item) as min_price, max(kolvo_int) as max_kolvo, count(*) as c'))->first() ?>
                        <div class="col-md-12 category text-center">

                            <a href="{{ $category->getUrl() }}">
                                <div class="row">

                                    <div class="col-md-12"
                                         style="font-size: 120%; font-weight: bolder">{{ $category->name }}</div>

                                    <div class="col-md-12 product-icons-line-mini" style="">
                                        @if($category->buy && $req_for_items['max_kolvo'] >= 50)
                                            <span href="{{ $category->getUrl() . '#Price' }}"
                                                  class="delivery active"></span>
                                        @endif

                                        @if($category->buy && $req_for_items['max_kolvo'] >= 20)
                                            <span href="{{ $category->getUrl() . '#Price' }}"
                                                  class="present active"></span>
                                        @endif



                                        @if($category->buy && $discount = $category->max_discount())
                                            <span href="{{ $category->getUrl() . '#products-grid' }}"
                                                  class="discount active"
                                                  data-help="{{ 'Скидки до '. $discount.'%' }}"></span>

                                        @endif


                                        @if(isset($category->sites_m2m()->where('site_id', $curr_site->id)->first()->video))
                                            <span href="{{ $category->getUrl() . '#Видео' }}"
                                                  class="video active"></span>

                                        @endif
                                    </div>

                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        @endif
    @endforeach
</div>