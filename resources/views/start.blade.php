@extends('base')




@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="GET">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                    <div class="input-group">
                        <input type="text" name="cityName" class="form-control"
                               placeholder="Введите название города или его часть">
                      <span class="input-group-btn">
                          <input class="btn btn-primary" type="submit" value="Поиск">
                      </span>
                    </div><!-- /input-group -->
                </form>

            </div>

            <div class="col-md-12">
                @foreach($bukvs as $b)
                    <a style="font-size: 2.2em;" href="#{{ $b }}">#{{ $b }}  </a>

                @endforeach


            </div>
        </div>
        <div class="h1">Выберите регион:</div>
        <div class="row">
            @foreach($top_regions as $key => $region)
                <div class="col-md-4">
                    <a class="h4 text-primary" href="{{ 'http://' . $region->alias . "." . $curr_site->url . '.xn--p1ai' }}">
                        {{ $region->name }}
                    </a>
                </div>
            @endforeach

            @foreach($regions as $key => $region_or_bukv)
                @if(gettype($region_or_bukv) == "string")
                    <div id="{{ $region_or_bukv }}" class="col-md-12 h3">
                        {{ $region_or_bukv }}
                    </div>

                @else
                    <div class="col-md-4">
                        <a class="h4 text-primary"
                           href="{{ 'http://' . $region_or_bukv->alias . "." . $curr_site->url . '.xn--p1ai' }}">
                            {{ $region_or_bukv->name }}
                        </a>
                    </div>
                @endif
            @endforeach
        </div>


    </div>
@endsection
