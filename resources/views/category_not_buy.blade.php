@extends('base')

@section('head')
    <meta property="og:title" content="[page_meta.meta_title]"/>
    <meta property="og:type" content="website"/>

    @if(count($curr_category->images) > 0)
        <meta property="og:image"
              content="[domain]{{ $curr_category->images[0]->getImageUrlResize('image') }}"/>
    @else
        <meta property="og:image" content="[domain]{{ $curr_category->getImageUrl('img_big') }}"/>
    @endif

    <meta property="og:description" content="[page_meta.meta_description]"/>
    <meta property="og:site_name" content="{{ $curr_site->name }}"/>
    <meta property="og:url" content="{{ $curr_category->getUrl() }}"/>

@endsection

@section('header')
    @parent
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="[domain]каталог" itemprop="url">
                <span itemprop="title">Каталог</span>
            </a>
        </li>
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="{{ $curr_category->type->getUrl() }}" itemprop="url"
               title="[curr_category.type.name]">
                <span itemprop="title">[curr_category.type.name]</span>
            </a>
        </li>
        <li class="active">
            [curr_category.name]
        </li>
    </ol>
@endsection


@section('content')
    <div class="container">


        <div class="row">

            <div class="col-md-9">

                <!--<div id="nav-affix" data-spy="affix" data-offset-top="300">-->
                <div itemscope itemtype="http://schema.org/Product">

                    <meta itemprop="productID" content="{{ $curr_category->id }}">

                    {{-- reviews microdata --}}
                    <div itemprop="aggregateRating"
                         itemscope itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="ratingValue" content="{{ $curr_category->avg_rating() }}">
                        <meta itemprop="reviewCount"
                              content="{{ $curr_category_reviews->count() }}">
                    </div>
                    {{-- END reviews microdata --}}

                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <h1 id="category-name" class="text-primary" style="float: left; margin-right: 40px">
                                <span itemprop="name">[curr_category.name]</span>
                                {{ config('api.none_city') ? '' : ' в [curr_city.name_p]' }}

                            </h1>

                            <?php $req_for_items = $curr_category->products()->select(DB::raw('max(price_per_item) as max_price, min(price_per_item) as min_price, max(kolvo_int) as max_kolvo, count(*) as c'))->first() ?>


                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            {{-- Сравнение --}}

                            <div class="col-md-12 col-xs-12 col-sm-12 text-center"
                                 style="height: 60px; padding-top: 30px;">

                                <div id="compare-{{ $curr_category->id }}-active"
                                     class="{{ in_array($curr_category->id, $compare->toArray()) ? '' : 'hide' }}">
                                        <span class="compare active"
                                              data-id="{{ $curr_category->id }}"
                                              data-img-src="{{ $curr_category->img_big }}"
                                              data-name="{{ $curr_category->name }}">
                                                    <i class="icon glyphicon glyphicon-ok"></i>
                                                    Добавлено к сравнению.
                                        </span>
                                    <a id="compare-button-{{ $curr_category->id }}"
                                       class="btn btn-primary btn-xs"
                                       href="[domain]сравнение">Перейти</a>

                                </div>

                                <div id="compare-{{ $curr_category->id }}"
                                     class="{{ in_array($curr_category->id, $compare->toArray()) ? 'hide' : '' }}">
                                        <span class="compare"
                                              data-id="{{ $curr_category->id }}"
                                              data-img-src="{{ $curr_category->img_big }}"
                                              data-name="{{ $curr_category->name }}">Добавить к сравнению.</span>
                                </div>


                            </div>

                            {{-- END Сравнение --}}

                        </div>

                    </div>


                    {{-- Product info --}}
                    <div class="row" id="kupit">

                        {{-- Image + options --}}
                        <div class="col-md-12">
                            <div class="row" style="margin-top: 30px">

                                <div class="col-md-8">

                                    {{-- Главный текст --}}
                                    <div itemprop="description" class="col-md-12 text-justify"
                                         style="font-size: 130%; margin-top: 20px; margin-bottom: 20px;">
                                        [curr_page.text]
                                    </div>
                                    {{-- END Главный текст --}}

                                    @include('components.category.category_options_table', ['category' => $curr_category])
                                </div>

                                <div class="col-md-4">


                                    {{-- Галлерея --}}
                                    <div class="col-md-12">

                                        @if(count($curr_category->images) > 0)
                                            <div class="row">
                                                @foreach($curr_category->images()->orderBy('priority', 'desc')->take(4)->get() as $image)


                                                    <div class="col-md-12 col-sm-6 col-xs-6" style="margin-top: 3px">
                                                        <a href="[domain]{{ $image->getImageUrlResize('image', 'large') }}"
                                                           data-lightbox="images"
                                                           data-title="{{ $curr_category->name }}">

                                                            <img src="[domain]{{ $image->getImageUrl('image') }}"
                                                                 itemprop="image"
                                                                 class="img-responsive"
                                                                 alt="{{ $curr_category->name }}"
                                                                 style="margin: 0 auto">

                                                        </a>
                                                    </div>


                                                @endforeach
                                            </div>

                                        @else

                                            <img src="[domain]{{ $curr_category->getImageUrl('img_big') }}"
                                                 itemprop="image"
                                                 class="img-responsive"
                                                 alt="{{ $curr_category->name }}" style="margin: 0 auto">


                                        @endif
                                    </div>

                                    {{-- END Галлерея --}}

                                </div>

                            </div>
                        </div>
                        {{-- END Image + options --}}

                        {{--  Блок информации --}}

                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="h3 text-info lead text-justify">
                                        <i class="glyphicon glyphicon-info-sign" style="font-size: 120%"></i>
                                        Наша интернет-аптека предлагает только по-настоящему эффективные и качественные
                                        препараты.
                                        {{ $curr_category->name }} не является таковым, поэтому его нет в наличие. Предлагаем Вашему вниманию
                                        лучшие аналоги
                                    </div>



                                </div>
                            </div>
                        </div>

                        <div id="аналоги" class="col-md-12" itemscope=""
                             itemtype="http://schema.org/ItemList">
                            @foreach($curr_category->another_categories(3) as $k => $category)

                                @include('components.product_block',
                                ['category'=>$category, 'position'=>$k, 'settings'=>['col-lg'=>4]])

                            @endforeach

                        </div>


                        {{--  END Блок информации  --}}



                        {{-- Description --}}

                        <div class="col-md-12 lead text-justify" style="font-size: 130%; margin-top: 30px">
                            @if(!empty($curr_category->seo_description))
                                {!! $curr_category->seo_description  !!}
                            @else
                                <noindex>{!! $curr_category->description or "" !!}</noindex>
                            @endif
                        </div>

                        {{-- END Description --}}


                        <!--
                        <div class="row">
                            <img src="{{ url('images/we_use.png') }}" alt="Способы оплаты" class="img-responsive"
                                 style="margin: 0 auto; margin-bottom: 10px">
                        </div>
                        -->


                        @if(!$curr_category->buy && $compare_categories->count() > 1)
                            @include('components.categories_compare', ['categories' => $compare_categories])
                        @endif

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @include('components.category_pages_nav', [
                                'pages' => $curr_category->pages_m2m()->where('site_id', $curr_site->id)->get(),
                            ])
                        </div>

                        {{-- Text --}}
                        <div id="category-text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 20px">
                            {!! $curr_category_site_m2m->text or '' !!}
                        </div>
                        {{-- END Text --}}

                        {{-- Video --}}
                        @if(!is_null($curr_category_site_m2m) && !empty($curr_category_site_m2m->video_url))

                            {{-- Video --}}
                            @if(!is_null($curr_category_site_m2m) && !empty($curr_category_site_m2m->video_url))

                                <div itemscope itemtype="http://schema.org/VideoObject">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"
                                         style="padding-top: 60px">
                                        <h2 itemprop="name">{{ $curr_category->name }} видео обзор</h2>
                                    </div>


                                    <meta itemprop="url"
                                          content="https://player.vimeo.com/video/{{ $curr_category_site_m2m->video_url }}">

                                    <meta itemprop="description"
                                          content="{{ $curr_category->name . ' - видео обзор и описание препарата' }}">
                                    <meta itemprop="duration"
                                          content="PT0M{{ $curr_category_site_m2m->video_duration }}S">
                                    <meta itemprop="isFamilyFriendly" content="false">
                                    <meta itemprop="uploadDate"
                                          content="{{ $curr_category_site_m2m->updated_at->format('Y-m-y\Th:i:s') }}">

                                    <div itemprop="thumbnail" itemscope itemtype="http://schema.org/ImageObject">
                                        <img itemprop="contentUrl" class="hide" src="[domain]images/screen-video.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="240">
                                    </div>

                                    <meta itemprop="thumbnail"
                                          content="{{ $curr_category_site_m2m->updated_at->format('Y-m-y\Th:i:s') }}">

                                    <meta itemprop="caption" content="{{ $curr_site->name }}">
                                    <meta itemprop="videoQuality" content="full HD">

                                    <!--

                                <div itemprop="aggregateRating" itemscope
                                     itemtype="http://schema.org/AggregateRating">
                                    <meta itemprop="ratingValue" content="5">
                                    <meta itemprop="reviewCount"
                                          content="{{ round($curr_category->id / 2) }}">
                                </div>
                                -->

                                    <meta itemprop="inLanguage" content="RU">

                                    <div style="padding-top: 20px" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                         id="видео" itemprop="embedHTML">
                                        {!! $curr_category_site_m2m->video !!}
                                        <iframe src="https://player.vimeo.com/video/{{ $curr_category_site_m2m->video_url }}"
                                                id="category-video"
                                                frameborder="0" webkitallowfullscreen mozallowfullscreen
                                                allowfullscreen></iframe>
                                    </div>
                                </div>


                            @endif
                            {{-- END Video --}}


                        @endif
                        {{-- END Video --}}


                    </div>
                    {{-- END Product info --}}

                </div>


                <div class="row" style="margin-top: 60px;">
                    <div class="col-md-12">
                        @if($curr_type->id == 1)
                            @if(!empty(\App\Product_category_type::find(11)->image_banner))
                                <img src="[domain]{{ \App\Product_category_type::find(11)->getImageUrl('image_banner') }}"
                                     alt="" style="width: 100%">
                            @else
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h2>{{ \App\Product_category_type::find(11)->name }}</h2>
                                    </div>
                                </div>
                            @endif

                            <div class="row" style="margin-top: 20px" itemscope=""
                                 itemtype="http://schema.org/ItemList">
                                <meta itemprop="numberOfItems"
                                      content="{{ count(\App\Product_category_type::find(11)->categories) }}">
                                <div class="col-md-12">
                                    @foreach(\App\Product_category_type::find(11)->categories as $category)
                                        @include('components.product_block',
                                        ['category'=>$category, 'settings'=>['col-lg'=>4]])
                                    @endforeach
                                </div>
                            </div>
                        @endif

                    </div>
                </div>


                @if($curr_category->buy && $compare_categories->count() > 1)
                    @include('components.categories_compare', ['categories' => $compare_categories])
                @endif

                {{-- Categories Groups --}}
                @foreach($curr_category->groups_m2m()->orderBy('priority', 'desc')->get() as $group_m2m)
                    <div class="row" itemscope="" itemtype="http://schema.org/ItemList">
                        <meta itemprop="numberOfItems"
                              content="{{ $curr_category->groups_m2m()->orderBy('priority', 'desc')->count() }}">
                        <h2 class="col-md-12 text-center" style="margin-top: 60px; margin-bottom: 20px">
                            {{ $group_m2m->group->name }}
                        </h2>

                        <div class="row">
                            @foreach($group_m2m->group->categories_list_m2m()->orderBy('priority', 'desc')->get() as $k => $category_list_m2m)

                                @include('components.product_block',
                                ['category'=>$category_list_m2m->category, 'position'=>$k, 'settings'=>['col-lg'=>4]])

                            @endforeach
                        </div>
                    </div>


                @endforeach

                {{-- END Categories Groups --}}



                {{--  BANNER--}}

                @include('components.site.banner', ['banner' => $curr_site->getBanner('Категория-БаннерКонфиденциальность')])
                {{-- END BANNER--}}



                {{-- Reviews --}}
                [blocks_no_cache.reviews]

                {{-- END Reviews --}}


                {{-- Social buttons --}}
                <div class="row">
                    <div class="col-md-12 text-center" style="font-weight: bolder; font-size: 110%;">
                        Понравилось у нас? Поделись с друзьями
                        <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"
                                charset="utf-8"></script>
                        <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                        <div class="ya-share2" style="margin-top: 5px;"
                             data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp"></div>
                    </div>
                </div>
                {{-- END Social buttons --}}


            </div>
            <div class="col-md-3 col-lg-3">

                <div class="hidden-xs hidden-sm">
                    @include('components.categories_column', ['types'=>$types, 'curr_category' => $curr_category])
                </div>

            </div>


        </div>


    </div>


@endsection

@section('js')
    @parent
    <script>

        var domain = '{{ url('') }}/';

        $(document).ready(function (e) {

            $(".review div .info").click(function (e) {
                e.preventDefault();

                var $fullBlock = $(this).parent().parent().find('.panel-body:last');
                var $miniBlock = $(this).parent().parent().find('.panel-body:first');

                if ($fullBlock.hasClass('hidden')) {
                    $fullBlock.removeClass('hidden');
                    $miniBlock.addClass('hidden');
                    $(this).text('Скрыть');
                } else {
                    $fullBlock.addClass('hidden');
                    $miniBlock.removeClass('hidden');
                    $(this).text('Подробнее');
                }

                return false;
            });

            $('a.nav-page').bind('click', function (e) {
                e.preventDefault();

                var target = $(this).attr("href");

                $('html, body').stop().animate({scrollTop: $(target).offset().top}, 500, function () {
                    location.hash = target;
                });

                return false;
            });


            /*
             $(".add-to-cart").on('click', function () {
             $.ajax({
             url: "
            {{ url('ajax/cart/add') }}",
             method: 'POST',
             data: {
             "product_id": $(this).data('id'),
             "product_count": 1
             }
             })
             .done(function (data) {
             var c = parseInt($("#cart-count").text());

             $("#cart-count").text((c + 1).toString());
             })
             .fail(function (data) {
             //
             })
             .always(function (data) {
             });

             });
             */

            $('#nav-affix').affix({
                offset: {
                    top: 100
                    , bottom: function () {
                        return (this.bottom = $('.footer').outerHeight(true))
                    }
                }
            })
        })

    </script>

@endsection
