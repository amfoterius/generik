@extends('base')

@section('header')
    @parent
@endsection


@section('content')
    <div class="container" style="margin-top: 60px">
        <div class="post_content entry-content">
            <div class="h1 text-center">Контакты</div>
            <div class="post_content entry-content">

                <div class="row" style="color: #000; margin-top: 20px">

                    <div class="row" style="margin-top: 10px; border-bottom: solid">
                        <div class="col-md-4">Название организации:</div>
                        <div class="col-md-8" style="font-size: x-large;">ООО "Фармсейл"</div>
                    </div>

                    <div class="row" style="margin-top: 10px; border-bottom: solid">
                        <div class="col-md-4">Адрес:</div>

                        @if(!config('api.none_city') && !is_null($site_city_m2m = $curr_city->site_m2m()->where('site_id', $curr_site->id)->first()))
                            <div class="col-md-8" style="font-size: x-large;">
                                {{ $site_city_m2m->address }}
                            </div>
                        @else
                            <div class="col-md-8" style="font-size: x-large;">195112, РФ, Санкт-Петербург, пр.
                                Малоохтинский, д. 16/12, лит. А, пом. 9-Н
                            </div>
                        @endif
                    </div>

                    <div class="row" style="margin-top: 10px; border-bottom: solid">
                        <div class="col-md-4">Адрес отпуска препаратов:</div>
                        @if(!config('api.none_city') && !is_null($site_city_m2m = $curr_city->site_m2m()->where('site_id', $curr_site->id)->first()))
                            <div class="col-md-8" style="font-size: x-large;">
                                {{ $site_city_m2m->address }}
                            </div>
                        @else
                            <div class="col-md-8" style="font-size: x-large;">
                                195112, РФ, Москва, ул. Долгопрудная, д. 11
                            </div>
                        @endif
                    </div>

                    <div class="row" style="margin-top: 10px; border-bottom: solid">
                        <div class="col-md-4">Телефон:</div>
                        @if(!config('api.none_city') && !is_null($site_city_m2m = $curr_city->site_m2m()->where('site_id', $curr_site->id)->first()))
                            <div class="col-md-8" style="font-size: x-large;">
                                {{ $site_city_m2m->phone }}
                            </div>
                        @else
                            <div class="col-md-8" style="font-size: x-large;">
                                <a class="btn btn-primary buy-for-one-click" data-toggle="modal"
                                   data-target="#buy-for-one-click"
                                   data-id="0" data-name=""
                                   data-kolvo="">
                                    <i class="glyphicon glyphicon-hand-up"></i>
                                    <span>&nbsp;Заказать обратный звонок</span>
                                </a>
                            </div>
                        @endif

                    </div>

                    <div class="row" style="margin-top: 10px; border-bottom: solid">

                        <div class="col-md-4">Режим работы:</div>
                        <div class="col-md-8" style="font-size: x-large;">24 часа 7 дней в неделю</div>
                    </div>

                    <div class="row" style="margin-top: 10px; border-bottom: solid">

                        <div class="col-md-4">Доставка:</div>
                        <div class="col-md-8" style="font-size: x-large;">по всей России</div>

                    </div>

                </div>


                <div class="row">
                    <div class="h2">Лицензии</div>
                    <div class="col-md-12">
                        <img src="{{ url('images/lic/1.jpg') }}" class="img-responsive" alt="Лицензия">
                    </div>
                    <div class="col-md-12">
                        <img src="{{ url('images/lic/2.jpg') }}" class="img-responsive" alt="Лицензия">
                    </div>
                    <div class="col-md-12">
                        <img src="{{ url('images/lic/3.jpg') }}" class="img-responsive" alt="Лицензия">
                    </div>
                    <div class="col-md-12">
                        <img src="{{ url('images/lic/4.jpg') }}" class="img-responsive" alt="Лицензия">
                    </div>
                </div>

            </div>


        </div>
    </div>


@endsection