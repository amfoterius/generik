@extends('base')


@section('content')
    <div class="container">


        <div class="row">
            <div class="col-md-12">

                <div class="row">

                    <div class="col-md-12">
                        @include('components.categories_grid',
                        [
                            'title' => ' ',
                             'categories' => \App\Product_category::whereIn('id', [137, 144, 149, 169, 140, 142])->get(),
                             'curr_category' => $curr_category,
                         ]
                         );
                    </div>
                </div>


                <div class="row" style="margin-top: 60px;">
                    <div class="col-md-12">
                        @include('components.categories_grid',
                        [
                            'title' => 'Препараты похожие по действию на ' . $curr_category->name,
                             'categories' => \App\Product_category::whereIn('id', [139, 138, 170])->get(),
                             'curr_category' => $curr_category,
                         ]
                         );
                    </div>
                </div>

                <div class="row" style="margin-top: 60px;">
                    <div class="col-md-12">
                        @include('components.categories_grid',
                        [
                            'title' => 'Вместе с аналогами ' . $curr_category->name_r . ' так же покупают',
                             'categories' => \App\Product_category::whereIn('id', [148, 146, 161])->get(),
                             'curr_category' => $curr_category,
                         ]
                         );
                    </div>
                </div>





                <div class="row" style="margin-top: 80px;">
                    <div class="col-md-12 h2 text-center">
                        Сравнение аналогов {{ $curr_category->name_r }}
                    </div>
                </div>

                <div class="row" style="margin-top: 20px">
                    <div class="col-md-12">
                        @include('components.categories_compare', [
                        'categories' =>  \App\Product_category::whereIn('id', [144, 137, 143])->get(),
                        'category_options' => \App\Product_category_option::whereIn('id', [2,4,3])->get(),
                        'curr_category' => $curr_category,
                        ])
                    </div>
                </div>

            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                @include('components.category_reviews', [
                'curr_category' => $curr_category,
                 'curr_site' => $curr_site,
                 'options' => [
                    'hide_head' => true,
                 ],
                 ])

            </div>
        </div>

    </div>

@endsection

