@extends('base')

@section('head')
    <meta name="robots" content="noindex"/>
@endsection

@section('content')

    <div class="container">
        <div class="h1">Сравнение товаров</div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th></th>
                        @foreach($categories as $category)
                            <th style="">
                                <div></div>
                                <a href="{{ $category->getUrl() }}"  style="margin-top: 5px">

                                    <img src="[domain]{{ $category->getImageUrl('img_small') }}"
                                         class="img-responsive" alt="" style="width: 50px; height: 50px; float: left; ">
                                    <span>{{ $category->name }}</span>

                                </a>
                            </th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="font-size: smaller; font-weight: bold;">
                            Действие
                        </td>
                        @foreach($categories as $category)
                            <td style="font-size: large">
                                <a href="{{ $category->type->getUrl() }}">{{ $category->type->name }}</a>
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td style="font-size: smaller; font-weight: bold;">
                            Цена от
                        </td>
                        @foreach($categories as $category)
                            <td style="font-size: large">{{ $category->min_price_item()['price'] }} р</td>
                        @endforeach
                    </tr>


                    <tr>
                        <td style="font-size: smaller; font-weight: bold;">
                            Общий рейтинг
                        </td>
                        @foreach($categories as $category)
                            <td style="font-size: large">@include('components.rating_stars', ['rating'=>$category->avg_rating()])</td>
                        @endforeach
                    </tr>

                    @foreach($category_options as $option)
                        <tr>
                            <td>{{ $option->name }}</td>
                            @foreach($categories as $category)
                                <td>{!! $category->options_m2m()->where('option_id', $option->id)->first()->value or '' !!}</td>
                            @endforeach
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </div>


@endsection