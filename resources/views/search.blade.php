@extends('base')

@section('meta_title')Поиск по сайту@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! $curr_site->search_result_code !!}
            </div>
        </div>
    </div>

@endsection