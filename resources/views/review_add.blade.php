@extends('base')

@section('meta_title')Добавить отзыв о {{ $curr_category->name_p }}@endsection
@section('meta_description')Страница по добавлению отзыва о препарате {{ $curr_category->name }}@endsection

@section('head')
    <meta name="robots" content="noindex nofollow"/>
@endsection

@section('content')
    <div class="container">

        <div class="col-md-9">
            <h1>
                Мой отзыв о
                <a href="{{ $curr_category->getUrl() }}">
                    {{ $curr_category->name }}
                </a>
            </h1>

            <form method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Оцените качества</h3>

                        <dl id="input-ratings" class="dl-horizontal">

                            @foreach($rating_options as $option)
                                <dt>
                                    {{ $option->name }}
                                </dt>
                                <dd>
                                    <div id="option-stars-{{ $option->id }}" class="input-review-stars"
                                         style="float: left">
                                        <input name="option-{{ $option->id }}" id="input-option-stars-{{ $option->id }}"
                                               type="hidden" value="-1">
                                        <i class="glyphicon glyphicon-star-empty" data-n="0"
                                           data-input-id="input-option-stars-{{ $option->id }}"></i>
                                        <i class="glyphicon glyphicon-star-empty" data-n="1"
                                           data-input-id="input-option-stars-{{ $option->id }}"></i>
                                        <i class="glyphicon glyphicon-star-empty" data-n="2"
                                           data-input-id="input-option-stars-{{ $option->id }}"></i>
                                        <i class="glyphicon glyphicon-star-empty" data-n="3"
                                           data-input-id="input-option-stars-{{ $option->id }}"></i>
                                        <i class="glyphicon glyphicon-star-empty" data-n="4"
                                           data-input-id="input-option-stars-{{ $option->id }}"></i>


                                    </div>

                                </dd>
                            @endforeach

                        </dl>
                    </div>


                    <div class="col-md-8">

                        <h3>Заполните информацию</h3>

                        <div class="row">
                            <input type="hidden" name="site_id" value="{{ $curr_site->id }}">
                            <div class="col-md-6"><input name="name" type="text" class="form-control" placeholder="Ваше имя*"></div>
                            <div class="col-md-6"><input name="email" type="email" class="form-control" placeholder="Ваш email*">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-success">Положительные качества</h5>
                                <textarea name="positive" class="form-control" rows="5"></textarea>
                            </div>
                            <div class="col-md-12">
                                <h5 class="text-danger">Отрицательные качества</h5>
                                <textarea name="negative" class="form-control" rows="5"></textarea>
                            </div>


                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-success btn-lg" value="Отправить отзыв">
                            </div>
                        </div>


                    </div>
                </div>
            </form>

        </div>


    </div>
@endsection

@section('js')
    @parent

    <script>
        $(document).ready(function () {

            var $blocksStars = $(".input-review-stars");
            var $input = $blocksStars.find('input');
            var $inputStars = $blocksStars.find('i');

            $inputStars.hover(function (e) {
                var curr = $(this).data('input-id');

                $currArrI = $inputStars.filter(function () {
                    return $(this).data("input-id") == curr;
                });


                for (var i = 0; i <= 4; i++) {
                    if ($(this).data('n') >= i) {
                        $($currArrI[i])
                                .removeClass('glyphicon-star-empty')
                                .addClass('glyphicon-star');

                    }
                    else {
                        $($currArrI[i])
                                .removeClass('glyphicon-star')
                                .addClass('glyphicon-star-empty');


                    }


                }


            }, function (e) {
                var curr = $(this).data('input-id');


                $currArrI = $inputStars.filter(function () {
                    return $(this).data("input-id") == curr;
                });

                for (var i = 0; i <= 4; i++) {
                    var v = parseInt($("#" + curr).val());
                    if (v >= i) {
                        $($currArrI[i])
                                .removeClass('glyphicon-star-empty')
                                .addClass('glyphicon-star');

                    }
                    else {
                        $($currArrI[i])
                                .removeClass('glyphicon-star')
                                .addClass('glyphicon-star-empty');


                    }

                }

            })

            $inputStars.click(function (e) {

                $("#" + $(this).data('input-id')).val($(this).data('n'));

            })


        })
    </script>


@endsection