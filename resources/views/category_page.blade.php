@extends('base')

@section('meta_title'){{ $site_category_page->meta_title or $curr_category_page->page->name }}@endsection
@section('meta_description'){{ $site_category_page->meta_description or '' }}@endsection
@section('meta_keywords'){{ $site_category_page->meta_keywords or '' }}@endsection

@section('head')
    <meta property="og:title"
          content="{{ $curr_category_page->seo()->where('site_id', $curr_site->id)->first()->meta_title or $curr_category_page->page->name }}"/>
    <meta property="og:type" content="website"/>

    @if(count($curr_category->images) > 0)
        <meta property="og:image"
              content="[domain]{{ $curr_category->images[0]->getImageUrlResize('image') }}"/>
    @else
        <meta property="og:image" content="[domain]{{ $curr_category->getImageUrl('img_big') }}"/>
    @endif

    <meta property="og:description"
          content="{{ $curr_category_page->seo()->where('site_id', $curr_site->id)->first()->meta_description or '' }}"/>
    <meta property="og:site_name" content="{{ $curr_site->name }}"/>
    <meta property="og:url" content="{{ $curr_category_page->getUrl() }}"/>

@endsection


@section('header')
    @parent
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="[domain]/каталог" itemprop="url">
                <span itemprop="title">Каталог</span>
            </a>
        </li>

        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="{{ $curr_category->type->getUrl() }}" itemprop="url"
               title="{{ $curr_category->type->name }}">
                <span itemprop="title">{{ str_limit($curr_category->type->name, 30) }}</span>
            </a>
        </li>
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="{{ $curr_category->getUrl() }}" itemprop="url"
               title="{{ $curr_category->name }}">
                <span itemprop="title">{{ $curr_category->name, 20 }}</span>
            </a>
        </li>
        <li class="active">
            {{ $curr_category_page->page->name, 20 }}
        </li>
    </ol>
@endsection


@section('content')
    <div class="container">

        <div class="row">

            <div id="category-page-body" class="col-md-9">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="text-primary">
                            <span>{{ $curr_category->name . ' ' . mb_strtolower($curr_category_page->page->name) . ' в [curr_city.name_p]' }}</span>
                        </h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        {!! $site_category_page->base_text or '' !!}
                    </div>
                </div>


                {{-- Fast buy --}}
                @if($curr_category->buy && $site_category_page->page->id <> 3)
                    @include('components.category.category_fast_buy_panel', ['category'=>$curr_category])
                @endif


                <div class="row" style="margin-top: 40px">
                    @include('components.category_pages_nav', [
                'pages' => $curr_category->pages_m2m()->where('site_id', $curr_site->id)->get(),
                 'curr_page' => $curr_page,
                 'curr_category_page' => $curr_category_page,
                 ])
                </div>

                <div class="row">
                    <div id="category_page_content--{{ $curr_category_page->page->id }}" class="col-md-12">
                        {!! $base_content !!}
                    </div>
                </div>

                <div class="row">
                    <h2>{{ $curr_category_page->page->name }} аналогов</h2>
                    <ol>
                    @foreach($curr_category->type->categories()->where('buy', 1)->get() as $category)

                        @if(!is_null($analog_category_page = $category->pages_m2m()->where('site_id', $curr_site->id)->where('page_id', $curr_category_page->page_id)->first()))
                            <li>
                                <a href="{{ $analog_category_page->getUrl() }}">
                                    {{ $category->name.', '.$curr_category_page->page->name }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                    </ol>
                </div>

                <div class="row" style="margin-top: 40px">
                    @include('components.category_pages_nav', [
                'pages' => $curr_category->pages_m2m()->where('site_id', $curr_site->id)->get(),
                 'curr_page' => $curr_page,
                 'curr_category_page' => $curr_category_page,
                 ])
                </div>

                {{-- Fast buy --}}
                @if($curr_category->buy)
                    @include('components.category.category_fast_buy_panel', ['category'=>$curr_category])
                @endif

                @if($curr_category_page->page->show_page_reviews)

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2>Отзывы</h2>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input id="new-review-page-name" type="text" class="form-control"
                                       placeholder="Ваше имя">
                            </div>
                            <div class="form-group">
                                <textarea id="new-review-page-text" class="form-control" rows="6"
                                          placeholder="Текст отзыва"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button id="new-review-page-add" class="btn btn-primary"
                                    data-category-page-id="{{ $curr_category_page->id }}"
                                    data-site-id="{{ $curr_site->id }}">
                                Добавить отзыв
                            </button>
                        </div>
                    </div>

                    [blocks_no_cache.reviews]


                @endif

                <div class="row">
                    <div class="col-md-12 text-center" style="font-weight: bolder; font-size: 110%;">
                        Понравилось у нас? Поделись с друзьями
                        <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"
                                charset="utf-8"></script>
                        <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                        <div class="ya-share2" style="margin-top: 5px;"
                             data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp"></div>
                    </div>
                </div>


            </div>

            <div class="col-md-3">


                <div class="hidden-xs hidden-sm">
                    <div class="h3 text-center">Все товары</div>
                    @include('components.categories_column', ['types'=>$types, 'curr_category' => $curr_category])
                </div>
            </div>


        </div>
    </div>


@endsection

@section('js')
    @parent
    <script>

        var domain = '{{ url('') }}/';

        $(document).ready(function (e) {

            $(".review div .info").click(function (e) {
                e.preventDefault();

                var $fullBlock = $(this).parent().parent().find('.panel-body:last');
                var $miniBlock = $(this).parent().parent().find('.panel-body:first');

                if ($fullBlock.hasClass('hidden')) {
                    $fullBlock.removeClass('hidden');
                    $miniBlock.addClass('hidden');
                    $(this).text('Скрыть');
                } else {
                    $fullBlock.addClass('hidden');
                    $miniBlock.removeClass('hidden');
                    $(this).text('Подробнее');
                }

                return false;
            })


            /*
             $(".add-to-cart").on('click', function () {



             $.ajax({
             url: "
            {{ url('ajax/cart/add') }}",
             method: 'POST',
             data: {
             "product_id": $(this).data('id'),
             "product_count": 1
             }
             })
             .done(function (data) {
             var c = parseInt($("#cart-count").text());

             $("#cart-count").text((c + 1).toString());
             })
             .fail(function (data) {
             //
             })
             .always(function (data) {
             });

             });
             */
        })

    </script>

@endsection