<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="[token]"/>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <title>@yield('meta_title', '[page_meta.meta_title]')</title>
    <link rel="shortcut icon" href="{{ url('favicons/'.$curr_site->id.'.png') }}" type="image/png">

    <meta name="description" content="@yield('meta_description', '[page_meta.meta_description]')"/>
    <meta name="robots" content="noodp"/>
    <meta name="yandex-verification" content="0efe7e2509dd46b9"/>
    <meta name="google-site-verification" content="CErbdXwrLTl6yU5GVtXKLbfKDSdhABO3pwsnFShiNQg"/>
    <meta name="keywords" content="@yield('meta_keywords', '[page_meta.meta_keywords]')"/>

    @yield('head')

    <link rel="stylesheet"
          href="{{ $domain . "css/app" . ($curr_site->template == 0 ? '' : $curr_site->template) . ".css?" . config('api.reset_cache') }}">
    <link rel="stylesheet" href="{{ $domain . 'css/lightbox.min.css' }}">


</head>
<body>

<nav class="navbar navbar-default" role="navigation" style="margin-top: 10px;">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>


            <a id="logo" class="navbar-brand" href="/">
                <div class="image"></div>
                <div class="text">{{ $curr_site->id == 9 ? '' : $curr_site->name }}</div>
            </a>



        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-top: 40px;">
            <ul class="nav navbar-nav">
                @if(!config('api.is_one_city'))
                    <li>
                        <a href="[domain]города" style="color: #337ab7; text-decoration: dashed; margin-left: 8px;">
                            {{ config('api.none_city') ? 'Выбрать город' : 'в [curr_city.name_p]' }}
                        </a>
                    </li>
                @endif
                <li class="{{ $page_is_catalog or '' }}">
                    <a href="[domain]каталог">
                        Каталог
                    </a>
                </li>
                <li class="{{ $page_is_delivery or '' }}">
                    <a href="[domain]доставка">
                        Доставка
                    </a>
                </li>
                <li class="{{ $page_is_delivery or '' }}">
                    <a href="[domain]скидки">
                        Скидки
                    </a>
                </li>
                <li class="{{ $page_is_contacts or '' }}">
                    <a href="[domain]контакты">
                        Контакты
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="[domain]сравнение"
                       class="{{ $compare->count() ? "text-primary-inherit" : "" }}">
                        Сравнение<span id="compare-count" class="badge pull-right"> {{ $compare->count() }}</span>
                    </a>

                </li>
                <li>
                    <a href="[domain]корзина"
                       class="{{ $cart_count ? "text-primary-inherit" : "" }}">
                        <i class="menu-icon glyphicon glyphicon-shopping-cart"></i>
                        Корзина <span id="cart-count" class="badge pull-right">[cart_count]</span>
                    </a>

                </li>
            </ul>


        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            {!! $curr_site->search_code or '' !!}
        </div>
    </div>


    <div class="row">
        @yield('breadcrumb')

    </div>
</div>


@yield('content')


<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="">
                    <a href="[domain]">
                        Главная
                    </a>
                </li>
                <li class="">
                    <a href="[domain]каталог">
                        Каталог
                    </a>
                </li>
                <li class="">
                    <a href="[domain]доставка">
                        Доставка
                    </a>
                </li>
                <li class="">
                    <a href="[domain]контакты">
                        Контакты
                    </a>
                </li>
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>


@section('modal-buy-for-one-click')

    <div class="modal fade" id="buy-for-one-click" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Быстрый заказ</h4>
                </div>
                <div class="modal-body">

                    <form id="form-order" action="[domain]ajax/cart/new_order" role="form" method="post">
                        <input type="hidden" name="_token" value="[token]">
                        <input type="hidden" name="product-id" class="input-product-id">
                        <input type="hidden" name="callback" value="1">

                        <div class="h4 row product-name" style="margin-left: 10px;"></div>

                        <div class="form-group">
                            <label>ФИО</label>
                            <input name="fio" type="text" class="form-control" id=""
                                   placeholder="Например: Иванов Иван Иванович"
                                   required>
                        </div>
                        <div class="form-group">
                            <label>Номер телефона</label>
                            <input name="tel" type="number" class="form-control" id=""
                                   placeholder="Например: 89001231212"
                                   required>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Отправить">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@show

@section('modal-on-cart')
    <div class="modal fade" id="on-cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Товар добавлен в корзину</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="http://generik.ru:81/uploads/api/images/categories/4-b.jpg" alt=""
                                 class="img-responsive" id="on-cart-img">
                        </div>
                        <div class="col-md-8">
                            <div class="h3" id="on-cart-name"></div>
                            <div class="row">
                                <div class="col-md-6" id="on-cart-kolvo"></div>
                                <div class="col-md-6" id="on-cart-price"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            В корзине <span id="on-carl-length"></span> товара(ов)
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="[domain]корзина" class="btn btn-success">Перейти к
                                оформлению заказа</a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@show

@section('panel-compare')
    <div class="row">

    </div>

@show


@section('js')


    <script src='https://www.google.com/recaptcha/api.js' async></script>
    <script src="{{ $domain . 'js/jquery.min.js' }}"></script>
    <script src="{{ $domain . 'js/bootstrap.min.js' }}"></script>
    <script src="{{ $domain . 'js/lightbox.min.js' }}"></script>
    <script src="{{ $domain . 'js/holder.min.js' }}"></script>
    <script src="{{ $domain . 'js/app.js' }}"></script>

    <script>
        var URL = '[domain]';
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });


    </script>
    <!--<script src="js/app.js"></script>-->

    <!-- Chat -->
    <script>


        ChatraID = 'RoYAsTdokggPNp6J7';
        (function (d) {
            var n = d.getElementsByTagName('script')[0], s = d.createElement('script');
            s.async = true;
            s.src = (d.location.protocol === 'https:' ? 'https:' : 'http:') + '//chat.chatra.io/chatra.js';
            n.parentNode.insertBefore(s, n);
        })(document);

    </script>


    <!-- END Chat -->

@show
<script>
    $(document).ready(function () {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>

<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button"
   title="Нажмите, чтобы вернуться на начало страницы" data-toggle="tooltip" data-placement="left"><span
            class="glyphicon glyphicon-chevron-up"></span></a>

@if($curr_site->metrika)
    {!! $curr_site->metrika !!}
@endif

<div
        class="fb-like"
        data-share="true"
        data-width="450"
        data-show-faces="true">
</div>

</body>
</html>
