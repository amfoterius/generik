@extends('base')

@section('meta_title'){{ $seo['category_title'] }}@endsection

@section('meta_description'){{ $seo['category_description'] }}@endsection

@section('meta_keywords'){{ $seo['category_keywords'] }}@endsection



@section('header')
    @parent
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('catalog') }}">
                Каталог
            </a>
        </li>
        <li>
            <a href="{{ route('type', [$curr_category->type->alias, mb_strtolower(str_replace(" ", "-", $curr_city->name_p))]) }}"
               title="{{ $curr_category->type->name }}">
                {{ str_limit($curr_category->type->name, 30) }}
            </a>
        </li>
        <li class="active">
            {{ $curr_category->name, 20 }}
        </li>
    </ol>
@endsection


@section('content')
    <div class="container" itemscope itemtype="http://schema.org/Product">


        <div class="row">

            <div class="col-md-9">

                <h1 class="text-primary">
                    <span itemprop="name">{{ $curr_category->name }}</span>
                </h1>

                <div itemprop="aggregateRating"
                     itemscope itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="3.5">
                    <meta itemprop="reviewCount" content="11">
                </div>
                <meta itemprop="productID" content="{{ $curr_category->id }}">


                <div class="row" itemprop="description">
                    <p>
                        [curr_category.name] — [curr_category.name] препарат для повышение потенции. На нашем сайте Вы можете ознакомиться с отзывами о препарате, посмотреть описание, аналоги, инструкцию по применению, сравнить с другими, а так же приобрести в [curr_city.name_p] по низким ценам.
                    </p>
                </div>

                <div class="row">
                    <div class="col-md-4">

                        <div class="col-md-12">
                            <img src="[domain]{{ $curr_category->getImageUrl('img_big') }}" itemprop="image"
                                 class="img-responsive"
                                 alt="Responsive image" style="margin: 0 auto">
                        </div>


                        <div class="col-md-12 text-center">
                            @include('components.rating_stars', ['rating'=>$curr_category->avg_rating()])
                        </div>
                    </div>


                    <div class="col-md-8">
                        <table class="table table-hover">
                            <thead>

                            </thead>


                            <tbody>
                            @foreach($curr_category->options as $option_m2m)


                                <tr>
                                    <td>
                                        <strong>{{ $option_m2m->option->name }}</strong>
                                    </td>
                                    <td class="">
                                        {!! $option_m2m->value !!}
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="bs-callout bs-callout-info">
                            <div class="row">
                                <img src="{{ url('icons/delivery50.svg') }}"
                                     style="width: 100px; float: left; margin: 20px"
                                     class="img-responsive"
                                     alt="Responsive image">

                                <div class="h3 text-primary">Стоимость доставки: 200 руб.</div>
                                <div class="h5">Доставка в любой регион России, включая Крым</div>
                                <div class="h4 text-primary">
                                    В Москве и Санкт-Петербурге работает курьерская доставка. Доставка почтой 1-ого
                                    класса
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-md-12">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">Количество</th>
                                <th class="text-center">Цена за ед.</th>
                                <th class="text-center">Стоимость</th>
                                <th class="text-center">Бонусы</th>
                                <th class="text-center">
                                    Заказать
                                </th>

                            </tr>
                            </thead>

                            <?php $req_for_items = $curr_category->products()->select(DB::raw('max(price_per_item) as max_price, min(price_per_item) as min_price, count(*) as c'))->first() ?>

                            <tbody itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
                            <meta itemprop="lowPrice" content="{{ $req_for_items->min_price }}">
                            <meta itemprop="highPrice" content="{{ $req_for_items->max_price }}">
                            <meta itemprop="offerCount" content="{{ $req_for_items->c }}">
                            <meta itemprop="priceCurrency" content="RUB">

                            @foreach($curr_category->products as $product)



                                <tr itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                    <td class="text-center">{{ $product->kolvo }}</td>
                                    <td class="text-primary text-center" itemprop="price">
                                        <meta itemprop="priceCurrency" content="RUB">
                                        {{ $product->price_per_item }}
                                        RUB
                                    </td>
                                    <td class="text-primary text-center" itemprop="price">
                                        {{ $product->price }}
                                        RUB
                                    </td>
                                    <td></td>
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <a class="btn btn-success add-to-cart" data-id="{{ $product->id }}"
                                               data-toggle="modal"
                                               data-target="#on-cart"
                                               data-img-src="{{ $curr_category->img_big }}"
                                               data-name="{{ $curr_category->name }}"
                                               data-price="{{ $product->price }}"
                                               data-kolvo="{{ $product->kolvo }}">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                В корзину
                                            </a>
                                            <button type="button" class="btn btn-primary">
                                                <i class="glyphicon glyphicon glyphicon-hand-up"></i>
                                                Заказать в один клик
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="bs-callout bs-callout-info">
                            <div class="row">
                                <img src="{{ url('icons/discount4.svg') }}"
                                     style="width: 100px; float: left; margin: 20px"
                                     class="img-responsive"
                                     alt="Responsive image">

                                <div class="h3 text-primary">Дорого? Можно уменьщить цену</div>
                                <div class="h4 text-default">Для оплаты используйте электронный кошелек</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <h2>Отзывы о препарате</h2>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('category_review_add', [$curr_city->alias, $curr_type->alias, mb_strtolower(str_replace(" ", "-", $curr_city->name_p)), $curr_category->alias, mb_strtolower(str_replace(" ", "-", $curr_city->name_p))]) }}"
                                   class="btn btn-success" style="margin-top: 12px; width: 100%;">

                                    Оставить отзыв
                                </a>
                            </div>
                        </div>


                        <form role="form"
                              style="display: none; border: solid 1px #4c4c4c; border-radius: 12px; padding: 5px; margin-bottom: 10px">
                            <div class="form-group">
                                <label class="sr-only" for="user-name">Name</label>
                                <input type="text" class="form-control" id="user-name" placeholder="Ваше имя">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="user-email">email</label>
                                <input type="email" class="form-control" id="user-email" placeholder="Ваш email">
                            </div>
                            <p>
                                <textarea class="form-control" name="text" rows="4"></textarea>
                            </p>
                        </form>


                        @foreach($curr_category->reviews as $review)
                            <div class="panel panel-default review">
                                <div class="panel-heading h5" style="margin-top: 0">
                                    {{ $review->name }}
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <dl class="dl-horizontal">
                                                @foreach($review->options_m2m as $option_m2m)
                                                    <dt style="width: 95px">{{ $option_m2m->option->name }}</dt>
                                                    <dd style="margin-left: 115px;">@include('components.rating_stars', ['rating'=>$option_m2m->stars])</dd>
                                                @endforeach
                                            </dl>
                                        </div>
                                        <div class="col-md-8">

                                            <p>

                                            <div class="h4 text-success">Положительные качества:</div>
                                            <div style="padding-left: 30px;">{!! $review->positive !!}</div>

                                            </p>
                                            <p>

                                            <div class="h4 text-danger">Отрицательные качества:</div>
                                            <div style="padding-left: 30px;">{!! $review->negative !!}</div>

                                            </p>
                                            <p>

                                            <div class="h4">Отзыв:</div>
                                            <div style="padding-left: 30px;">{!! $review->text !!}</div>
                                            </p>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="row">

                </div>


            </div>
            <div class="col-md-3">
                <div class="h3 text-center">Страницы товара {{ $curr_category->name }}</div>
                @include('components.category_tabs', [
                'curr_type'=>$curr_type,
                'curr_category'=>$curr_category,
                'curr_page'=>null,
                ])

                <div class="hidden-xs hidden-sm">
                    <div class="h3 text-center">Все товары</div>
                    @include('components.categories_column', ['types'=>$types, 'curr_category' => $curr_category])
                </div>

            </div>



        </div>

    </div>


@endsection

@section('js')
    @parent
    <script>

        var domain = '{{ url('') }}/';

        $(document).ready(function (e) {

            $(".review div .info").click(function (e) {
                e.preventDefault();

                var $fullBlock = $(this).parent().parent().find('.panel-body:last');
                var $miniBlock = $(this).parent().parent().find('.panel-body:first');

                if ($fullBlock.hasClass('hidden')) {
                    $fullBlock.removeClass('hidden');
                    $miniBlock.addClass('hidden');
                    $(this).text('Скрыть');
                } else {
                    $fullBlock.addClass('hidden');
                    $miniBlock.removeClass('hidden');
                    $(this).text('Подробнее');
                }

                return false;
            });


            /*
             $(".add-to-cart").on('click', function () {



             $.ajax({
             url: "
            {{ url('ajax/cart/add') }}",
             method: 'POST',
             data: {
             "product_id": $(this).data('id'),
             "product_count": 1
             }
             })
             .done(function (data) {
             var c = parseInt($("#cart-count").text());

             $("#cart-count").text((c + 1).toString());
             })
             .fail(function (data) {
             //
             })
             .always(function (data) {
             });

             });
             */
        })

    </script>

@endsection