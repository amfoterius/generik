{{ Log::error('404: ' . URL::full()) }}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Такой страницы не существует</title>

    <link href="{{ url('css/app.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="text-center">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> 404:
                        <small>Страница не найдена</small>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="h3" style="width: 100%; margin: 0 auto; text-align: center">
                                К сожалению, по вашему запросу страница не найдена <a href="{{ url() }}" class="btn btn-primary btn-lg">На главную</a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>
</body>
</html>