@extends('base')




@section('header')

@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="[domain]" itemprop="url">
                <span itemprop="title">Главная</span>
            </a>
        </li>
        <li class="active">
            Каталог
        </li>
    </ol>
@endsection


@section('content')
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-12">
                <div class="row h1">Каталог</div>


                <div class="row">
                    <div class="col-md-12">

                        @foreach($types as $type)
                            @if(count($type->categories) > 0)
                                <div class="row" style="margin-bottom: 100px">
                                    <div class="col-md-12" itemscope="" itemtype="http://schema.org/ItemList">
                                        <meta itemprop="numberOfItems" content="{{ count($type->categories) }}">
                                    </div>
                                </div>
                                
                                @if(!empty($type->image_banner))
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div itemscope itemtype="http://schema.org/ImageObject">

                                                <meta itemprop="name" content="">
                                                <img src="{{ $type->getImageUrl('image_banner') }}?{{ config('api.reset_cache') }}"
                                                     style="width: 100%" itemprop="contentUrl" alt="{{ $type->name }} цены отзывы дженерик дешево"/>
                                                <meta itemprop="description" content="{{ $type->name }} цены отзывы дженерик дешево">

                                            </div>

                                        </div>
                                    </div>
                                    
                                @endif    
                            
                            
                                <div class="row">
                                    @foreach($type->categories()->where('buy', 1)->get() as $category)

                                        @include('components.product_block', ['category' => $category,
                                        'type' => $type,
                                        'curr_site' => $curr_site])
                                    @endforeach
                                </div>
                                <div class="row" style="height: 50px">
                                    <div class="col-md-12">
                                        <a style="width: 100%" href="{{ $type->getUrl() }}" class="btn btn-primary btn-lg">Все товары "{{ $type->name }}"</a>
                                    </div>
                                </div>

                            @endif

                        @endforeach

                    </div>


                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center" style="font-weight: bolder; font-size: 110%;">
                Понравилось у нас? Поделись с друзьями
                <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
                <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                <div class="ya-share2" style="margin-top: 5px;" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp"></div>
            </div>
        </div>


    </div>
@endsection



@section('js')
    @parent
@endsection