@extends('base')

@section('content')
    <div class="container" style="padding-top: 60px">
        <div class="row">
            <div id="content" class="col-sm-12"><h1>Корзина покупок &nbsp;(5.00kg)
                </h1>

                <form action="http://test15.ru/index.php?route=checkout/cart/edit" method="post"
                      enctype="multipart/form-data">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td class="text-center">Изображение</td>
                                <td class="text-left">Название</td>
                                <td class="text-left">Модель</td>
                                <td class="text-left">Количество</td>
                                <td class="text-right">Цена за шт.</td>
                                <td class="text-right">Всего</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products_on_cart as $product)
                                <tr>
                                    <td class="text-center"><a href="">
                                            <img src="{{ url('uploads/products/thumbs/small/'.$product['model']->image) }}" alt="{{ $product['model']->name }}" class="img-thumbnail" style="width: 100px; height: 100px">
                                        </a>
                                    </td>
                                    <td class="text-left">
                                        <a href="">{{ str_limit($product['model']->name, 100) }}</a>
                                    </td>
                                    <td class="text-left"></td>
                                    <td class="text-left">
                                        {{ $product['count'] }}
                                    </td>
                                    <td class="text-right">100.00 руб.</td>
                                    <td class="text-right">100.00 руб.</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </form>
                <h2>Что бы вы хотели сделать дальше?</h2>

                <p>Если у вас есть код купона на скидку или бонусные баллы, которые вы хотите использовать, выберите
                    соответствующий пункт ниже. А также, Вы можете приблизительно узнать стоимость доставки в ваш
                    регион.</p>

                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse-coupon" class="accordion-toggle collapsed"
                                                       data-toggle="collapse" data-parent="#accordion"
                                                       aria-expanded="false">Использовать
                                    купон на скидку <i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div id="collapse-coupon" class="panel-collapse collapse" aria-expanded="false"
                             style="height: 0px;">
                            <div class="panel-body">
                                <label class="col-sm-2 control-label" for="input-coupon">Введите код Купона на
                                    скидку</label>

                                <div class="input-group">
                                    <input type="text" name="coupon" value="" placeholder="Введите код Купона на скидку"
                                           id="input-coupon" class="form-control">
                            <span class="input-group-btn">
                            <input type="button" value="Применение купона" id="button-coupon"
                                   data-loading-text="Загрузка..."
                                   class="btn btn-primary">
                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse-voucher" data-toggle="collapse"
                                                       data-parent="#accordion"
                                                       class="accordion-toggle collapsed" aria-expanded="false">Использовать
                                    Подарочный сертификат <i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div id="collapse-voucher" class="panel-collapse collapse" aria-expanded="false"
                             style="height: 0px;">
                            <div class="panel-body">
                                <label class="col-sm-2 control-label" for="input-voucher">Введите код Подарочного
                                    сертификата</label>

                                <div class="input-group">
                                    <input type="text" name="voucher" value=""
                                           placeholder="Введите код Подарочного сертификата"
                                           id="input-voucher" class="form-control">
                            <span class="input-group-btn">
                            <input type="submit" value="Применить подарочный сертификат" id="button-voucher"
                                   data-loading-text="Загрузка..."
                                   class="btn btn-primary">
                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse-shipping" class="accordion-toggle collapsed"
                                                       data-toggle="collapse" data-parent="#accordion"
                                                       aria-expanded="false">Оценки
                                    стоимости доставки <i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div id="collapse-shipping" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                <p>Укажите ваш регион для оценки стоимости доставки.</p>

                                <form class="form-horizontal">
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-country">Страна</label>

                                        <div class="col-sm-10">
                                            <select name="country_id" id="input-country" class="form-control">
                                                <option value=""> --- Выберите ---</option>
                                                <option value="244">Aaland Islands</option>
                                                <option value="1">Afghanistan</option>
                                                <option value="2">Albania</option>
                                                <option value="3">Algeria</option>
                                                <option value="4">American Samoa</option>
                                                <option value="5">Andorra</option>
                                                <option value="6">Angola</option>
                                                <option value="7">Anguilla</option>
                                                <option value="8">Antarctica</option>
                                                <option value="9">Antigua and Barbuda</option>
                                                <option value="10">Argentina</option>
                                                <option value="11">Armenia</option>
                                                <option value="12">Aruba</option>
                                                <option value="252">Ascension Island (British)</option>
                                                <option value="13">Australia</option>
                                                <option value="14">Austria</option>
                                                <option value="15">Azerbaijan</option>
                                                <option value="16">Bahamas</option>
                                                <option value="17">Bahrain</option>
                                                <option value="18">Bangladesh</option>
                                                <option value="19">Barbados</option>
                                                <option value="20">Belarus</option>
                                                <option value="21">Belgium</option>
                                                <option value="22">Belize</option>
                                                <option value="23">Benin</option>
                                                <option value="24">Bermuda</option>
                                                <option value="25">Bhutan</option>
                                                <option value="26">Bolivia</option>
                                                <option value="245">Bonaire, Sint Eustatius and Saba</option>
                                                <option value="27">Bosnia and Herzegovina</option>
                                                <option value="28">Botswana</option>
                                                <option value="29">Bouvet Island</option>
                                                <option value="30">Brazil</option>
                                                <option value="31">British Indian Ocean Territory</option>
                                                <option value="32">Brunei Darussalam</option>
                                                <option value="33">Bulgaria</option>
                                                <option value="34">Burkina Faso</option>
                                                <option value="35">Burundi</option>
                                                <option value="36">Cambodia</option>
                                                <option value="37">Cameroon</option>
                                                <option value="38">Canada</option>
                                                <option value="251">Canary Islands</option>
                                                <option value="39">Cape Verde</option>
                                                <option value="40">Cayman Islands</option>
                                                <option value="41">Central African Republic</option>
                                                <option value="42">Chad</option>
                                                <option value="43">Chile</option>
                                                <option value="44">China</option>
                                                <option value="45">Christmas Island</option>
                                                <option value="46">Cocos (Keeling) Islands</option>
                                                <option value="47">Colombia</option>
                                                <option value="48">Comoros</option>
                                                <option value="49">Congo</option>
                                                <option value="50">Cook Islands</option>
                                                <option value="51">Costa Rica</option>
                                                <option value="52">Cote D'Ivoire</option>
                                                <option value="53">Croatia</option>
                                                <option value="54">Cuba</option>
                                                <option value="246">Curacao</option>
                                                <option value="55">Cyprus</option>
                                                <option value="56">Czech Republic</option>
                                                <option value="237">Democratic Republic of Congo</option>
                                                <option value="57">Denmark</option>
                                                <option value="58">Djibouti</option>
                                                <option value="59">Dominica</option>
                                                <option value="60">Dominican Republic</option>
                                                <option value="61">East Timor</option>
                                                <option value="62">Ecuador</option>
                                                <option value="63">Egypt</option>
                                                <option value="64">El Salvador</option>
                                                <option value="65">Equatorial Guinea</option>
                                                <option value="66">Eritrea</option>
                                                <option value="67">Estonia</option>
                                                <option value="68">Ethiopia</option>
                                                <option value="69">Falkland Islands (Malvinas)</option>
                                                <option value="70">Faroe Islands</option>
                                                <option value="71">Fiji</option>
                                                <option value="72">Finland</option>
                                                <option value="74">France, Metropolitan</option>
                                                <option value="75">French Guiana</option>
                                                <option value="76">French Polynesia</option>
                                                <option value="77">French Southern Territories</option>
                                                <option value="126">FYROM</option>
                                                <option value="78">Gabon</option>
                                                <option value="79">Gambia</option>
                                                <option value="80">Georgia</option>
                                                <option value="81">Germany</option>
                                                <option value="82">Ghana</option>
                                                <option value="83">Gibraltar</option>
                                                <option value="84">Greece</option>
                                                <option value="85">Greenland</option>
                                                <option value="86">Grenada</option>
                                                <option value="87">Guadeloupe</option>
                                                <option value="88">Guam</option>
                                                <option value="89">Guatemala</option>
                                                <option value="256">Guernsey</option>
                                                <option value="90">Guinea</option>
                                                <option value="91">Guinea-Bissau</option>
                                                <option value="92">Guyana</option>
                                                <option value="93">Haiti</option>
                                                <option value="94">Heard and Mc Donald Islands</option>
                                                <option value="95">Honduras</option>
                                                <option value="96">Hong Kong</option>
                                                <option value="97">Hungary</option>
                                                <option value="98">Iceland</option>
                                                <option value="99">India</option>
                                                <option value="100">Indonesia</option>
                                                <option value="101">Iran (Islamic Republic of)</option>
                                                <option value="102">Iraq</option>
                                                <option value="103">Ireland</option>
                                                <option value="254">Isle of Man</option>
                                                <option value="104">Israel</option>
                                                <option value="105">Italy</option>
                                                <option value="106">Jamaica</option>
                                                <option value="107">Japan</option>
                                                <option value="257">Jersey</option>
                                                <option value="108">Jordan</option>
                                                <option value="109">Kazakhstan</option>
                                                <option value="110">Kenya</option>
                                                <option value="111">Kiribati</option>
                                                <option value="113">Korea, Republic of</option>
                                                <option value="253">Kosovo, Republic of</option>
                                                <option value="114">Kuwait</option>
                                                <option value="115">Kyrgyzstan</option>
                                                <option value="116">Lao People's Democratic Republic</option>
                                                <option value="117">Latvia</option>
                                                <option value="118">Lebanon</option>
                                                <option value="119">Lesotho</option>
                                                <option value="120">Liberia</option>
                                                <option value="121">Libyan Arab Jamahiriya</option>
                                                <option value="122">Liechtenstein</option>
                                                <option value="123">Lithuania</option>
                                                <option value="124">Luxembourg</option>
                                                <option value="125">Macau</option>
                                                <option value="127">Madagascar</option>
                                                <option value="128">Malawi</option>
                                                <option value="129">Malaysia</option>
                                                <option value="130">Maldives</option>
                                                <option value="131">Mali</option>
                                                <option value="132">Malta</option>
                                                <option value="133">Marshall Islands</option>
                                                <option value="134">Martinique</option>
                                                <option value="135">Mauritania</option>
                                                <option value="136">Mauritius</option>
                                                <option value="137">Mayotte</option>
                                                <option value="138">Mexico</option>
                                                <option value="139">Micronesia, Federated States of</option>
                                                <option value="140">Moldova, Republic of</option>
                                                <option value="141">Monaco</option>
                                                <option value="142">Mongolia</option>
                                                <option value="242">Montenegro</option>
                                                <option value="143">Montserrat</option>
                                                <option value="144">Morocco</option>
                                                <option value="145">Mozambique</option>
                                                <option value="146">Myanmar</option>
                                                <option value="147">Namibia</option>
                                                <option value="148">Nauru</option>
                                                <option value="149">Nepal</option>
                                                <option value="150">Netherlands</option>
                                                <option value="151">Netherlands Antilles</option>
                                                <option value="152">New Caledonia</option>
                                                <option value="153">New Zealand</option>
                                                <option value="154">Nicaragua</option>
                                                <option value="155">Niger</option>
                                                <option value="156">Nigeria</option>
                                                <option value="157">Niue</option>
                                                <option value="158">Norfolk Island</option>
                                                <option value="112">North Korea</option>
                                                <option value="159">Northern Mariana Islands</option>
                                                <option value="160">Norway</option>
                                                <option value="161">Oman</option>
                                                <option value="162">Pakistan</option>
                                                <option value="163">Palau</option>
                                                <option value="247">Palestinian Territory, Occupied</option>
                                                <option value="164">Panama</option>
                                                <option value="165">Papua New Guinea</option>
                                                <option value="166">Paraguay</option>
                                                <option value="167">Peru</option>
                                                <option value="168">Philippines</option>
                                                <option value="169">Pitcairn</option>
                                                <option value="170">Poland</option>
                                                <option value="171">Portugal</option>
                                                <option value="172">Puerto Rico</option>
                                                <option value="173">Qatar</option>
                                                <option value="174">Reunion</option>
                                                <option value="175">Romania</option>
                                                <option value="176" selected="selected">Russian Federation</option>
                                                <option value="177">Rwanda</option>
                                                <option value="178">Saint Kitts and Nevis</option>
                                                <option value="179">Saint Lucia</option>
                                                <option value="180">Saint Vincent and the Grenadines</option>
                                                <option value="181">Samoa</option>
                                                <option value="182">San Marino</option>
                                                <option value="183">Sao Tome and Principe</option>
                                                <option value="184">Saudi Arabia</option>
                                                <option value="185">Senegal</option>
                                                <option value="243">Serbia</option>
                                                <option value="186">Seychelles</option>
                                                <option value="187">Sierra Leone</option>
                                                <option value="188">Singapore</option>
                                                <option value="189">Slovak Republic</option>
                                                <option value="190">Slovenia</option>
                                                <option value="191">Solomon Islands</option>
                                                <option value="192">Somalia</option>
                                                <option value="193">South Africa</option>
                                                <option value="194">South Georgia &amp; South Sandwich Islands</option>
                                                <option value="248">South Sudan</option>
                                                <option value="195">Spain</option>
                                                <option value="196">Sri Lanka</option>
                                                <option value="249">St. Barthelemy</option>
                                                <option value="197">St. Helena</option>
                                                <option value="250">St. Martin (French part)</option>
                                                <option value="198">St. Pierre and Miquelon</option>
                                                <option value="199">Sudan</option>
                                                <option value="200">Suriname</option>
                                                <option value="201">Svalbard and Jan Mayen Islands</option>
                                                <option value="202">Swaziland</option>
                                                <option value="203">Sweden</option>
                                                <option value="204">Switzerland</option>
                                                <option value="205">Syrian Arab Republic</option>
                                                <option value="206">Taiwan</option>
                                                <option value="207">Tajikistan</option>
                                                <option value="208">Tanzania, United Republic of</option>
                                                <option value="209">Thailand</option>
                                                <option value="210">Togo</option>
                                                <option value="211">Tokelau</option>
                                                <option value="212">Tonga</option>
                                                <option value="213">Trinidad and Tobago</option>
                                                <option value="255">Tristan da Cunha</option>
                                                <option value="214">Tunisia</option>
                                                <option value="215">Turkey</option>
                                                <option value="216">Turkmenistan</option>
                                                <option value="217">Turks and Caicos Islands</option>
                                                <option value="218">Tuvalu</option>
                                                <option value="219">Uganda</option>
                                                <option value="220">Ukraine</option>
                                                <option value="221">United Arab Emirates</option>
                                                <option value="222">United Kingdom</option>
                                                <option value="223">United States</option>
                                                <option value="224">United States Minor Outlying Islands</option>
                                                <option value="225">Uruguay</option>
                                                <option value="226">Uzbekistan</option>
                                                <option value="227">Vanuatu</option>
                                                <option value="228">Vatican City State (Holy See)</option>
                                                <option value="229">Venezuela</option>
                                                <option value="230">Viet Nam</option>
                                                <option value="231">Virgin Islands (British)</option>
                                                <option value="232">Virgin Islands (U.S.)</option>
                                                <option value="233">Wallis and Futuna Islands</option>
                                                <option value="234">Western Sahara</option>
                                                <option value="235">Yemen</option>
                                                <option value="238">Zambia</option>
                                                <option value="239">Zimbabwe</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-zone">Регион / Область</label>

                                        <div class="col-sm-10">
                                            <select name="zone_id" id="input-zone" class="form-control">
                                                <option value=""> --- Выберите ---</option>
                                                <option value="2721">Abakan</option>
                                                <option value="2722">Aginskoye</option>
                                                <option value="2723">Anadyr</option>
                                                <option value="2724">Arkahangelsk</option>
                                                <option value="2725">Astrakhan</option>
                                                <option value="2726">Barnaul</option>
                                                <option value="2727">Belgorod</option>
                                                <option value="2728">Birobidzhan</option>
                                                <option value="2729">Blagoveshchensk</option>
                                                <option value="2730">Bryansk</option>
                                                <option value="2731">Cheboksary</option>
                                                <option value="2732">Chelyabinsk</option>
                                                <option value="2733">Cherkessk</option>
                                                <option value="2734">Chita</option>
                                                <option value="2735">Dudinka</option>
                                                <option value="2736">Elista</option>
                                                <option value="2737">Gomo-Altaysk</option>
                                                <option value="2738">Gorno-Altaysk</option>
                                                <option value="2739">Groznyy</option>
                                                <option value="2740">Irkutsk</option>
                                                <option value="2741">Ivanovo</option>
                                                <option value="2742">Izhevsk</option>
                                                <option value="2743">Kalinigrad</option>
                                                <option value="2744">Kaluga</option>
                                                <option value="2745">Kasnodar</option>
                                                <option value="2746">Kazan</option>
                                                <option value="2747">Kemerovo</option>
                                                <option value="2748">Khabarovsk</option>
                                                <option value="2749">Khanty-Mansiysk</option>
                                                <option value="2750">Kostroma</option>
                                                <option value="2751">Krasnodar</option>
                                                <option value="2752">Krasnoyarsk</option>
                                                <option value="2753">Kudymkar</option>
                                                <option value="2754">Kurgan</option>
                                                <option value="2755">Kursk</option>
                                                <option value="2756">Kyzyl</option>
                                                <option value="2757">Lipetsk</option>
                                                <option value="2758">Magadan</option>
                                                <option value="2759">Makhachkala</option>
                                                <option value="2760">Maykop</option>
                                                <option value="2761">Moscow</option>
                                                <option value="2762">Murmansk</option>
                                                <option value="2763">Nalchik</option>
                                                <option value="2764">Naryan Mar</option>
                                                <option value="2765">Nazran</option>
                                                <option value="2766">Nizhniy Novgorod</option>
                                                <option value="2767">Novgorod</option>
                                                <option value="2768">Novosibirsk</option>
                                                <option value="2769">Omsk</option>
                                                <option value="2770">Orel</option>
                                                <option value="2771">Orenburg</option>
                                                <option value="2772">Palana</option>
                                                <option value="2773">Penza</option>
                                                <option value="2774">Perm</option>
                                                <option value="2775">Petropavlovsk-Kamchatskiy</option>
                                                <option value="2776">Petrozavodsk</option>
                                                <option value="2777">Pskov</option>
                                                <option value="2778">Rostov-na-Donu</option>
                                                <option value="2779">Ryazan</option>
                                                <option value="2780">Salekhard</option>
                                                <option value="2781">Samara</option>
                                                <option value="2782">Saransk</option>
                                                <option value="2783">Saratov</option>
                                                <option value="2784">Smolensk</option>
                                                <option value="2785">St. Petersburg</option>
                                                <option value="2786">Stavropol</option>
                                                <option value="2787">Syktyvkar</option>
                                                <option value="2788">Tambov</option>
                                                <option value="2789">Tomsk</option>
                                                <option value="2790">Tula</option>
                                                <option value="2791">Tura</option>
                                                <option value="2792">Tver</option>
                                                <option value="2793">Tyumen</option>
                                                <option value="2794">Ufa</option>
                                                <option value="2795">Ul'yanovsk</option>
                                                <option value="2796">Ulan-Ude</option>
                                                <option value="2797">Ust'-Ordynskiy</option>
                                                <option value="2798">Vladikavkaz</option>
                                                <option value="2799">Vladimir</option>
                                                <option value="2800">Vladivostok</option>
                                                <option value="2801">Volgograd</option>
                                                <option value="2802">Vologda</option>
                                                <option value="2803">Voronezh</option>
                                                <option value="2804">Vyatka</option>
                                                <option value="2805">Yakutsk</option>
                                                <option value="2806">Yaroslavl</option>
                                                <option value="2807">Yekaterinburg</option>
                                                <option value="2808">Yoshkar-Ola</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-postcode">Индекс</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="postcode" value="" placeholder="Индекс"
                                                   id="input-postcode"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <input type="button" value="Узнать цены" id="button-quote"
                                           data-loading-text="Загрузка..."
                                           class="btn btn-primary">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td class="text-right"><strong>Сумма:</strong></td>
                                <td class="text-right">100.00руб.</td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Итого:</strong></td>
                                <td class="text-right">100.00руб.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="buttons">
                    <div class="pull-left"><a href="http://test15.ru/index.php?route=common/home"
                                              class="btn btn-default">Продолжить
                            покупки</a></div>
                    <div class="pull-right"><a href="http://test15.ru/index.php?route=checkout/checkout"
                                               class="btn btn-primary">Оформление заказа</a></div>
                </div>
            </div>
        </div>
    </div>

@show