@extends('base')

@section('head')
    <meta name="robots" content="noindex"/>
@endsection


@section('breadcrumb')

@endsection



@section('content')
    <noindex>
        <div class="container">
            <div class="row">
                <div class="col-sm-9" style="background-color: #fff; padding-bottom: 20px">
                    <div class="row">
                        <h3>Корзина</h3>



                        @if(count($cart['products']))
                            @include('cart.components.products-grid', ['cart'=>$cart])
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-8">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td class="text-right"><strong>Сумма:</strong></td>
                                            <td class="text-right">
                                                <span id="cart-summ">{{ number_format($summ, 0, "," , " " )}}</span>
                                                р
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <form id="form-order" action="{{ url('ajax/cart/new_order') }}" role="form" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="callback" value="0">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Выберите способ оплаты</div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-6 payment">
                                                        <input type="radio" name="billing" value="1" checked="checked"
                                                               data-discount="0">
                                                        <img src="{{ url('icons/payment_post.png') }}" alt="">

                                                        <div>Оплата на почте наложенным платежом</div>
                                                    </div>
                                                    <div class="col-md-6 payment">
                                                        <input type="radio" name="billing" value="2" data-discount="1">
                                                        <img src="{{ url('icons/payment_webmoney.png') }}" alt="">

                                                        <div>Webmoney <b class="text-success">( СКИДКА 5% )</b></div>
                                                    </div>
                                                    <div class="col-md-6 payment">
                                                        <input type="radio" name="billing" value="3" data-discount="1">
                                                        <img src="{{ url('icons/payment_yad.png') }}" alt="">

                                                        <div>Яндекс.Деньги <b class="text-success">( СКИДКА 5% )</b>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 payment" data-discount="1">
                                                        <input type="radio" name="billing" value="17" data-discount="1">
                                                        <img src="{{ url('icons/payment_euroset.png') }}" alt="">

                                                        <div>Евросеть <b class="text-success">( СКИДКА 5% )</b></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="fio" class="control-label">ФИО</label>
                                            <input name="fio" type="text" class="form-control" id="name"
                                                   placeholder="Иванов Иван Иванович"
                                                   required="">
                                    <span class="glyphicon form-control-feedback glyphicon-remove"
                                          aria-hidden="true"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="control-label">E-mail</label>
                                            <input name="email" type="email" class="form-control" id="name"
                                                   placeholder="my@mail.ru (не обязательно)">
                                    <span class="glyphicon form-control-feedback glyphicon-remove"
                                          aria-hidden="true"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="tel" class="control-label">Телефон</label>

                                            <div class="input-group">
                                                <span class="input-group-addon">+7</span>
                                                <input name="tel" type="number" max="9999999999" min="1000000000"
                                                       class="form-control" id="phone" placeholder="9121231212"
                                                       required=""><!-- onkeyup="key_phone(this, event, 1);"-->
                                            </div>
                                    <span class="glyphicon form-control-feedback glyphicon-remove"
                                          aria-hidden="true"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="city" class="control-label">Город</label>
                                            <input name="city" type="text" class="form-control" id="name"
                                                   placeholder="Москва">
                                    <span class="glyphicon form-control-feedback glyphicon-remove"
                                          aria-hidden="true"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="address" class="control-label">Адрес</label>
                                            <a href="http://indexp.ru/" target="_blank" class="f-right indexx"
                                               rel="nofollow">Узнать свой индекс</a>

                                            <input name="address" type="text" class="form-control" min="5" max="10"
                                                   id="name"
                                                   placeholder="100000, г. Москва, ул. Ленина, д. 2">
                                    <span class="glyphicon form-control-feedback glyphicon-remove"
                                          aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row" style="margin-top: 20px">
                                    <div class="col-md-12">

                                        <input type="submit" class="btn btn-success" value="Отправить заказ">
                                    </div>
                                </div>
                            </form>
                        @else
                            В корзине нет товаров
                        @endif
                    </div>

                </div>
                <div class="col-sm-3" id="cart-left-info">

                    <div class="panel panel-default">
                        <div class="panel-heading h3">
                            <img src="{{ url('icons/inspiration.svg') }}" alt="Бонус 5 таблеток виагры"
                                 style="width: 40px">
                            Бонус
                        </div>
                        <div class="panel-body">
                            При покупке 20 или более штук таблеток предоставляется подарок, <b class="text-success">5
                                ТАБЛЕТОК ВИАГРЫ БЕСПЛАТНО</b>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading h3">
                            <img src="{{ url('icons/delivery13.svg') }}" alt="Доставка по всей России"
                                 style="width: 40px">
                            Доставка
                        </div>
                        <div class="panel-body">
                            Доставка осуществляется <b class="text-danger">ТОЛЬКО</b> почтой 1-ого класса.
                            Стоимость <b class="text-info">300 РУБ</b>. При покупке 50 или более штук таблеток, доставка
                            осуществляется <b class="text-success">БЕСПЛАТНО</b>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading h3">
                            <img src="{{ url('icons/credit-card3.svg') }}" alt="Оплата" style="width: 40px">
                            Оплата
                        </div>
                        <div class="panel-body">
                            При оплате онлайн с помошью: <b class="text-info">Банковской карты, WebMoney, Яндекс.Деньги,
                                Сбербанк Онлайн, Евросеть</b> предоставляется <b class="text-success">СКИДКА 5%</b>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div id="modal-error" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Проверьте, пожалуйста, поля</h4>
                    </div>
                    <div class="modal-body">
                        <p id="modal-error-text"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </noindex>


@endsection

@section('js')
    @parent
    <script>
        $(document).ready(function () {


        });


    </script>



@endsection