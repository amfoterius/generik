@extends('base')

@section('head')
    <meta name="robots" content="noindex"/>
@endsection

@section('content')
    <noindex>
        <div class="container">
            <div class="row" style="background: #fff">
                <div class="col-md-12">
                    <div class="media" style="background: #fff; padding: 100px;">
                        <div class="pull-left">
                            <i class="glyphicon glyphicon-ok-sign" style="font-size: 90px; color: #26AD38;"></i>
                        </div>
                        @if($callback)
                            <div class="media-body">
                                <div class="h4 media-heading">Заявка на обратный звонок отправленна, в ближайшее время с Вами свяжутся</div>

                                <a href="{{ url('/') }}" class="btn btn-primary">Вернуться к покупкам</a>
                            </div>

                        @else
                            <div class="media-body">
                                <div class="h4 media-heading">Поздравляем, Вы удачно совершили покупку, в ближайшее время наш менеджер с Вами свяжется</div>
                                <div>
                                    В качестве оплаты Вы выбрали
                                    <code>{{ $curr_billing->name }}</code>, номер заказа <code>{{ $curr_order->api_id }}</code>, счет для оплаты <code>{{ $curr_billing->rekvizity }}</code>
                                </div>
                                <a href="{{ url('/') }}" class="btn btn-primary">Вернуться к покупкам</a>
                            </div>

                        @endif

                        @if($curr_site->id == 11)

                        <!-- Facebook Pixel Code -->
                            <script>
                                !function(f,b,e,v,n,t,s)
                                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                                    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                                    n.queue=[];t=b.createElement(e);t.async=!0;
                                    t.src=v;s=b.getElementsByTagName(e)[0];
                                    s.parentNode.insertBefore(t,s)}(window, document,'script',
                                    'https://connect.facebook.net/en_US/fbevents.js');
                                fbq('init', '335902193599693');
                                fbq('track', 'PageView');
                            </script>
                            <noscript><img height="1" width="1" style="display:none"
                                           src="https://www.facebook.com/tr?id=335902193599693&ev=PageView&noscript=1"
                                /></noscript>
                            <!-- End Facebook Pixel Code -->
                            <script>
                                fbq('track', 'Purchase');
                            </script>



                        @endif

                    </div>
                </div>

            </div>

        </div>
    </noindex>



@endsection