@extends('base')
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('cart') }}">Корзина</a>
        </li>
        <li class="active">
            Ввод данных
        </li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12" style="background-color: #fff">


                <div class="row">
                    <section>
                        <div class="wizard">
                            <div class="wizard-inner">
                                <div class="connecting-line"></div>
                                <ul class="nav nav-tabs" role="tablist">

                                    <li role="presentation">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Корзина">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-shopping-cart"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li role="presentation" class="active">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Контактные данные">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li role="presentation" class="disabled">
                                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Способ доставки и оплаты">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li role="presentation" class="disabled">
                                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Завершение">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-ok"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>
                </div>



                <h1>Введите контактные данные</h1>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form id="form-order" method="post" action="{{ route('cart.step3') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input id="input-order-text" type="hidden" name="products">

                    <div class="form-group">
                        <label for="name" class="control-label">ФИО</label>
                        <input name="fio" type="text" class="form-control" id="name" placeholder="Иванов Иван Иванович"
                               required="">
                        <span class="glyphicon form-control-feedback glyphicon-remove" aria-hidden="true"></span>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label">E-mail</label>
                        <input name="email" type="email" class="form-control" id="name" placeholder="my@mail.ru"
                               required="">
                        <span class="glyphicon form-control-feedback glyphicon-remove" aria-hidden="true"></span>
                    </div>
                    <div class="form-group">
                        <label for="tel" class="control-label">Телефон</label>

                        <div class="input-group">
                            <span class="input-group-addon">+7</span>
                            <input name="tel" type="text" class="form-control" id="phone" placeholder="9121231212"
                                   required=""><!-- onkeyup="key_phone(this, event, 1);"-->
                        </div>
                        <span class="glyphicon form-control-feedback glyphicon-remove" aria-hidden="true"></span>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label">Город</label>
                        <input name="city" type="text" class="form-control" id="name" placeholder="Москва"
                               required="">
                        <span class="glyphicon form-control-feedback glyphicon-remove" aria-hidden="true"></span>
                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label">Адрес</label>
                        <input name="address" type="text" class="form-control" id="name" placeholder="100000, г. Москва, ул. Ленина, д. 2"
                               required="">
                        <span class="glyphicon form-control-feedback glyphicon-remove" aria-hidden="true"></span>
                    </div>

                    @include('cart.components.products-grid', ['cart'=>$cart])

                    <div class="buttons" style="padding-bottom: 50px">
                        <div class="pull-left">
                            <button type="button" class="btn btn-labeled btn-default">
                                <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>В корзину</button>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-labeled btn-success">
                                <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span>
                                Доставка и оплата</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
@endsection



@section('scripts')
    @parent
    <script>
        $(document).ready(function () {

            $("#form-order").on('submit', function (e) {

                e.preventDefault();
                //$("#input-order-text").val($("#table-cart-products").html());
                console.log($("#table-cart-products").html());
                return false;
            });

        })


    </script>


@endsection