@extends('base')

@section('head')
    <meta name="robots" content="noindex"/>
@endsection

@section('content')
    <noindex>
        <div class="container">
            <div class="row" style="background: #fff">
                <div class="col-md-12">
                    <div class="media" style="background: #fff; padding: 100px;">
                        <div class="pull-left">
                            <i class="glyphicon glyphicon-remove-sign" style="font-size: 90px; color: red;"></i>
                        </div>
                        <div class="media-body">
                            <div class="h4 media-heading">Ошибка покупки</div>
                            <div>
                                Заказы можно отправлять не чаще 1 раза в 5 минут
                            </div>
                            <a href="{{ url('/') }}" class="btn btn-primary">Вернуться к покупкам</a>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </noindex>



@endsection