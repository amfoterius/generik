<script>
    var summ = {{ $summ }};
    var summDiscount = {{ $summ_discount }};
</script>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <td class="text-center">Изображение</td>
            <td class="text-left">Название</td>
            <td class="text-left">Количество</td>
            <td class="text-right">Цена за шт.</td>
            <td class="text-right">Всего</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        @foreach($cart['products'] as $product)

            <tr>
                <td class="text-center">
                    <a href="{{ $product->category->getUrl() }}">
                        <img style="width: 70px; height: 70px"
                             src="[domain]{{ $product->getImageUrl('image') }}" alt="" title=""
                             class="img-thumbnail">
                    </a>
                </td>
                <td class="text-left">
                    <a href="{{ $product->category->getUrl() }}">{{ $product->name }}</a>, {{ $product->kolvo }}
                </td>
                <td class="text-left">
                    <div class="input-group btn-block" style="max-width: 200px;">
                        {{ $product->count }}
                    </div>
                </td>
                <td class="text-right">{{ number_format($product->price, 0, "," , " " ) }} р</td>
                <td class="text-right">
                    <span class="summ">{{ number_format($product->price * $product->count, 0, "," , " " ) }}</span>
                    р
                </td>
                <td>
                    <button type="button" class="btn btn-labeled btn-danger remove-on-cart"
                            data-product-id="{{ $product->id }}">
                        <span class="btn-label"><i class="glyphicon glyphicon-trash"></i></span>Удалить
                    </button>
                </td>
            </tr>
        @endforeach

        <tr id="row-discount" class="alert alert-success hide">
            <td class="text-center">
            </td>
            <td class="text-left">
                Скидка <code style="font-size: 18px">5%</code>
            </td>
            <td class="text-left">

            </td>
            <td class="text-right"></td>
            <td class="text-right">
                <span id="summ-discount">-{{ $summ_discount }}</span>
                руб.
            </td>
            <td>

            </td>
        </tr>

        @if($summ_kolvo >= 20)
            <tr class="alert alert-info">
                <td class="text-center">
                    <a target="_blank" href="{{ $viagra_category->getUrl() }}">
                        <img style="width: 70px; height: 70px"
                             src="[domain]{{ $viagra_category->products()->first()->getImageUrl('image') }}" alt="" title=""
                             class="img-thumbnail">
                    </a>
                </td>
                <td class="text-left">
                    <a target="_blank" href="{{ $viagra_category->getUrl() }}">Дженерик Виагра 100 мг.</a>, 2 таблеток
                    <span class="label label-success">ПОДАРОК</span>
                </td>
                <td class="text-left">
                    <div class="input-group btn-block" style="max-width: 200px;">
                        1
                    </div>
                </td>
                <td class="text-right">0 р</td>
                <td class="text-right">
                    <span class="summ">0</span>
                    р
                </td>
                <td>

                </td>
            </tr>
        @endif


        <tr class="{{ $summ_kolvo >= 50 ? 'alert alert-info' : ''}}">
            <td class="text-center">
            </td>
            <td class="text-left">
                Доставка
            </td>
            <td class="text-left">
                <div class="input-group btn-block" style="max-width: 200px;">
                    1
                </div>
            </td>
            <td class="text-right">{{ $summ_kolvo >= 50 ? '0' : '300'}} р</td>
            <td class="text-right">
                <span class="summ">{{ $summ_kolvo >= 50 ? '0' : '300'}}</span>
                р
            </td>
            <td>

            </td>
        </tr>



        </tbody>
    </table>
</div>