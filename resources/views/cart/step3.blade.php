@extends('base')
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('cart') }}">Корзина</a>
        </li>
        <li class="active">
            Ввод данных
        </li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12" style="background-color: #fff">
                
                <div class="row">
                    <section>
                        <div class="wizard">
                            <div class="wizard-inner">
                                <div class="connecting-line"></div>
                                <ul class="nav nav-tabs" role="tablist">

                                    <li role="presentation">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Корзина">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-shopping-cart"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li role="presentation">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Контактные данные">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li role="presentation" class="active">
                                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Способ доставки и оплаты">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li role="presentation" class="disabled">
                                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Завершение">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-ok"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <form role="form">
                                <div class="tab-content">
                                    <div class="tab-pane active" role="tabpanel" id="step1">
                                        <h3>Step 1</h3>
                                        <p>This is step 1</p>
                                        <ul class="list-inline pull-right">
                                            <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="step2">
                                        <h3>Step 2</h3>
                                        <p>This is step 2</p>
                                        <ul class="list-inline pull-right">
                                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                            <li><button type="button" class="btn btn-primary next-step" style="display: none">Save and continue</button></li>
                                            <li><button id="button-add-order" type="button" class="btn btn-primary">Отправить заказ</button></li></li>

                                        </ul>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="step3">
                                        <h3>Step 3</h3>
                                        <p>This is step 3</p>
                                        <ul class="list-inline pull-right">
                                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                            <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                                            <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="complete">
                                        <h3>Complete</h3>
                                        <p>You have successfully completed all steps.</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>



                <h1>Выберите способ доставки и оплаты</h1>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="row">

                    <div class="col-md-6">
                        <h4>Доставка</h4>

                        <div class="funkyradio">
                            @foreach($delivery_types as $k=>$type)
                                <div class="funkyradio-info radio-delivery">
                                    <input type="radio" name="delivery" id="radioDev{{ $type->id }}"
                                           value="{{ $type->api_id }}">
                                    <label for="radioDev{{ $type->id }}">{{ $type->name }}</label>

                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h4>Оплата</h4>

                        <div class="funkyradio">
                            @foreach($billing_types as $k=>$type)
                                <div class="funkyradio-success radio-billing"
                                     data-delivery-id="{{ $type->delivery_id }}" style="display: none">
                                    <input type="radio" name="billing" id="radioBill{{ $type->id }}"
                                           value="{{ $type->api_id }}">
                                    <label for="radioBill{{ $type->id }}">{{ $type->name }}</label>

                                    @if($type->discount)
                                        <div class="alert alert-success">
                                            Скидка <strong>{{ $type->discount }} %</strong>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>


                </div>

                <form id="form-order" method="post" action="{{ url('корзина/новый-заказ') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input id="input-order-text" type="hidden" name="products">


                    @include('cart.components.products-grid', ['cart'=>$cart])

                    <div class="buttons" style="padding-bottom: 50px">
                        <div class="pull-left">
                            <button type="button" class="btn btn-labeled btn-default">
                                <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>В корзину
                            </button>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-labeled btn-success">
                                <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>Отправить заказ
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>

@endsection



@section('js')
    @parent
    <script>
        $(document).ready(function () {

            $("#form-order").on('submit', function (e) {

                e.preventDefault();
                //$("#input-order-text").val($("#table-cart-products").html());
                console.log($("#table-cart-products").html());
                return false;
            });


            $('input[type="radio"][name="delivery"]').change(function () {

                var i = true;
                var currDeliveryId = $('input:radio[name=delivery]:checked').val();

                $('.radio-billing').each(function (index) {

                    if ($(this).data('delivery-id') == currDeliveryId) {
                        $(this).show();
                        if(i) {
                            $(this).find('input').prop("checked", true);
                            i = false;
                        }

                    }
                    else {
                        $(this).hide();
                    }


                })

            });




        });


    </script>



@endsection