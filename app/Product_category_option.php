<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_option extends Model
{
    public function categories_m2m()
    {
        return $this->hasMany('App\Product_category_option_m2m', 'option_id');
    }


}
