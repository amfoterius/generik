<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_group extends Model
{
    public function categories_list_m2m()
    {
        return $this->hasMany('App\Product_category_group_list_m2m', 'group_id');

    }

    public function categories_m2m()
    {
        return $this->hasMany('App\Product_category_group_m2m', 'group_id');

    }




}
