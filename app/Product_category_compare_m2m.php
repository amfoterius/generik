<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_compare_m2m extends Model
{
    protected $table = 'product_category_compare_m2m';

    public function base_category()
    {
        return $this->belongsTo('App\Product_category', 'base_category_id');
    }

    public function compare_category()
    {
        return $this->belongsTo('App\Product_category', 'compare_category_id');
    }




}
