<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 18.02.2017
 * Time: 23:21
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;
use Illuminate\Http\Request;
use DB;

use Intervention\Image;


class Base_model extends Model
{

    protected $cache_time = 1000;

    public $is_init = false;

    const file_path = '';
    const image_sizes = [];

    public function beforeCreate($model) {

    }

    public function afterCreate($model) {

    }

    public function getImageUrl($param) {

        $path = 'files/images/'.$this::file_path.'/'.$param.'/'.$this[$param];

        return $path;


    }

    public function getImageUrlResize($param, $size='medium') {

        $path = 'files/images/'.$this::file_path.'/'.$param.'/'.$size.'/'.config('api.curr_site_id').'-'.$this[$param];

        return $path;


    }

    protected function getParam($param, $callback = '', $callback_params = []) {

        if($callback == '') {
            return $this[$param];

        } else {

            $result = call_user_func([$this, $callback]);
            return $result;
        }

    }

    public function scopeFilterRequest($query, Request $request)
    {

        foreach($request->input('filters', []) as $filter) {

            $query = $query->where($filter[0], $filter[1], $filter[2]);

        }


        return $query;

    }

    /**
     * query scope nPerGroup
     *
     * @return void
     */
    public function scopeNPerGroup($query, $group, $n = 10)
    {
        // queried table
        $table = ($this->getTable());

        // initialize MySQL variables inline
        $query->from( DB::raw("(SELECT @rank:=0, @group:=0) as vars, {$table}") );

        // if no columns already selected, let's select *
        if ( ! $query->getQuery()->columns)
        {
            $query->select("{$table}.*");
        }

        // make sure column aliases are unique
        $groupAlias = 'group_'.md5(time());
        $rankAlias  = 'rank_'.md5(time());

        // apply mysql variables
        $query->addSelect(DB::raw(
            "@rank := IF(@group = {$group}, @rank+1, 1) as {$rankAlias}, @group := {$group} as {$groupAlias}"
        ));

        // make sure first order clause is the group order
        $query->getQuery()->orders = (array) $query->getQuery()->orders;
        array_unshift($query->getQuery()->orders, ['column' => $group, 'direction' => 'asc']);

        // prepare subquery
        $subQuery = $query->toSql();

        // prepare new main base Query\Builder
        $newBase = $this->newQuery()
            ->from(DB::raw("({$subQuery}) as {$table}"))
            ->mergeBindings($query->getQuery())
            ->where($rankAlias, '<=', $n)
            ->getQuery();

        // replace underlying builder to get rid of previous clauses
        $query->setQuery($newBase);
    }

}