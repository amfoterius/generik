<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_page_review extends Model
{
    //

    public function category_page_m2m()
    {
        return $this->belongsTo('App\Product_category_page_m2m', 'category_page_id');
    }

}
