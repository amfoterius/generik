<?php

namespace App\Site;

use Illuminate\Database\Eloquent\Model;

class Site_category_page_m2m extends Model
{
    protected $table = 'site_category_page_m2m';

    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');
    }

    public function page()
    {
        return $this->belongsTo('App\Product_category_page', 'page_id');
    }


}
