<?php

namespace App\Site;

use Illuminate\Database\Eloquent\Model;

class Site_option_m2m extends Model
{
    protected $table = 'site_option_m2m';

    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');
    }


    public function option()
    {
        return $this->belongsTo('App\Site\Site_option', 'option_id');
    }


}
