<?php

namespace App\Site;

use Illuminate\Database\Eloquent\Model;

class Site_banner extends Model
{
    //
    public function sites_m2m()
    {
        return $this->hasMany('App\Site\Site_banner_m2m', 'banner_id');
    }
}
