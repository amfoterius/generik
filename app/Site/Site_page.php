<?php

namespace App\Site;

use Illuminate\Database\Eloquent\Model;

class Site_page extends Model
{
    function sites_m2m() {

        return $this->hasMany('App\Site\Site_page_m2m', 'site_id');

    }

    public function afterCreate($page) {

        foreach(Site::select('id')->get()->pluck('id') as $site_id) {

            if(is_null($this->sites_m2m()->where('site_id', $site_id)->first())) {

                $model = new \App\Site\Site_page_m2m();
                $model->site_id = $site_id;
                $model->page_id = $this->id;
                $model->save();

            }
        }

    }
}
