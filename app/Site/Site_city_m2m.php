<?php

namespace App\Site;

use Illuminate\Database\Eloquent\Model;

class Site_city_m2m extends Model
{

    protected $table = 'site_city_m2m';

    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }
}
