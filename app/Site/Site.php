<?php

namespace App\Site;

use Illuminate\Database\Eloquent\Model;

class Site extends \App\Base_model
{
    public function pages_m2m() {

        return $this->hasMany('App\Site\Site_page_m2m', 'site_id');

    }

    public function category_pages_m2m()
    {
        return $this->hasMany('App\Site\Site_category_page_m2m');
    }

    public function categories_m2m() {

        return $this->hasMany('App\Product_category_site_m2m', 'site_id');

    }

    public function types_m2m()
    {
        return $this->hasMany('App\Site\Site_category_type_m2m', 'site_id');
            
    }

    public function options_m2m()
    {
        return $this->hasMany('App\Site\Site_option_m2m', 'site_id');
    }

    public function banner_m2m()
    {
        return $this->hasMany('App\Site\Site_banner_m2m', 'site_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review', 'site_id');
    }

    public function afterCreate($site)
    {

        foreach(Site_page::select('id')->get()->pluck('id') as $page_id) {

            if(is_null($this->pages_m2m()->where('page_id', $page_id)->first())) {

                $model = new \App\Site\Site_page_m2m();
                $model->site_id = $this->id;
                $model->page_id = $page_id;
                $model->save();

            }
        }

        foreach(\App\Product_category_page::select('id')->get()->pluck('id') as $page_id) {

            if(is_null($this->category_pages_m2m()->where('page_id', $page_id)->first())) {

                $model = new \App\Product_category_page_m2m();
                $model->site_id = $this->id;
                $model->page_id = $page_id;
                $model->save();

            }
        }


    }

    public function seo_product_category_page_m2ms()
    {
        return $this->hasMany('App\Seo\Product_category_page_m2m', 'site_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'site_id');
    }


    public function getBanner($banner_name)
    {

        $banner_id = Site_banner::where('name', $banner_name)->first()->id;


        if(!is_null($banner = Site_banner_m2m::where('banner_id', $banner_id)->where('site_id', 1)->first()))
        {

            return $banner;

        }
        else
        {
            return null;
        }


    }

    public function getMetaForPage($page_name="index")
    {
        // TODO:
        $pages_m2m = $this->pages_m2m;

        foreach($pages_m2m as $page_m2m)
        {

            if($page_m2m->page->n == $page_name)
            {
                return $page_m2m;
            }
        }

    }
}
