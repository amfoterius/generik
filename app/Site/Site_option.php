<?php

namespace App\Site;

use Illuminate\Database\Eloquent\Model;

class Site_option extends Model
{
    //

    public function sites_m2m()
    {
        return $this->hasMany('App\Site\Site_option_m2m', 'option_id');
    }
}
