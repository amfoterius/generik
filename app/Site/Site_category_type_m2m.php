<?php

namespace App\Site;

use Illuminate\Database\Eloquent\Model;

class Site_category_type_m2m extends Model
{

    protected $table = 'site_category_type_m2m';


    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Product_category_type', 'type_id');
    }
}
