<?php

namespace App\Site;

use App\Base_model;
use Illuminate\Database\Eloquent\Model;

class Site_banner_m2m extends Base_model
{
    protected $table = 'site_banner_m2m';

    public static $path_images = '/uploads/sites/banners/';
    public static $path_images_m = '/uploads/sites/banners-m/';

    const file_path = 'site_banner_m2m';

    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');
    }


    public function banner()
    {
        return $this->belongsTo('App\Site\Site_banner', 'banner_id');
    }


}
