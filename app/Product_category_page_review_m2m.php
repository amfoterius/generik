<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_page_review_m2m extends Model
{
    protected $table = 'product_category_option_m2m';

    public function review()
    {
        return $this->belongsTo('App\Product_category_page_review', 'review_id');
    }



}
