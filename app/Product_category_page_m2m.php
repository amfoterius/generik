<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_page_m2m extends Model
{
    protected $table = 'product_category_page_m2m';

    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Product_category', 'category_id');
    }

    public function page()
    {
        return $this->belongsTo('App\Product_category_page', 'page_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Product_category_page_review', 'category_page_id');
    }

    public function seo()
    {
        return $this->hasMany('App\Seo\Product_category_page_m2m', 'page_m2m_id');

    }

    public function save(array $options = [])
    {
        parent::save($options);


        // Создаем записи СЕО для каждого домена
        foreach(\App\Site\Site::all() as $site) {
            $seo = new \App\Seo\Product_category_page_m2m();
            $seo->page_m2m_id = $this->id;
            $seo->site_id = $site->id;
            $seo->save();

        }

    }


    public function getUrl()
    {
        //TODO: need routers
        if(config('api.none_city'))
        {


            return
                config('api.domain') .
                $this->category->type->alias . '/'
                . $this->category->alias . "-купить/"
                . $this->category->alias . "_" . $this->page->alias;

        }
        else
        {


            return
                config('api.domain') .
                $this->category->type->alias . '-в-' . '[curr_city.name_p_lw]/'
                . $this->category->alias . '-купить-в-[curr_city.name_p_lw]/'
                . $this->category->alias . "_" . $this->page->alias . '-в-' . '[curr_city.name_p_lw]';

        }
    }

    public function getUrlAttribute()
    {
        return $this->getUrl();
    }




}
