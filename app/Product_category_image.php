<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_image extends \App\Base_model
{
    const file_path = 'product_category_image';
    const image_sizes = [

        'image' => [

            'large' => [1024, 768],
            'medium' => [200, 160],
            'small' => [50, 40],

        ],


    ];


    public function category()
    {
        return $this->belongsTo('App\Product_category', 'category_id');
    }
}
