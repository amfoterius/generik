<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;

    public function site_m2m()
    {
        return $this->hasMany('App\Site\Site_city_m2m', 'city_id');
    }
}
