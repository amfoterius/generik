<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_option_m2m extends Model
{
    protected $table = 'product_category_option_m2m';


    public function category()
    {
        return $this->belongsTo('App\Product_category', 'category_id');
    }

    public function option()
    {
        return $this->belongsTo('App\Product_category_option', 'option_id');
    }


}
