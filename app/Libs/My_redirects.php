<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 04.08.2016
 * Time: 17:34
 */

namespace App\Libs;


class My_redirects
{

    public static function category_redirect($curr_site, $category_alias)
    {
        $category_id = 0;

        switch ($category_alias) {

            case 'сиалис-20':
                $category_id = 140;
                break;

            case 'левитра-20':
                $category_id = 144;
                break;

            case 'левитра-40':
                $category_id = 145;
                break;

        }

        if ($category_id === 0) {
            return false;
        }

        $categoty = \App\Product_category::find($category_id);


        if (config('api.none_city')) {
            $url_redirect = route('category_none_city', [
                $curr_site->url,
                $categoty->type->alias,
                $categoty->alias,

            ]);
        } else {
            $alias_city = explode('.', $_SERVER['HTTP_HOST'])[0];
            $city = \App\City::where('alias', $alias_city)->first();

            $url_redirect = route('category', [
                $city->alias,
                $curr_site->url,
                $categoty->type->alias,
                $city->name_p_alias,
                $categoty->alias,
                $city->name_p_alias,
            ]);
        }

        return $url_redirect;


    }


}