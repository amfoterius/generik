<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery_type extends Model
{
    public function billings()
    {
        return $this->hasMany('App\Billing_type');
    }
}
