<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;

class Product_category_site_m2m extends \App\Base_model
{

    const file_path = 'product_category_site_m2m';
    const image_sizes = [

        'banner_image' => [
            'medium' => [800, 120],
        ],

        'video_image' => [
            'medium' => [1024, 768],
        ]

    ];

    protected $casts = [
        'text_blocks' => 'array',
        'navigate_text_blocks' => 'array',
    ];

    protected $table = 'product_category_site_m2m';

    public function category()
    {
        return $this->belongsTo('App\Product_category', 'category_id');
    }

    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');
    }

    public function save(array $options = [])
    {
        $this->text_blocks = [];
        $this->navigate_text_blocks = [];
        $arr = explode('<h2>', $this->text);
        $text_blocks = [];
        $i = 0;

        foreach($arr as $row) {

            if($i === 0) {
                $i++;
                continue;
            }
            $text_blocks[] = '<h2>' . $row;
            $i++;

        }

        $this->text_blocks = $text_blocks;

        $nav = [];
        $k = 0;
        foreach($this->text_blocks as $tb) {

            $matches = [];
            preg_match_all('/<h2>([^<]+)<\/h2>/i', $tb, $matches);

            if(count($matches[1]) == 0) {
                dd($this);
            }

            $nav[$k]['title'] = trim($matches[1][0]);
            $nav[$k]['anchor'] = str_replace(' ', '-', trim(mb_strtolower($matches[1][0])));

            $k++;

        }
        $this->navigate_text_blocks = $nav;

        return parent::save($options);
    }
}
