<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Blade функции



        // END Blade функции

        // Model events

        \App\Product_category_image::creating(function($image) {

            \Log::info('Image event Creating for id: '.$image->id);
            for($i = 3; $i <= 12; $i++) {
                try {

                    config(['api.curr_site_id' => $i]);

                    file_get_contents(url($image->getImageUrlResize('image')));
                    file_get_contents(url($image->getImageUrlResize('image', 'large')));
                    file_get_contents(url($image->getImageUrlResize('image', 'small')));

                } catch(Exception $e) {
                    continue;
                }

            }
        });

        \App\Product_category_image::updated(function($image) {

            \Log::info('Image event Updated for id: '.$image->id);
            for($i = 3; $i <= 12; $i++) {
                try {

                    config(['api.curr_site_id' => $i]);

                    file_get_contents(url($image->getImageUrlResize('image')));
                    file_get_contents(url($image->getImageUrlResize('image', 'large')));
                    file_get_contents(url($image->getImageUrlResize('image', 'small')));

                } catch(Exception $e) {
                    continue;
                }

            }
        });


        /*
        \App\Site\Site::created(function($model) {
            $model->afterCreate($model);
        });

        \App\Site\Site_page::created(function($model) {
            $model->afterCreate($model);
        });
        */


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
