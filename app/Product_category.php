<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\Review_option;
use DB;
use Cache;


class Product_category extends \App\Base_model
{
    protected $table = 'product_categories';

    const file_path = 'product_category';
    const image_sizes = [

        'img_big' => [

            'large' => [1024, 768],
            'medium' => [200, 160],
            'small' => [50, 40],

        ],

        'img_small' => [
            'medium' => [50, 40],
            'small' => [50, 40],
        ]

    ];

    // products

    public function products()
    {
        return $this->hasMany('App\Product', 'category_id');
    }

    public function get_min_price_item()
    {
        return $this->products->sortBy('price')->first();
    }

    public function get_max_price_item()
    {
        return $this->products->sortByDesc('price')->first();
    }

    public function get_req_for_items()
    {
        return $this->products()->select(
            DB::raw('max(price_per_item) as max_price_per_item,
             min(price_per_item) as min_price_per_item,
              max(kolvo_int) as max_kolvo,
               count(*) as c, min(price) as min_price'))->first();

    }

    // END products

    public function type()
    {
        return $this->belongsTo('App\Product_category_type', 'type_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Product_category', 'parent_id');
    }

    public function childrens()
    {
        return $this->hasMany('App\Product_category', 'parent_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review', 'product_id');
    }

    public function get_rating()
    {
        return $this->avg_rating();
    }

    public function options_m2m()
    {
        return $this->hasMany('App\Product_category_option_m2m', 'category_id');
    }

    // pages_m2m
    public function pages_m2m()
    {
        return $this->hasMany('App\Product_category_page_m2m', 'category_id');
    }

    public function get_pages_m2m()
    {
        $result = $this->pages_m2m()->with('page')->get();

        for ($i = 0; $i < count($result); $i++) {

            if (config('api.none_city')) {
                $result[$i]['url'] = $this->url . '/' . $this->alias . "_" . $result[$i]->page->alias;

            } else {

                $result[$i]['url'] = $this->url . '/' . $this->alias . "_" . $result[$i]->page->alias . '-в-' . '[curr_city.name_p_lw]';
            }

        }

        return $result;

    }

    // END pages_m2m


    // sites_m2m
    public function sites_m2m()
    {
        return $this->hasMany('App\Product_category_site_m2m', 'category_id');
    }

    // END sites_m2m

    public function images()
    {
        return $this->hasMany('App\Product_category_image', 'category_id');
    }

    public function groups_list_m2m()
    {
        return $this->hasMany('App\Product_category_group_list_m2m', 'category_id');

    }

    public function groups_m2m()
    {
        return $this->hasMany('App\Product_category_group_m2m', 'category_id');

    }

    public function bases_categories_m2m()
    {
        return $this->hasMany('App\Product_category_compare_m2m', 'compare_category_id');
    }

    public function compare_categories_m2m()
    {
        return $this->hasMany('App\Product_category_compare_m2m', 'base_category_id');
    }

    public function afterCreate($model)
    {

    }

    public function init($params = [])
    {

        if (config('api.none_city')) {
            $key = 'categories_none_city.'.$this->id;
        } else {
            $key = 'categories.'.$this->id;
        }

        if($params == []) {
            $params = [
                'url',
                'type',
                'products',
                'sites_m2m',
                'pages_m2m',
                'options_m2m',
                'min_price_item',
                'max_price_item',
                'req_for_items',
                'rating',

            ];
        }


        if(Cache::has($key)) {
            $this->attributes = Cache::get($key);
        } else {
            foreach ($params as $param) {

                if (method_exists($this, 'get_' . $param)) {
                    $this[$param] = $this->getParam($param, 'get_' . $param);
                } else {
                    $this[$param] = $this->getParam($param);
                }

            }

            $this->is_init = true;
            Cache::put($key, $this->attributes, 1000);

        }

    }

    /**
     * @return null, App\Product
     */
    public function min_price_item()
    {
        $products = $this->products;

        if ($products->count() > 0) {
            $product_min = $products[0];
            foreach ($products as $product) {
                if ($product_min->price > $product->price) {
                    $product_min = $product;
                }
            }

            return $product_min;

        } else {
            return null;

        }

    }

    public function getMinPriceItemAttribute()
    {
        return $this->min_price_item();
    }

    /**
     * @return null, App\Product
     */
    public function max_price_item()
    {
        $products = $this->products;

        if ($products->count() > 0) {
            $product_max = $products[0];
            foreach ($products as $product) {
                if ($product_max->price < $product->price) {
                    $product_max = $product;
                }
            }

            return $product_max;

        } else {
            return null;

        }

    }


    /**
     * Возвращает похожие товары
     *
     * @return Product_category collect
     */
    public function another_categories($count = 3)
    {
        return Product_category::where('type_id', $this->type_id)
            ->where('id', '<>', $this->id)
            ->where('buy', true)
            ->orderBy('priority', 'desc')
            ->take($count)->get();
    }

    /**
     * @return string
     */
    public function getUrl()
    {

        //TODO: need routers
        if (config('api.none_city')) {

            return '[domain]' . $this->type->alias . '/' . $this->alias . "-купить";

        }

        else {

            return '[domain]' .
            $this->type->alias . '-в-' . '[curr_city.name_p_lw]/' . $this->alias . '-купить-в-[curr_city.name_p_lw]';

        }

    }

    public function get_url()
    {
        return $this->getUrl();
    }


    public function avg_rating()
    {
        $review_options = Review_option::all();

        $summ_ratings = 0;
        $count = 0;
        foreach ($review_options as $opt) {
            if (($n = $this->avg_rating_for_option($opt->id)) > 0) {
                $summ_ratings += $this->avg_rating_for_option($opt->id);
                $count++;
            }

        }

        if ($count === 0) {
            return 5;
        } else {
            return ($result = $summ_ratings / $count) == 0 ? 5 : round($result);
        }


    }


    public function avg_rating_for_option($option_id)
    {
        $values = [];
        foreach ($this->reviews as $review) {
            $values[] = $review->options_m2m()->where('option_id', $option_id)->first();

        }

        return collect($values)->filter(function ($v) {

            if($v)
                return $v->stars > 0;

        })->avg('stars');

    }

    public function max_discount()
    {
        try {
            $q = $this->products()->select(DB::raw('max(price_per_item) as max_price, min(price_per_item) as min_price'))->first();
            return round(100 - $q['min_price'] / $q['max_price'] * 100);

        } catch(\Exception $e) {
            return 0;

        }

    }
    
    
    
    
    // EVENTS




}
