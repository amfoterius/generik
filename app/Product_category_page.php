<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_page extends \App\Base_model
{
    public function categories_m2m()
    {
        return $this->hasMany('App\Product_category_page_m2m', 'page_id');
    }

    public function afterCreate($site)
    {

        foreach(\App\Site\Site::select('id')->get()->pluck('id') as $site_id) {

            if(is_null($this->categories_m2m()->where('site_id', $site_id)->first())) {

                $model = new \App\Product_category_page_m2m();
                $model->site_id = $site_id;
                $model->page_id = $this->id;
                $model->save();

            }
        }


    }
}
