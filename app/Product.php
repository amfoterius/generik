<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
class Product extends Base_model
{
    use SearchableTrait;

    const file_path = 'product';

    public function category()
    {
        return $this->belongsTo('App\Product_category', 'category_id');
    }


    /**
     * @param $query
     * @param $category_id
     * @return mixed
     */
    public function scopeByCategory($query, $category_id)
    {
        $subcategories_ids = DB::table('product_categories')->where('parent_id', $category_id)->lists('id');
        $subcategories_ids[] = $category_id;

        return $query->whereIn('category_id', $subcategories_ids)->orderBy('priority', 'desc');

    }






}


