<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_type extends \App\Base_model
{

    const file_path = 'product_category_type';
    const image_sizes = [

        'image_banner' => [

            'medium' => [800, 120],

        ],


    ];

    public function categories()
    {
        return $this->hasMany('App\Product_category', 'type_id');
    }

    public function sites_m2m()
    {
        return $this->hasMany('App\Site\Site_category_type_m2m', 'type_id');

    }

    public function getUrl()
    {

        //TODO: need routers
        if(config('api.none_city'))
        {
            return config('api.domain') . $this->alias;
        }
        else
        {
            return config('api.domain') . $this->alias . '-в-' . '[curr_city.name_p_lw]/';
        }



    }
}
