<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Product_category', 'product_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function options_m2m()
    {
        return $this->hasMany('App\Review_option_m2m', 'review_id');
    }

    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');

    }

    public function avg_rating()
    {
        $avg = $this->options_m2m()->where('stars', '>', 0)->avg('stars');
        return round($avg == null ? 5 : $avg);
    }


}
