<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Order extends Model
{

    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');
    }


    public function init_products()
    {
        $products = json_decode($this->products);

        $result = [];
        foreach ($products as $id => $product) {

            $result[$id] = Product::find($id);

        }

        return $result;

    }
}
