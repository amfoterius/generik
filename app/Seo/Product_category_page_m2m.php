<?php

namespace App\Seo;

use Illuminate\Database\Eloquent\Model;

class Product_category_page_m2m extends Model
{
    protected $table = 'seo_product_category_page_m2m';

    public function page_m2m()
    {
        return $this->belongsTo('App\Product_category_page_m2m', 'page_m2m_id');
    }

    public function site()
    {
        return $this->belongsTo('App\Site\Site', 'site_id');
    }
}
