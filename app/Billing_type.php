<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing_type extends Model
{
    public function delivery()
    {
        return $this->belongsTo('App\Delivery_type');
    }
}
