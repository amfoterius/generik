<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review_option extends Model
{
    public function reviews_m2m()
    {
        return $this->hasMany('App\Review_option_m2m', 'option_id');
    }
}
