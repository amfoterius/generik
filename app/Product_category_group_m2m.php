<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category_group_m2m extends Model
{
    protected $table = 'product_category_group_m2m';

    public function group()
    {
        return $this->belongsTo('App\Product_category_group', 'group_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Product_category', 'category_id');
    }
}
