<?php


Route::get('test', 'HomeController@test');
Route::get('test1', 'HomeController@test1');

// images
Route::get('files/images/{model}/{option}/{size}/{site}-{filename}', 'FilesController@getImage');
Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET,POST");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

   Route::group(['prefix' => 'product_categories'], function() {
       Route::any('getList', 'ProductCategoriesController@getListPopular');
       Route::any('getOne', 'ProductCategoriesController@getOne');
       Route::any('getForAlias/{alias}', 'ProductCategoriesController@getForAlias');
   });

    Route::group(['prefix' => 'product_category_types'], function() {
        Route::any('getTypeForAlias', 'ProductCategoryTypesController@getTypeForAlias');
        Route::any('getTypesAndCategories', 'ProductCategoryTypesController@getTypesAndCategories');
    });

});

Route::group(['domain' => '{subdomain}.{domain}.xn--p1ai'], function() {

    Route::any('/', ['as' => 'index', 'uses' => 'AllCityController@index']);

    Route::get('/каталог', ['as' => 'catalog', 'uses' => 'AllCityController@catalog']);
    Route::get('/доставка', ['as' => 'delivery', 'uses' => 'AllCityController@delivery']);
    Route::get('/контакты', ['as' => 'contacts', 'uses' => 'AllCityController@contacts']);
    Route::get('/поиск', ['as' => 'contacts', 'uses' => 'AllCityController@search']);
    Route::any('города', ['as' => 'cities', 'uses' => 'AllCityController@start']);

    Route::get('/sitemap.xml', ['as' => 'sitemap', 'uses' => 'AllCityController@sitemap']);
    Route::get('/robots.txt', ['as' => 'robots', 'uses' => 'AllCityController@robots']);

    Route::get('/скидки', ['as' => 'sales', 'uses' => 'AllCityController@sales']);
    Route::get('/отзывы', ['as' => 'reviews', 'uses' => 'AllCityController@reviews']);


    Route::get('сравнение', ['as' => 'compare', 'uses' => 'AllCityController@compare']);
// Route::get('скидки-и-акции', ['as' => 'sales', 'uses' => 'HomeController@sale']);

    Route::get('корзина', ['as' => 'cart', 'uses' => 'CartController@index']);
// Route::get('корзина/шаг2', ['as' => 'cart.step2', 'uses' => 'CartController@step2']);
// Route::post('корзина/шаг3', ['as' => 'cart.step3', 'uses' => 'CartController@step3']);
    Route::post('корзина/новый-заказ', ['as' => 'cart.new_order', 'uses' => 'CartController@new_order']);



    // AJAX

    Route::group(['prefix' => 'ajax', 'namespace' => 'Ajax'], function() {


        //
        Route::group(['prefix' => 'cart'], function(){

            Route::post('add', 'CartController@add');
            Route::post('remove', 'CartController@remove');
            Route::get('clear', 'CartController@clear');
            Route::post('new_order', 'CartController@new_order');

        });

        //
        Route::group(['prefix' => 'compare'], function(){

            Route::post('add', 'CompareController@add');
            Route::post('remove', 'CompareController@remove');
            Route::get('clear', 'CompareController@clear');
            Route::get('getOnCompare', 'CompareController@getOnCompare');


        });

        //
        Route::group(['prefix' => 'review'], function(){

            Route::get('vote', 'Product_category_review@vote');
            Route::get('vote_category_page', 'Product_category_review@vote_category_page');
            Route::post('add_review_category_page', 'Product_category_review@add_review_category_page');

        });


    });

    // END Ajax


    Route::get('/yandex_{key}.html', ['as' => 'yandex_verification', 'uses' => 'HomeController@verification']);
    Route::get('/google{key}.html', ['as' => 'google_verification', 'uses' => 'HomeController@google_verification']);




    Route::get(
        '/{type}-в-{city_p}',
        ['as' => 'type', 'uses' => 'AllCityController@type']
    )
        ->where(['type' => '[^/]+']);


    Route::get(
        '/{type}-в-{city_p}/{category}-купить-в-{city_p1}',
        ['as' => 'category', 'uses' => 'AllCityController@category']
    )
        ->where(['type' => '[^/]+', 'category' => '[^/]+']);

    Route::any(
        '/{type}-в-{city_p}/{category}-купить-в-{city_p1}/добавить-отзыв',
        ['as' => 'category_review_add', 'uses' => 'AllCityController@category_review_add']
    )
        ->where(['type' => '[^/]+', 'category' => '[^/]+']);


    Route::get(
        '/{type}-в-{city_p}/{category}-купить-в-{city_p1}/{category1}_{page}-в-{city_p2}',
        ['as' => 'category_page', 'uses' => 'AllCityController@category_page']
    )
        ->where(['type' => '[^/]+', 'category' => '[^/]+', 'page' => '[^/]+']);



});



/*
 * ======================================================================================================
 * ======================================================================================================
 * ======================================================================================================
 */

Route::group(['domain' => '{domain}.xn--p1ai'], function() {

    Route::any('/', ['as' => 'index_none_city', 'uses' => 'NoneCityController@index']);

    Route::get('/каталог', ['as' => 'catalog_none_city', 'uses' => 'NoneCityController@catalog']);
    Route::get('/доставка', ['as' => 'delivery_none_city', 'uses' => 'NoneCityController@delivery']);
    Route::get('/контакты', ['as' => 'contacts_none_city', 'uses' => 'NoneCityController@contacts']);
    Route::get('/поиск', ['as' => 'contacts_none_city', 'uses' => 'NoneCityController@search']);
    Route::any('/города', ['as' => 'cities', 'uses' => 'NoneCityController@start']);

    Route::get('/sitemap.xml', ['as' => 'sitemap_none_city', 'uses' => 'NoneCityController@sitemap']);
    Route::get('/robots.txt', ['as' => 'robots_none_city', 'uses' => 'NoneCityController@robots']);

    Route::get('/скидки', ['as' => 'sales', 'uses' => 'NoneCityController@sales']);
    Route::get('/отзывы', ['as' => 'reviews', 'uses' => 'NoneCityController@reviews']);

    Route::get('сравнение', ['as' => 'compare_none_city', 'uses' => 'NoneCityController@compare']);
    // Route::get('скидки-и-акции', ['as' => 'sales', 'uses' => 'HomeController@sale']);

    Route::get('корзина', ['as' => 'cart', 'uses' => 'CartController@index']);
    // Route::get('корзина/шаг2', ['as' => 'cart.step2', 'uses' => 'CartController@step2']);
    // Route::post('корзина/шаг3', ['as' => 'cart.step3', 'uses' => 'CartController@step3']);
    Route::post('корзина/новый-заказ', ['as' => 'cart.new_order', 'uses' => 'CartController@new_order']);


    // AJAX

    Route::group(['prefix' => 'ajax', 'namespace' => 'Ajax'], function() {


        //
        Route::group(['prefix' => 'cart'], function(){

            Route::post('add', 'CartController@add');
            Route::post('remove', 'CartController@remove');
            Route::get('clear', 'CartController@clear');
            Route::post('new_order', 'CartController@new_order');

        });

        //
        Route::group(['prefix' => 'compare'], function(){

            Route::post('add', 'CompareController@add');
            Route::post('remove', 'CompareController@remove');
            Route::get('clear', 'CompareController@clear');
            Route::get('getOnCompare', 'CompareController@getOnCompare');

        });

        //
        Route::group(['prefix' => 'review'], function(){

            Route::get('vote', 'Product_category_review@vote');
            Route::get('vote_category_page', 'Product_category_review@vote_category_page');
            Route::post('add_review_category_page', 'Product_category_review@add_review_category_page');

        });


    });

    // END Ajax

    Route::get('/yandex_{key}.html', ['as' => 'yandex_verification_none_city', 'uses' => 'NoneCityController@verification']);
    Route::get('/google{key}.html', ['as' => 'google_verification_none_city', 'uses' => 'NoneCityController@google_verification']);



    // for one city domains

    Route::get(
        '/{type}-в-{city_p}',
        ['as' => 'type_one_city', 'uses' => 'OneCityController@type']
    )
        ->where(['type' => '[^/]+']);


    Route::get(
        '/{type}-в-{city_p}/{category}-купить-в-{city_p1}',
        ['as' => 'category_one_city', 'uses' => 'OneCityController@category']
    )
        ->where(['type' => '[^/]+', 'category' => '[^/]+']);

    Route::any(
        '/{type}-в-{city_p}/{category}-купить-в-{city_p1}/добавить-отзыв',
        ['as' => 'category_review_add_one_city', 'uses' => 'OneCityController@category_review_add']
    )
        ->where(['type' => '[^/]+', 'category' => '[^/]+']);


    Route::get(
        '/{type}-в-{city_p}/{category}-купить-в-{city_p1}/{category1}_{page}-в-{city_p2}',
        ['as' => 'category_page_one_city', 'uses' => 'OneCityController@category_page']
    )
        ->where(['type' => '[^/]+', 'category' => '[^/]+', 'page' => '[^/]+']);

    // END for one city domains


    Route::get(
        '/{type}',
        ['as' => 'type_none_city', 'uses' => 'NoneCityController@type']
    )
        ->where(['type' => '[^/]+']);

    Route::get(
        '/{type}/{category}-купить',
        ['as' => 'category_none_city', 'uses' => 'NoneCityController@category']
    )
        ->where(['type' => '[^/]+', 'category' => '[^/]+']);

    Route::get('/{type}/{category}', function($domain, $type, $category) {
        return redirect(route('category_none_city', [$domain, $type, $category]), 301);
    })
        ->where(['type' => '[^/]+', 'category' => '[^/]+']);

    Route::any(
        '/{type}/{category}-купить/добавить-отзыв',
        ['as' => 'category_review_add_none_city', 'uses' => 'NoneCityController@category_review_add']
    )
        ->where(['type' => '[^/]+', 'category' => '[^/]+']);

    Route::get(
        '/{type}/{category}-купить/{category1}_{page}',
        ['as' => 'category_page_none_city', 'uses' => 'NoneCityController@category_page']
    )
        ->where(['type' => '[^/]+', 'category' => '[^/]+', 'page' => '[^/]+']);







});



//Route::any('/', ['as' => 'index', 'uses' => 'HomeController@index']);


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

















