<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use SimpleXMLElement;
use Cache;
use Hash;

use App\Product_category;
use App\Product_category_type;
use App\City;
use App\Site\Site;
use App\Site\Site_category_page_m2m;
use App\Product_category_page;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $data = [];

    public function __construct(Request $request)
    {

        $this->data['aliases'] = [];
        $this->data['page_meta'] = null;
        $this->data['curr_page'] = 'index';

        $host_arr = preg_split('/\./', $_SERVER["HTTP_HOST"]);

        //TODO:
        if (count($host_arr) == 3) {

            config(['api.none_city' => false]);


            $this->data['curr_city'] = City::where('alias', $host_arr[0])->first();
            if ($this->data['curr_city'] == null) {
                abort(404);
            }

            if (empty($this->data['curr_city']->name_p)) {
                try {

                    $xml = file_get_contents('https://export.yandex.ru/inflect.xml?name=' . urlencode($this->data['curr_city']->name));
                    $obj = new SimpleXMLElement($xml);
                    $this->data['curr_city']->name_r = $obj->inflection[1] . "";
                    $this->data['curr_city']->name_v = $obj->inflection[3] . "";
                    $this->data['curr_city']->name_p = $obj->inflection[5] . "";
                    $this->data['curr_city']->save();


                } catch (Exception $e) {


                }
            }

            $this->data['curr_city']['name_p_lw'] = mb_strtolower($this->data['curr_city']->name_p);


            // TODO: local and prod
            $this->data['curr_site'] = Site::where('url', $host_arr[1])
                ->orWhere('url_local', $host_arr[1])
                ->first();

            if(is_null($this->data['curr_site'])) {
                abort(404);
            }

            config(['api.site_id' => $this->data['curr_site']->id]);

            // поправка для доменов, привязанные к городам
            if(!empty($this->data['curr_site']->city_id)) {
                abort(404);
            }

            if ($this->data['curr_site']->url_local == $host_arr[0]) {
                $this->data['domain'] = 'http://[curr_city.alias].[curr_site.url_local].xn--p1ai/';
            } else {
                $this->data['domain'] = 'http://' . '[curr_city.alias].[curr_site.url].xn--p1ai/';
            }

        } elseif (count($host_arr) == 2) {


            // TODO: local and prod
            $this->data['curr_site'] = Site::where('url', $host_arr[0])
                ->orWhere('url_local', $host_arr[0])
                ->first();


            if ($this->data['curr_site']->url_local == $host_arr[0]) {
                $this->data['domain'] = 'http://[curr_site.url_local].xn--p1ai/';
            } else {
                $this->data['domain'] = 'http://' . '[curr_site.url].xn--p1ai/';
            }

            // поправка для доменов, привязанные к городам
            if(!empty($this->data['curr_site']->city_id)) {

                $this->data['curr_city'] = \App\City::find($this->data['curr_site']->city_id);
                config(['api.is_one_city' => true]);
                config(['api.none_city' => false]);
                $this->data['curr_city']['name_p_lw'] = mb_strtolower($this->data['curr_city']->name_p);

            }

        }


        config(['api.curr_site_id' => $this->data['curr_site']->id]);
        if(isset($this->data['curr_city'])) {
            config(['api.curr_city_id' => $this->data['curr_city']->id]);
        }
        config(['api.domain' => $this->data['domain']]);

        $this->init_session($request);

        // menu
        $this->data['menu'] = collect([

            'catalog' => ['Каталог', 'glyphicon glyphicon-th-large'],
            'delivery' => ['Доставка', 'glyphicon glyphicon-globe'],
            'contacts' => ['Контакты', 'glyphicon glyphicon-globe']
        ]);


        $this->data['types'] = Product_category_type::orderBy('priority', 'desc')->with('categories')->get();
        $this->data['cart'] = collect($request->session()->get('cart'));

        $this->data['compare'] = collect($request->session()->get('compare'));


        $this->data['cart_count'] = 0;
        foreach ($this->data['cart']['products'] as $c) {
            $this->data['cart_count'] += $c;
        }

        //meta TODO: need cache
        $this->data['seo'] = collect(json_decode(file_get_contents(base_path("storage/administrator_settings/seo.json"))));


    }

    public function init($argv = [])
    {


        if (!empty($this->data['curr_site']->logo))
            $this->data['is_have_logo'] = 1;


        $this->data['page_meta'] = $this->data['curr_site']->getMetaForPage($this->data['curr_page']);
        $this->data['curr_page'] = $this->data['page_meta'];
        $this->data['token'] = csrf_token();


        // Проверка алиасов
        foreach ($this->data['aliases'] as $k => $v) {

            switch ($k) {
                case 'start':
                    $this->data['cache_key'] = '_start_' . $this->data['curr_site']->id;
                    break;
                case 'index':
                    $this->data['cache_key'] = '_index_' . $this->data['curr_site']->id;
                    $this->data['categories'] = Product_category::where('type_id', '>', 0)
                        ->orderBy('priority', 'desc')->get()->each(function($category, $k) {
                            $category->init();
                        });
                    break;

                case 'index_none_city':
                    $this->data['cache_key'] = '_index_none_city_' . $this->data['curr_site']->id;
                    $this->data['categories'] = Product_category::where('type_id', '>', 0)
                        ->orderBy('priority', 'desc')->get()->each(function($category, $k) {
                            $category->init();
                        });
                    break;

                case 'catalog':
                    $this->data['cache_key'] = '_catalog_' . $this->data['curr_site']->name;
                    break;

                case 'catalog_none_city':
                    $this->data['cache_key'] = '_catalog_none_city_' . $this->data['curr_site']->name;
                    break;


                case 'type':
                    if (($this->data['curr_type'] = Product_category_type::where('alias', $v)->first()) == null) {
                        return abort(404);
                    }

                    $this->data['curr_type']->categories->each(function($category, $k) {

                        $category->init();

                    });

                    $this->data['cache_key'] = '_type_' . $this->data['curr_site']->name . "_" . $this->data['curr_type']->id;
                    break;

                case 'type_none_city':
                    if (($this->data['curr_type'] = Product_category_type::where('alias', $v)->with('categories')->first()) == null) {
                        return abort(404);
                    }

                    $this->data['curr_type']->categories->each(function($category, $k) {

                        $category->init();

                    });

                    $this->data['cache_key'] = '_type_none_city_' . $this->data['curr_type']->id;
                    break;

                case 'category':
                    if (($this->data['curr_category'] = Product_category::where('alias', $v)->with('type')->first()) == null) {
                        return abort(404);
                    }
                    $this->data['cache_key'] = '_category_' . $this->data['curr_site']->name . $this->data['curr_category']->id;
                    $this->data['curr_category_page'] = null;
                    $this->data['curr_category_site_m2m'] = $this->data['curr_category']->sites_m2m()
                        ->where('site_id', $this->data['curr_site']->id)->first();
                    break;

                case 'category_none_city':
                    if (($this->data['curr_category'] = Product_category::where('alias', $v)->first()) == null) {
                        return abort(404);
                    }
                    $this->data['cache_key'] = '_category_none_city_' . $this->data['curr_site']->name . $this->data['curr_category']->id;
                    $this->data['curr_category_page'] = null;
                    $this->data['curr_category_site_m2m'] = $this->data['curr_category']->sites_m2m()
                        ->where('site_id', $this->data['curr_site']->id)->first();
                    break;

                case 'category1':
                    if ($this->data['curr_category']->alias !== $v) {
                        return abort(404);
                    }
                    break;

                case 'page':
                    $this->data['curr_category_page'] = Product_category_page::where('alias', $v)->first()
                        ->categories_m2m()->where('category_id', $this->data['curr_category']->id)->first();
                    if ($this->data['curr_category_page'] == null) {
                        return abort(404);
                    }

                    $this->data['site_category_page'] = Site_category_page_m2m::where('site_id', $this->data['curr_site']->id)
                        ->where('page_id', $this->data['curr_category_page']->page->id)->first();

                    $this->data['cache_key'] = '_category_page_' . $this->data['curr_site']->name . $this->data['curr_category_page']->id;

                    break;

                case 'page_none_city':
                    $this->data['curr_category_page'] = Product_category_page::where('alias', $v)->first()
                        ->categories_m2m()->where('category_id', $this->data['curr_category']->id)->first();
                    if ($this->data['curr_category_page'] == null) {
                        return abort(404);
                    }

                    $this->data['site_category_page'] = Site_category_page_m2m::where('site_id', $this->data['curr_site']->id)
                        ->where('page_id', $this->data['curr_category_page']->page->id)->first();

                    $this->data['cache_key'] = '_category_page_none_city_' . $this->data['curr_site']->name . $this->data['curr_category_page']->id;

                    break;


                case 'city_p':
                    if ($v != mb_strtolower($this->data['curr_city']->name_p)) {

                        return abort(404);
                    }
                    break;

                case 'city_p1':
                    if ($v != mb_strtolower($this->data['curr_city']->name_p)) {
                        return abort(404);
                    }
                    break;

                case 'city_p2':
                    if ($v != mb_strtolower($this->data['curr_city']->name_p)) {
                        return abort(404);
                    }
                    break;

            }
        }
    }


    private function init_session(Request $request)
    {
        if (!$request->session()->has('cart')) {
            $request->session()->put('cart', [
                'name' => '',
                'tel' => '',
                'address' => '',
                'products' => []
            ]);
        }

        if (!$request->session()->has('review_likes')) {
            $request->session()->put('review_likes', []);
        }

        if (!$request->session()->has('category_page_review_likes')) {
            $request->session()->put('category_page_review_likes', []);
        }


        if (!$request->session()->has('compare')) {
            $request->session()->put('compare', []);
        }

        if (!$request->session()->has('last_order')) {
            $request->session()->put('last_order', 0);
        }

    }

    public function settings_data($file_name)
    {
        $path = base_path("storage/administrator_settings/$file_name.json");

        if (file_exists($path)) {
            return collect(json_decode(file_get_contents($path)));

        } else {
            return collect(json_decode(file_get_contents(base_path("storage/administrator_settings/index.json"))));

        }

    }


    public function m_view($view, $is_cache = true)
    {


        \Debugbar::startMeasure('m_view_1', '');

        if (isset($this->data['cache_key']) && !config('app.debug') && $is_cache) {

            \Debugbar::info('HAS CACHE');

            $cache_key = $this->data['curr_site']->id . $this->data['cache_key'];

            if (Cache::has($cache_key)) {
                $content = Cache::get($cache_key);
            } else {
                if (view()->exists($this->data['curr_site']->id . '.' . $view)) {
                    $content = view($this->data['curr_site']->id . '.' . $view, $this->data)->render();
                } else {
                    $content = view($view, $this->data)->render();
                }
                Cache::put($cache_key, $content, 1800);
            }

        } else {

            \Debugbar::info('HAS NOT CACHE');

            if (view()->exists($this->data['curr_site']->id . '.' . $view)) {
                $content = view($this->data['curr_site']->id . '.' . $view, $this->data)->render();
            } else {
                $content = view($view, $this->data)->render();
            }

        }


        \Debugbar::stopMeasure('m_view_1');

        \Debugbar::startMeasure('m_view_2', '');

        $matches = [];


        preg_match_all('/\[[a-z0-9_.]{2,}\]/', $content, $matches);
        $matches = array_unique($matches[0]);


        foreach ($matches as $match) {
            $val = substr($match, 1, -1);

            $el_arr = explode(".", $val);


            switch (count($el_arr)) {

                case 1:
                    if (isset($this->data["" . $el_arr[0]])) {
                        $val = $this->data["" . $el_arr[0]];
                    }
                    break;

                case 2:
                    if (isset($this->data["" . $el_arr[0]]["" . $el_arr[1]])) {
                        $val = $this->data["" . $el_arr[0]]["" . $el_arr[1]];
                    }
                    break;

                case 3:
                    if (isset($this->data["" . $el_arr[0]]["" . $el_arr[1]]["" . $el_arr[2]])) {
                        $val = $this->data["" . $el_arr[0]]["" . $el_arr[1]]["" . $el_arr[2]];
                    }

                    break;

            }

            $content = str_replace($match, $val, $content);

        }


        // Второй уровень обработки
        preg_match_all('/\[[a-z0-9_.]{2,}\]/', $content, $matches);
        $matches = array_unique($matches[0]);


        foreach ($matches as $match) {
            $val = substr($match, 1, -1);

            $el_arr = explode(".", $val);


            switch (count($el_arr)) {

                case 1:
                    if (isset($this->data["" . $el_arr[0]])) {
                        $val = $this->data["" . $el_arr[0]];
                    }
                    break;

                case 2:
                    if (isset($this->data["" . $el_arr[0]]["" . $el_arr[1]])) {
                        $val = $this->data["" . $el_arr[0]]["" . $el_arr[1]];
                    }
                    break;

                case 3:
                    if (isset($this->data["" . $el_arr[0]]["" . $el_arr[1]]["" . $el_arr[2]])) {
                        $val = $this->data["" . $el_arr[0]]["" . $el_arr[1]]["" . $el_arr[2]];
                    }

                    break;

            }

            $content = str_replace($match, $val, $content);

        }
        // END Второй уровень обработки

        // for none_city
        if (config('api.none_city')) {
            $content = str_replace(' в curr_city.name_p', '', $content);
            $content = str_replace(' в curr_city.name_v', '', $content);
            $content = str_replace(' в curr_city.name_v', '', $content);

        }

        \Debugbar::stopMeasure('m_view_2');


        return $content;
    }


}
