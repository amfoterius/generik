<?php

namespace App\Http\Controllers\Ajax;

use App\Product_category_page_review;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Review;


class Product_category_review extends Controller
{
    public function vote(Request $request)
    {
        $review_likes = $request->session()->get('review_likes');

        if(!is_null($review = Review::find($request->input('review_id'))))
        {
            if(array_search($request->input('review_id'), $request->session()->get('review_likes')) !== false)
            {
                $review->votes = $review->votes - 1;


                unset($review_likes[array_search($request->input('review_id'), $review_likes)]);
                $request->session()->set('review_likes', $review_likes);

            }
            else
            {
                $review->votes = $review->votes + 1;

                $review_likes[] = $request->input('review_id');
                $request->session()->set('review_likes', $review_likes);

            }

            // $request->session()->set('review_likes', []);
            $review->save();
            return $review_likes;


        }
        else
        {
            return -1;

        }

    }

    public function vote_category_page(Request $request)
    {
        $category_page_review_likes = $request->session()->get('category_page_review_likes');

        if(!is_null($review = Product_category_page_review::find($request->input('review_id'))))
        {
            if(array_search($request->input('review_id'), $category_page_review_likes) !== false)
            {
                $review->votes = $review->votes - 1;


                unset($category_page_review_likes[array_search($request->input('review_id'), $category_page_review_likes)]);
                $request->session()->set('category_page_review_likes', $category_page_review_likes);

            }
            else
            {
                $review->votes = $review->votes + 1;

                $category_page_review_likes[] = $request->input('review_id');
                $request->session()->set('category_page_review_likes', $category_page_review_likes);

            }



            // $request->session()->set('review_likes', []);
            $review->save();
            return $category_page_review_likes;


        }
        else
        {
            return -1;

        }


    }

    public function add_review_category_page(Request $request)
    {
        $result = [];

        $category_page_id = $request->input('category_page_id', '');
        $site_id = $request->input('site_id', '');
        $name = $request->input('name', '');
        $text = $request->input('text', '');


        if($name != '' && $text != '' && $category_page_id != '' && $site_id != '')
        {
            $review = new Product_category_page_review();
            $review->category_page_id = $category_page_id;
            $review->site_id = $site_id;
            $review->name = $name;
            $review->text = $text;
            $review->save();


            $result['block'] = view('components.page_review_block', [
                'review' => $review,
                'category_page_review_likes' => $request->session()->get('category_page_review_likes')
            ])->render();
            return $result;

        }

        return -1;


    }

}
