<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CompareController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->compare = $request->session()->get('compare');


    }


    public function add(Request $request)
    {
        if(!collect($this->compare)->search($request->input('id')))
        {
            $this->compare[] = $request->input('id');
            $request->session()->put('compare', $this->compare);
        }
        return $this->compare;

    }

    public function remove(Request $request)
    {
        if(collect($this->compare)->search($request->input('id')) !== false)
        {
            $this->compare = collect($this->compare)->diff([$request->input('id')])->toArray();

            $request->session()->put('compare', $this->compare);

        }




        return $this->compare;


    }

    public function getOnCompare(Request $request)
    {
        return $this->compare;

    }


}
