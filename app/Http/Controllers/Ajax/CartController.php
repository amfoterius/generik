<?php

namespace App\Http\Controllers\Ajax;

use App\Billing_type;
use Cache;
use Validator;
use App\Product;
use App\Product_category;
use App\Order;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class CartController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);


        $this->data['cart']['products'] = collect($this->data['cart']['products'])->map(function ($value, $key) {

            $result = Product::find($key);
            $result->count = $value;

            return $result;

        });

        $this->data['settings'] = [

            'meta_title'=>'������� �������',
            'meta_description'=>'',
            'meta_keywords'=>'',
        ];



    }


    public function add(Request $request)
    {

        $product_id = $request->input('product_id');
        $product_count = $request->input('product_count');
        $is_one_click = $request->input('is_one_click');

        if ($is_one_click) {
            $request->session('cart.products')->put('cart.products', []);

        }

        if (!$request->session()->has('cart.products.' . $product_id)) {
            $request->session()->put('cart.products.' . $product_id, $product_count);
        } else {
            $t = $request->session()->get('cart.products.' . $product_id) + $product_count;
            $request->session()->put('cart.products.' . $product_id, $t);
        }

        return $request->session()->get('cart');

    }

    public function remove(Request $request)
    {
        $product_id = $request->input('product_id');

        try {
            $request->session()->forget('cart.products.' . $product_id);
            $this->data['cart']['products'] = collect($request->session()->get('cart.products'))->map(function ($value, $key) {

                $result = Product::find($key);
                $result->count = $value;

                return $result;

            });

        } catch (Exception $e) {

        }
        return $this->data['cart']['products'];


    }


    public function new_order(Request $request)
    {
        // captcha or another
        /*
        if($request->input('not_captcha', '') == '') {
            $post_param = "secret=6LdtIScTAAAAAI3ogRsmkU4IrGaFWGM9KG0Y3tzW&response=".$_POST['g-recaptcha-response'];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_param);
            $out = curl_exec($curl);

            if(!json_decode($out)->success) {
                return redirect(url('/'));
            }

        } else {



        }
        */

        // ddos protection
        $key = 'protection.ddos.'.str_replace('.', '-', $request->ip());

        if(Cache::has($key)) {
            return $this->m_view('cart.error_ddos', $this->data);
        } else {
            Cache::put($key, 1, 5);
        }


        $products_for_order_arr = [];
        $this->init();

        if (empty($request->input('callback'))) {
            foreach ($this->data['cart']['products'] as $key => $product) {
                $products_for_order_arr[$product->api_id] = $product->count;
            }

        } else {
            if (!empty($request->input('product-id'))) {
                $product = Product::find($request->input('product-id'));
                $products_for_order_arr[$product->api_id] = 1;
            }
        }


        $products_str = serialize($products_for_order_arr);

        /*
        $validator = Validator::make($request->all(), [
            'fio' => 'required|max:300|min:5',
            'email' => 'required|max:100',
            'tel' => 'required|min:10|max:15',
            'city' => 'required|min:2|max:50',
            'address' => 'required|min:10|max:500',
        ]);


        if ($validator->fails()) {
            return $validator->errors();
        }
        */

        $order = new Order;
        $order->fio = $request->input('fio');

        $order->email = $request->input('email', '');
        $order->tel = $request->input('tel');
        $order->city = $request->input('city', '');
        $order->address = $request->input('address', '');
        $order->products = $products_str;
        $order->site_id = $this->data['curr_site']->id;

        $order->save();

        if($this->data['curr_site']->id == 9) {
            config(['api.key' => 'ae4580683d88790ce273e6fc5668a8df']);
        }

        // '.config('api.key').'
        $req = 'http://api.fastnsafe.ru/api.php?api_key=' . config('api.key') . '&
        site_id=' . config('api.site_id') . '&
        action=create_user&
        email=' . $order->email . '&
        pass=kaluga&
        phone=' . $order->tel . '&
        fio=' . $order->fio . '&
        city=' . $order->city . '&
        address=' . $order->address . '&
        pindex=&
        ip=' . '85.21.240.65' . '&
        wm_id=' . config('api.wm_id') . '';


        $u = file_get_contents(preg_replace('/\n|\s/', "", $req));


        $req = 'http://api.fastnsafe.ru/api.php?api_key=' . config('api.key') . '&
        site_id=2&
        action=create_order&
        basket_arr=' . urlencode($order->products) . '&
        user_id=' . $u . '&
        wm_id=' . config('api.wm_id') . '&
        site_id=' . config('api.site_id') . '&
        sub_id=0&
        promo_id=0&
        inst=stat_20151110_88023&
        service_id=' . $request->input('delivery') . '&
        payment_id=' . $request->input('billing') . '&
        ip=' . '85.21.240.65' . '&
        comments=&
        promo_code='
            . (!empty($request->input('callback')) ? "&request_callback=1" : "");

        $r = file_get_contents(preg_replace('/\n|\s/', "", $req));


        $r_order = unserialize($r);
        $order->api_id = $r_order['order_id'];
        $order->save();

        $this->data['curr_order'] = $order;

        $this->data['curr_billing'] = Billing_type::where('api_id', $request->input('billing'))->first();
        $this->data['num_order'] = $r;
        $this->data['callback'] = $request->input('callback');

        $request->session('cart.products')->put('cart.products', []);

        return $this->m_view('cart.order_success', $this->data);


    }







    public function clear(Request $request)
    {

        /*
        if($request->session()->has('cart'))
        {
            $request->session()->put('cart.products', []);

        }
        */
        $request->session()->flush();

        $request->session()->put('cart', [
            'fio' => '',
            'email' => '',
            'password' => '',
            'tel' => '',
            'city' => '',
            'address' => '',
            'delivery' => '',
            'products' => []
        ]);


        return $request->session()->get('cart');

    }


}
