<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\Product_category;
use App\Billing_type;
use App\Delivery_type;

class CartController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);


        $this->data['cart']['products'] = collect($this->data['cart']['products'])->map(function ($value, $key) {

            $result = Product::find($key);
            $result->count = $value;

            return $result;

        });

        $this->data['summ_kolvo'] = 0;

        foreach($this->data['cart']['products'] as $product)
        {
            $this->data['summ_kolvo'] += $product->count * $product->kolvo;
        }

        $this->data['summ'] = $this->data['cart']['products']->reduce(function($carry, $item){return $carry + $item->count*$item->price;});
        $this->data['summ_discount'] = $this->data['summ'] * 0.05;

        if($this->data['summ_kolvo'] < 50)
        {
            $this->data['summ'] += 300;

        }

        $this->data['settings'] = [

            'meta_title'=>'Корзина товаров',
            'meta_description'=>'',
            'meta_keywords'=>'',
        ];

        // TODO:
        $this->data['viagra_category'] = Product_category::find(137);


    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->init();


        $this->data['delivery_types'] = Delivery_type::all();
        $this->data['billing_types'] = Billing_type::all();


        return $this->m_view('cart.index');

    }

    public function step2(Request $request)
    {
        $this->init();


        return $this->m_view('cart.step2');
    }

    public function step3(Request $request)
    {
        $this->init();

        $this->data['delivery_types'] = Delivery_type::all();
        $this->data['billing_types'] = Billing_type::all();

        return $this->m_view('cart.step3', $this->data);
    }



}
