<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Cache;

use App\Product_category;
use App\Product_category_type;

class ProductCategoriesController extends Controller
{

    public function __construct(Request $request)
    {

    }

    private function getHashRequest(Request $request) {
        $arr = [
            $request->input('join', []),
            $request->input('select', []),
            $request->input('skip', 0),
            $request->input('take', 100)
        ];

        return Hash::make(json_encode($arr));

    }

    public function getListPopular(Request $request)
    {
        $cache_id = 'api_product_categories_'.$this->getHashRequest($request);

        if (Cache::has($cache_id)) {
            return Cache::get($cache_id);
        } else {
            $base_params = ['id', 'name', 'alias'];
            $join = $request->input('join', []);

            $q = Product_category::filterRequest($request)->orderBy('priority', 'desc');
            $q = call_user_func_array([$q, 'select'], array_merge($base_params, $request->input('select', [])));

            $categories = $q->skip($request->input('skip', 0))->take($request->input('take', 20))->get()->map(function($v, $k) use ($join) {

                foreach($join as $param) {
                    $v[$param] = $v[$param];
                }

                return $v;

            });

            Cache::put($cache_id, $categories, 60);
            return $categories;

        }



    }

    public function getOne(Request $request)
    {
        return Product_category::find($request->input('id', 0))->init();
    }

    public function getForAlias(Request $request, $alias)
    {
        return Product_category::where('alias', $alias)->with(
            'products', 'options_m2m', 'options_m2m.option'
        )->first();

    }


}
