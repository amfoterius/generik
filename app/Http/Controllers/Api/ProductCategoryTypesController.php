<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product_category_type;


class ProductCategoryTypesController extends Controller
{
    public function __construct(Request $request)
    {

    }

    /**
     * alias string Алиас типа
     *
     * @param Request $request
     * @return mixed
     */
    public function getTypeForAlias(Request $request)
    {
        $type = Product_category_type::where('alias', $request->input('alias', ''))->first();
        $type->categories = $type->categories()->with('type', 'products')->select('id', 'name', 'alias', 'priority', 'img_big')->orderBy('priority', 'desc')->get();

        return $type;
    }

    /**
     *
     * take_categories int Количество категорий для выборки для каждого типа
     *
     * @param Request $request
     * @return static
     */
    public function getTypesAndCategories(Request $request)
    {

        $types = Product_category_type::has('categories')->with(['categories' => function($q) {
            $q->orderBy('priority_1', 'desc')->nPerGroup('type_id', 8);
        }, 'categories.products', 'categories.type'])->orderBy('priority', 'desc')->take(5)->get();


        return $types;

    }

    public function getTypesForMenu(Request $request)
    {

    }
}
