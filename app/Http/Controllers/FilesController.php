<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

use Log;

class FilesController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function getImage(Request $request, $model, $option, $size, $site_id, $filename)
    {

        try {
            $base_path = public_path("files/images/$model/$option/");

            $arr_mode = explode('-', $model);
            $str_model = '\\App\\';
            for($i = 0; $i < count($arr_mode); $i++) {
                $str_model .= ucfirst($arr_mode[$i]);
            }


            $sizes = $str_model::image_sizes[$option][$size];

        } catch(\Exception $e) {

            Log::warning('Error image request: '.$_SERVER['REQUEST_URI']);
            return abort(404);

        }


        if(file_exists($base_path.$filename)) {

            if(!file_exists($base_path.$size)) mkdir($base_path.$size);

            $watermark_path = public_path('files/images/site_watermark/'.$site_id.'-'.$sizes[0].'.png');
            if(!file_exists($watermark_path)) {


                if(file_exists(public_path('files/images/site_watermark/'.$site_id.'.png'))) {
                    Image::make(public_path('files/images/site_watermark/'.$site_id.'.png'))
                        ->resize($sizes[0], null)
                        ->save($watermark_path);
                } else {
                    Image::make(public_path('files/images/site_watermark/0.png'))
                        ->resize($sizes[0], null)
                        ->save($watermark_path);
                }


            }

            $image = Image::make($base_path.$filename)
                ->brightness($site_id)
                ->resize($sizes[0], $sizes[1])
                ->insert($watermark_path,'bottom')
                ->insert($watermark_path,'top')
                ->save($base_path.$size.'/'.$site_id.'-'.$filename);


            $response = \Response::make($image->encode('jpg'));
            $response->header('Content-Type', 'image/jpg');

            return $response;

        } else {

            return abort(404);

        }


    }



}
