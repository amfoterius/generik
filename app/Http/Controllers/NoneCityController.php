<?php

namespace App\Http\Controllers;


use App\Product;
use App\Product_category_image;
use App\Product_category_option_m2m;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Mockery\Exception;
use Sitemap;
use Cache;
use SimpleXMLElement;
use TrueBV\Punycode;
use phpMorphy;
use Healey\Robots\Robots;
use Torann\GeoIP;


use App\Site\Site;
use App\Site\Site_page;
use App\Site\Site_page_m2m;
use App\City;
use App\Product_category;
use App\Product_category_page;
use App\Product_category_page_m2m;
use App\Product_category_type;
use App\Product_category_option;
use App\Product_category_site_m2m;
use App\Review;
use App\Review_option;
use App\Review_option_m2m;
use App\Product_category_page_review;

use Illuminate\Support\Facades\Log;


class NoneCityController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->data['curr_menu'] = 'Каталог';
        $this->data['rating_options'] = Review_option::all();

        $this->data['blocks_cache'] = [];
        $this->data['session_review_votes'] = $request->session()->get('review_likes', []);
        $this->data['session_category_page_review_likes'] = $request->session()->get('category_page_review_likes', []);

        // compare

    }


    public function search(Request $request)
    {
        $this->data['curr_page'] = 'search';
        $this->data['aliases'] = [
            'search_none_city' => ''
        ];
        $this->init();

        $is_redirect = $request->input('isRedirect', 0);

        if ($is_redirect == 0) {
            $matches = [];
            preg_match('/&text=([^&]+)/', $_SERVER['REQUEST_URI'], $matches);
            $new_search = urldecode($matches[1]);
            $req = preg_replace('/&text=([^&]+)/', '&isRedirect=1&text=' . $new_search, $_SERVER['REQUEST_URI']);
            return redirect(substr($req, 1));
        }

        return $this->m_view('search');

    }

    public function start(Request $request)
    {

        if(config('api.is_one_city')) {
            return abort(404);
        }


        $this->data['curr_page'] = 'start';
        $this->data['aliases'] = [
            'start' => ''
        ];
        $this->init();


        $city_name = $request->input('cityName', '');

        if ($city_name == "") {
            $this->data['top_regions'] = City::take(10)->get();
            $this->data['regions'] = City::orderBy('name', 'asc')->get();

            //
            $b = [];
            $bs = [];
            $bukva = "А";

            $this->data['bukvs'][] = $bukva;
            array_push($b, $bukva);
            foreach ($this->data['regions'] as $region) {

                if (($b1 = mb_substr($region->name, 0, 1)) == $bukva) {
                    array_push($b, $region);

                } else {

                    $bukva = $b1;
                    array_push($b, $bukva);
                    array_push($b, $region);

                    array_push($this->data['bukvs'], $bukva);
                }

            }

            $this->data['regions'] = $b;

            // END

            return $this->m_view('start', false);
        } else {
            $this->data['regions'] = City::where('name', 'like', '%' . $city_name . '%')->orderBy('name', 'asc')->get();
            return $this->m_view('start', false);
        }


    }

    public function index(Request $request, $domain)
    {
        $this->data['curr_page'] = 'index';
        $this->data['aliases'] = [
            'index_none_city' => ''
        ];


        $this->data['curr_menu'] = "index";

        $this->init();

        $this->data['categories'] = Product_category::where('type_id', '>', 0)
            ->orderBy('priority', 'desc')->get()->each(function($category, $k) {
                $category->init();
            });

        return $this->m_view('index');

    }

    public function catalog(Request $request, $domain)
    {
        $this->data['curr_page'] = 'catalog';
        $this->data['page_is_catalog'] = 'active';
        $this->data['aliases'] = [
            'catalog_none_city' => ''
        ];

        $this->init();

        // $this->data['categories'] = Product_category::where('type_id', '>', 0)->orderBy('priority', 'desc')->get();

        return $this->m_view('catalog');

    }

    public function type(Request $request, $domain, $type)
    {

        $this->data['curr_page'] = 'type';
        $this->data['page_is_catalog'] = 'active';
        $this->data['aliases'] = [
            'type_none_city' => $type,
        ];

        $this->init();

        $this->data['category_options'] = Product_category_option::orderBy('priority', 'desc')->get();
        return $this->m_view('type');


    }

    public function category(Request $request, $domain, $type, $category)
    {

        if(($r = \App\Libs\My_redirects::category_redirect($this->data['curr_site'], $category)) !== false) {
            return redirect($r);
        }


        $this->data['curr_page'] = 'category';
        $this->data['page_is_catalog'] = 'active';
        $this->data['aliases'] = [
            'type' => $type,
            'category_none_city' => $category,
        ];
        $this->data['curr_category_page'] = null;
        $this->init();

        $this->data['compare_categories'] = Product_category::whereIn(
            'id', $this->data['curr_category']->compare_categories_m2m->pluck('compare_category_id')
        )->get()->prepend($this->data['curr_category']);

        $this->data['curr_category_reviews'] = $this->data['curr_category']->reviews()->where('publish', 1)
            ->where('site_id', $this->data['curr_site']->id)->orderBy('stars', 'desc')->get();

        $this->data['review_options'] = Review_option::all();

        $this->data['blocks_no_cache']['reviews'] = view('components.category_reviews', $this->data)->render();


        // Отбираем опции, если есть запись хоть по одной категории
        $this->data['category_options'] = Product_category_option::where('show_on_category_page', 1)
            ->orderBy('priority', 'desc')->get()->filter(function ($category_option) {

                return \App\Product_category_option_m2m::where('option_id', $category_option->id)->whereIn('category_id', $this->data['compare_categories']->pluck('id'))->count() > 0;

            });


        return $this->m_view('category'.($this->data['curr_category']->buy || $this->data['curr_category']->partners_frame_url ? '' : '_not_buy'));
    }

    public function category_page(Request $request, $domain, $type, $category, $category1, $page)
    {

        $this->data['curr_page'] = 'category_page_none_city';
        $this->data['page_is_catalog'] = 'active';
        $this->data['aliases'] = [
            'type_none_city' => $type,
            'category_none_city' => $category,
            'category1' => $category1,
            'page_none_city' => $page,
        ];

        $this->init();

        $this->data['base_content'] = view(
            array(
                // this actual blade template
                'template'  => $this->data['curr_category_page']->content,
                'cache_key' => 'category_page-'.$this->data['curr_category_page']->id,
                'updated_at' => $this->data['curr_category_page']->updated_at,
            ),
            $this->data

        )->render();

        $this->data['blocks_no_cache']['reviews'] =
            view('components.category_page_reviews', $this->data)->render();


        return $this->m_view('category_page');
    }

    public function delivery(Request $request, $domain)
    {
        $this->data['curr_page'] = 'delivery';
        $this->data['page_is_delivery'] = 'active';

        $this->init();

        // $this->data['settings'] = $this->settings_data('delivery');

        $this->data['curr_menu'] = 'Доставка';
        $this->data['curr_page'] = 'delivery';

        return $this->m_view('delivery');

    }

    public function sales(Request $request, $domain)
    {

        $this->init();

        //$this->data['settings'] = $this->settings_data('sale');

        return $this->m_view('sales', $this->data);

    }

    public function category_review_add(Request $request, $domain, $type, $category)
    {

        $this->data['curr_page'] = 'category_reviews_add_none_city';
        $this->data['aliases'] = [
            'type' => $type,
            'category' => $category,
        ];
        $this->init();


        if ($request->isMethod('post')) {

            $rules = [
                'name' => 'required|max:255|min:3',
                'email' => 'required|email'
            ];

            $rev = new Review();
            $rev->product_id = $this->data['curr_category']->id;
            $rev->name = $request->input('name');
            $rev->email = $request->input('email');
            $rev->positive = $request->input('positive');
            $rev->negative = $request->input('negative');
            $rev->text = $request->input('text');
            $rev->site_id = $request->input('site_id');
            $rev->publish = 0;

            $rev->save();


            $review_options = Review_option::all();

            foreach ($review_options as $review_opt) {
                $rev_opt_m2m = new Review_option_m2m;
                $rev_opt_m2m->review_id = $rev->id;
                $rev_opt_m2m->option_id = $review_opt->id;
                $rev_opt_m2m->stars = $request->input('option-' . $review_opt->id) + 1;
                $rev_opt_m2m->save();


                // $rules['option-'.$review_opt->id] = 'required|numeric|min:-1|max:4';
            }


            /*
             * TODO: доделать валидацию
            $validator = $this->validate($request, $rules);
            if ($validator->fails()) {
                $this->data['errors'] = [];
            }
            */

            return redirect(route('category_none_city', [
                $domain,
                $type,
                $category,
            ]));

        }

        return $this->m_view('review_add', false);

    }

    public function category_page_review_add(Request $request, $domain, $type, $category, $page)
    {

        $this->data['curr_page'] = 'category_reviews_add_none_city';
        $this->data['aliases'] = [
            'type' => $type,
            'category' => $category,
            'page' => $page,
        ];
        $this->init();


        if ($request->isMethod('post')) {

            $rules = [
                'name' => 'required|max:255|min:3',
                'email' => 'required|email'
            ];

            $rev = new Review();
            $rev->product_id = $this->data['curr_category']->id;
            $rev->name = $request->input('name');
            $rev->email = $request->input('email');
            $rev->positive = $request->input('positive');
            $rev->negative = $request->input('negative');
            $rev->text = $request->input('text');
            $rev->publish = 0;

            $rev->save();

            /*
             * TODO: доделать валидацию
            $validator = $this->validate($request, $rules);
            if ($validator->fails()) {
                $this->data['errors'] = [];
            }
            */

            return redirect(route('category_none_city', [
                $domain,
                $type,
                $category,
                $page,
            ]));

        }

        return $this->m_view('review_add', false);

    }

    public function compare(Request $request, $domain)
    {
        $this->data['aliases'] = [
            'compare_none_city' => '',
        ];

        $this->init();

        $this->data['settings'] = $this->settings_data('compare');

        $compare = $request->session()->get('compare', []);
        $this->data['categories'] = Product_category::whereIn('id', $compare)->get();
        $this->data['review_options'] = Review_option::all();

        $this->data['category_options'] = collect();
        $category_options = Product_category_option::all();

        foreach($category_options as $option) {

            if(Product_category_option_m2m::whereIn('category_id', $compare)->where('option_id', $option->id)->count() > 0) {

                $this->data['category_options']->push($option);
            }

        }


        return $this->m_view('compare', false);


    }

    public function contacts(Request $request, $domain)
    {
        $this->data['curr_page'] = 'contacts';
        $this->data['page_is_contacts'] = 'active';

        $this->init();

        $this->data['settings'] = $this->settings_data('contacts');

        $this->data['curr_menu'] = 'Контакты';
        return $this->m_view('contacts', false);
    }

    public function sitemap(Request $request)
    {

        $this->init();
        $categories = Product_category::where('type_id', '>', 0)->get();

        foreach ($categories as $category) {

            Sitemap::addTag(route('category_none_city', [
                $this->data['curr_site']->url,
                $category->type->alias,
                $category->alias,

            ]), $category->updated_at, 'always', '1');


            $category_pages = Product_category_page_m2m::where('category_id', $category->id)->get();


            foreach ($category_pages as $page) {

                Sitemap::addTag(route('category_page_none_city', [
                    $this->data['curr_site']->url,
                    $category->type->alias,
                    $category->alias,
                    $page->page->alias,
                ]), $page->updated_at, 'always', '0.7');

            }


        }

        $types = Product_category_type::all();

        foreach ($types as $type) {

            Sitemap::addTag(route('type_none_city', [
                $this->data['curr_site']->url,
                $type->alias,

            ]), $type->updated_at, 'always', '0.8');
        }

        $base_data = Carbon::today();
        Sitemap::addTag(route('catalog_none_city', [$this->data['curr_site']->url,]), $base_data, 'always', '0.8');
        Sitemap::addTag(route('index_none_city', [$this->data['curr_site']->url,]), $base_data, 'always', '0.6');

        return Sitemap::render();

    }

    public function robots(Request $request)
{
    $Punycode = new \TrueBV\Punycode();

    $this->init();
    header('Content-Type: text/plain; charset=utf-8');
    $robots = new Robots();
    $robots->addUserAgent('*');
    $robots->addHost($_SERVER['HTTP_HOST']);
    $robots->addSitemap(url('sitemap.xml'));
    $robots->addDisallow('/'.$Punycode->encode('корзина'));
    $robots->addDisallow('/'.$Punycode->encode('auth'));
    $robots->addDisallow('/'.$Punycode->encode('ajax'));
    $robots->addDisallow('/'.$Punycode->encode('сравнение'));
    $robots->addDisallow('/'.$Punycode->encode('поиск'));

    /*
    foreach (Product_category::where('type_id', '>', 0)->get() as $category) {
        $robots->addDisallow('/'.$Punycode->encode($category->type->alias).
            '/'.$Punycode->encode($category->alias . '-купить') .
            '/' . $Punycode->encode('добавить-отзыв')
        );

    }
    */

    header("HTTP/1.1 200 OK");
    echo $robots->generate();


}

    public function verification(Request $request, $domain, $key)
    {
        return view('auth.verification', ['key' => $key]);
    }

    public function google_verification(Request $request, $domain, $key)
    {
        return view('auth.google_verification', ['key' => $key]);
    }
}
