<?php

namespace App\Http\Controllers;


use App\Product;
use App\Product_category_image;
use App\Product_category_option_m2m;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Mockery\Exception;
use Sitemap;
use Cache;
use SimpleXMLElement;
use TrueBV\Punycode;
use phpMorphy;
use Healey\Robots\Robots;
use Torann\GeoIP;


use App\Site\Site;
use App\Site\Site_page;
use App\Site\Site_page_m2m;
use App\City;
use App\Product_category;
use App\Product_category_page;
use App\Product_category_page_m2m;
use App\Product_category_type;
use App\Product_category_option;
use App\Product_category_site_m2m;
use App\Review;
use App\Review_option;
use App\Review_option_m2m;
use App\Product_category_page_review;

use Illuminate\Support\Facades\Log;


class OneCityController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->data['curr_menu'] = 'Каталог';
        $this->data['rating_options'] = Review_option::all();

        $this->data['blocks_cache'] = [];
        $this->data['session_review_votes'] = $request->session()->get('review_likes', []);
        $this->data['session_category_page_review_likes'] = $request->session()->get('category_page_review_likes', []);

        // compare

    }

    public function type(Request $request, $domain, $type, $city_p)
    {

        $this->data['curr_page'] = 'type';
        $this->data['page_is_catalog'] = 'active';
        $this->data['aliases'] = [
            'type_none_city' => $type,
        ];

        $this->init();

        $this->data['category_options'] = Product_category_option::orderBy('priority', 'desc')->get();
        return $this->m_view('type');


    }

    public function category(Request $request, $domain, $type, $city_p, $category, $city_p1)
    {

        if(($r = \App\Libs\My_redirects::category_redirect($this->data['curr_site'], $category)) !== false) {
            return redirect($r);
        }


        $this->data['curr_page'] = 'category';
        $this->data['page_is_catalog'] = 'active';
        $this->data['aliases'] = [
            'type' => $type,
            'category_none_city' => $category,
        ];
        $this->data['curr_category_page'] = null;
        $this->init();

        $this->data['compare_categories'] = Product_category::whereIn(
            'id', $this->data['curr_category']->compare_categories_m2m->pluck('compare_category_id')
        )->get()->prepend($this->data['curr_category']);

        $this->data['curr_category_reviews'] = $this->data['curr_category']->reviews()->where('publish', 1)
            ->where('site_id', $this->data['curr_site']->id)->orderBy('stars', 'desc')->get();

        $this->data['review_options'] = Review_option::all();

        $this->data['blocks_no_cache']['reviews'] = view('components.category_reviews', $this->data)->render();


        // Отбираем опции, если есть запись хоть по одной категории
        $this->data['category_options'] = Product_category_option::where('show_on_category_page', 1)
            ->orderBy('priority', 'desc')->get()->filter(function ($category_option) {

                return \App\Product_category_option_m2m::where('option_id', $category_option->id)->whereIn('category_id', $this->data['compare_categories']->pluck('id'))->count() > 0;

            });


        return $this->m_view('category'.($this->data['curr_category']->buy || $this->data['curr_category']->partners_frame_url ? '' : '_not_buy'));
    }

    public function category_page(Request $request, $domain, $type, $city_p, $category, $city_p1, $page, $city_p2)
    {

        $this->data['curr_page'] = 'category_page_none_city';
        $this->data['page_is_catalog'] = 'active';
        $this->data['aliases'] = [
            'type_none_city' => $type,
            'category_none_city' => $category,
            'city_p1' => $city_p1,
            'city_p2' => $city_p2,
            'page_none_city' => $page,
        ];

        $this->init();

        $this->data['base_content'] = view(
            array(
                // this actual blade template
                'template'  => $this->data['curr_category_page']->content,
                'cache_key' => 'category_page-'.$this->data['curr_category_page']->id,
                'updated_at' => $this->data['curr_category_page']->updated_at,
            ),
            $this->data

        )->render();

        $this->data['blocks_no_cache']['reviews'] =
            view('components.category_page_reviews', $this->data)->render();


        return $this->m_view('category_page');
    }

    public function category_review_add(Request $request, $domain, $type, $city_p, $category, $city_p1)
    {

        $this->data['curr_page'] = 'category_reviews_add_none_city';
        $this->data['aliases'] = [
            'type' => $type,
            'category' => $category,
        ];
        $this->init();


        if ($request->isMethod('post')) {

            $rules = [
                'name' => 'required|max:255|min:3',
                'email' => 'required|email'
            ];

            $rev = new Review();
            $rev->product_id = $this->data['curr_category']->id;
            $rev->name = $request->input('name');
            $rev->email = $request->input('email');
            $rev->positive = $request->input('positive');
            $rev->negative = $request->input('negative');
            $rev->text = $request->input('text');
            $rev->site_id = $request->input('site_id');
            $rev->publish = 0;

            $rev->save();


            $review_options = Review_option::all();

            foreach ($review_options as $review_opt) {
                $rev_opt_m2m = new Review_option_m2m;
                $rev_opt_m2m->review_id = $rev->id;
                $rev_opt_m2m->option_id = $review_opt->id;
                $rev_opt_m2m->stars = $request->input('option-' . $review_opt->id) + 1;
                $rev_opt_m2m->save();


                // $rules['option-'.$review_opt->id] = 'required|numeric|min:-1|max:4';
            }


            /*
             * TODO: доделать валидацию
            $validator = $this->validate($request, $rules);
            if ($validator->fails()) {
                $this->data['errors'] = [];
            }
            */

            return redirect(route('category_none_city', [
                $domain,
                $type,
                $category,
            ]));

        }

        return $this->m_view('review_add', false);

    }

    public function category_page_review_add(Request $request, $domain, $type, $city_p, $category, $city_p1, $page, $city_p2)
    {

        $this->data['curr_page'] = 'category_reviews_add_none_city';
        $this->data['aliases'] = [
            'type' => $type,
            'category' => $category,
            'page' => $page,
        ];
        $this->init();


        if ($request->isMethod('post')) {

            $rules = [
                'name' => 'required|max:255|min:3',
                'email' => 'required|email'
            ];

            $rev = new Review();
            $rev->product_id = $this->data['curr_category']->id;
            $rev->name = $request->input('name');
            $rev->email = $request->input('email');
            $rev->positive = $request->input('positive');
            $rev->negative = $request->input('negative');
            $rev->text = $request->input('text');
            $rev->publish = 0;

            $rev->save();

            /*
             * TODO: доделать валидацию
            $validator = $this->validate($request, $rules);
            if ($validator->fails()) {
                $this->data['errors'] = [];
            }
            */

            return redirect(route('category_none_city', [
                $domain,
                $type,
                $category,
                $page,
            ]));

        }

        return $this->m_view('review_add', false);

    }

}
