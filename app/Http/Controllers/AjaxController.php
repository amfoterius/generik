<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class AjaxController extends Controller
{

    public function cart(Request $request)
    {
        $method = $request->input('action');

        if(!$request->session()->has('cart'))
        {
            $request->session()->put('cart', [

                'name' => '',
                'tel' => '',
                'post_address' => '',

                // id => count
                'products' => []

            ]);

        }

        switch($method)
        {
            case 'add':
                $product = 'cart.products.'.$request->input('product_id');
                if($request->session()->has($product))
                {
                    $request->session()->put($product, $request->session()->get($product) + $request->input('product_count'));

                }
                else
                {
                    $request->session()->put($product, $request->input('product_count'));

                }


                return response($request->session()->get('cart'));

                break;




        }

    }

}
