<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review_option_m2m extends Model
{
    protected $table = 'review_option_m2m';

    public function review()
    {
        return $this->belongsTo('App\Review', 'review_id');
    }

    public function option()
    {
        return $this->belongsTo('App\Review_option', 'option_id');
    }
}
