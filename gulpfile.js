var elixir = require('laravel-elixir');
var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

gulp.task('minify-css', function() {
    return gulp.src('public/css/app.css')
        .pipe(minifyCss())
        .pipe(gulp.dest('public/min-css'));
});

elixir(function(mix) {
    mix.coffee(['app.coffee']);
});



elixir(function(mix) {

    mix.less('app.less');
    mix.copy('public/css/app.css', 'public/css/app0.css');

});




elixir(function(mix) {
    mix.less('apps/app6.less');


    /*
    mix.less('apps/app1.less');

    mix.less('apps/app2.less');

    mix.less('apps/app3.less');

    mix.less('apps/app4.less');

    mix.less('apps/app5.less');

    mix.less('apps/app6.less');

    mix.less('apps/app7.less');

    mix.less('apps/app8.less');

    mix.less('apps/app9.less');

    mix.less('apps/app10.less');

    mix.less('apps/app11.less');

    mix.less('apps/app12.less');

    mix.less('apps/app13.less');

    mix.less('apps/app14.less');

    mix.less('apps/app15.less');

    mix.less('apps/app16.less');
    */


});






