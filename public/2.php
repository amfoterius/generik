<?php


/**
 * Возвращает элемент массива по состоянию $curr_position
 * @param $arr
 * @param $curr_position
 * @return bool
 */
function el_on_lvl_position(&$arr, &$curr_position)
{
    if (count($curr_position) == 0) {
        return $arr;
    }

    $result = $arr[$curr_position[0]];


    for ($k = 1; $k < count($curr_position); $k++) {
        if (array_key_exists($curr_position[$k], $result)) {
            $result = $result[$curr_position[$k]];

        } else {
            return false;

        }

    }

    return $result;

}

/**
 * Функция, которая двигает маховики... и курсор
 * @param $arr
 * @param $curr_position
 * @param $max_level
 * @return bool|int
 */
function for_test(&$arr, &$curr_position, &$max_level)
{


    // Если мы на границе тещкуего массива, ставим метку на нулевой индекс
    if(count(el_on_lvl_position($arr, array_slice($curr_position, 0, -1))) - 1 == $curr_position[count($curr_position) - 1]) {
        if(count($curr_position) == 1)
        {
            $a = 0;
        }

        $link = &$arr;
        for($k = 0; $k < count($curr_position)-1; $k++) {
            $link = &$link[$curr_position[$k]];
        }

        $n = array_slice($curr_position, 0, -1);
        $n[] = 0;

        array_unshift($link, '%' . implode('-', $n) . '%');

        $curr_position[count($curr_position) - 1] += 1;

    }

    // Если элемента с такой позицией нет
    if(!el_on_lvl_position($arr, $curr_position)) {
        $curr_position = array_slice($curr_position, 0, -1);
        $curr_position[count($curr_position) - 1] += 1;

    }

    // Если элемента массив, и в нет нет хлебн крошек
    if (is_array(el_on_lvl_position($arr, $curr_position)) && !preg_match('/%[^%]+%/', el_on_lvl_position($arr, $curr_position)[0])) {
        $curr_position[] = 0;

    }
    // Если есть хлебные крошки, возвращаем flase
    elseif (is_array(el_on_lvl_position($arr, $curr_position)) && preg_match('/%[^%]+%/', el_on_lvl_position($arr, $curr_position)[0])) {
        return false;

    }
    // Если элемент не массив и есть куда двигаться
    elseif (array_key_exists($curr_position[count($curr_position) - 1] + 1, el_on_lvl_position($arr, array_slice($curr_position, 0, -1)))) {
        $curr_position[count($curr_position) - 1] += 1;
    }
    else {

        if(count($curr_position) == 1)
        {
            return -1;
        }





    }



    // меняем уровень, если надо
    if (count($curr_position) > $max_level) {
        $max_level = count($curr_position);
    }

    return true;


}

// Вспомогательная функция
function f(&$arr, &$curr_position, &$max_level)
{
    while (1) {
        $r = for_test($arr, $curr_position, $max_level);
        if ($r === -1) {
            return $max_level;

        } elseif($r === false) {
            return false;

        } else {
            continue;
        }



    }

}


// Демо

$arr1 = [];


$arr2 = ['1', '2', '3', &$arr1];
$arr1 = ['a', 'b', 'c', &$arr2];

$arr3 = [

    [
        1,
        1,
        1,
    ],
    1,
    1,
    [
        1,
        1,
        [
            1,
        ]
    ]


];
$map_array = [];
$max_level = 0;
$curr_position = [0];




echo f($arr1, $curr_position, $max_level);


