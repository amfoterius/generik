(function() {
  $(function() {
    $.ajax({
      url: URL + 'ajax/compare/getOnCompare',
      method: 'GET',
      success: function(data) {
        var j, len, n, results;
        results = [];
        for (j = 0, len = data.length; j < len; j++) {
          n = data[j];
          $("#compare-" + n).addClass("hide");
          results.push($("#compare-" + n + "-active").removeClass("hide"));
        }
        return results;
      }
    });
    $(".add-to-cart").on("click", function(e) {
      var c, el;
      c = $("#cart-count").text();
      $("#on-cart-img").attr("src", "");
      el = $(this);
      $("#cart-count, #cart-count-mobile").text(parseInt(c) + 1).animate({
        "font-size": "25px",
        "background-color": "#5DBA5D"
      }, 500).animate({
        "font-size": "12px",
        "background-color": "#777777"
      }, 100);
      return $.ajax({
        url: URL + 'ajax/cart/add',
        method: 'POST',
        data: {
          "product_id": $(this).data('id'),
          "product_count": 1,
          "is_one_click": 0
        },
        success: function(data) {
          var i, p, ref, summ;
          $("#on-cart-img").attr("src", URL + "uploads/api/images/categories/" + el.data("img-src"));
          $("#on-cart-name").text(el.data("name"));
          $("#on-cart-kolvo").text(el.data("kolvo"));
          $("#on-cart-price").text(el.data("price"));
          summ = 0;
          ref = data.products;
          for (i in ref) {
            p = ref[i];
            summ = summ + parseInt(p);
          }
          return $("#on-carl-length").text(summ);
        }
      });
    });
    $(".add-to-cart-product").on("click", function(e) {
      var c, el;
      c = $("#cart-count").text();
      $("#on-cart-img").attr("src", "");
      el = $(this);
      $("#cart-count, #cart-count-mobile").text(parseInt(c) + 1).animate({
        "font-size": "25px",
        "background-color": "#5DBA5D"
      }, 500).animate({
        "font-size": "12px",
        "background-color": "#777777"
      }, 100);
      return $.ajax({
        url: URL + 'ajax/cart/add',
        method: 'POST',
        data: {
          "product_id": $(this).data('id'),
          "product_count": 1,
          "is_one_click": 0
        },
        success: function(data) {
          var i, p, ref, summ;
          $("#on-cart-img").attr("src", URL + "uploads/api/images/categories/" + el.data("img-src"));
          $("#on-cart-name").text(el.data("name"));
          $("#on-cart-kolvo").text(el.data("kolvo"));
          $("#on-cart-price").text(el.data("price"));
          summ = 0;
          ref = data.products;
          for (i in ref) {
            p = ref[i];
            summ = summ + parseInt(p);
          }
          return $("#on-carl-length").text(summ);
        }
      });
    });
    $(".remove-on-cart").on("click", function(e) {
      var tr;
      tr = $(this).parent().parent();
      return $.ajax({
        url: URL + 'ajax/cart/remove',
        method: 'POST',
        data: {
          "product_id": $(this).data('product-id')
        },
        success: function() {
          var summDiscount;
          tr.remove();
          summ -= parseInt(tr.find('td .summ').text());
          summDiscount = summ * 0.05;
          $("#summ-discount").text(summDiscount);
          if ($('.payment input:checked').data('discount') === 1) {
            $("#row-discount").removeClass('hide');
            $('#cart-summ').text(summ - summDiscount);
          } else {

          }
          $("#row-discount").addClass('hide');
          return $('#cart-summ').text(summ);
        }
      });
    });
    $("#select-delivery").change(function(e) {
      var dId, isSelect;
      isSelect = false;
      dId = parseInt($(this).val());
      $("#select-billing").show();
      return $("#select-billing option").each(function(index) {
        if (parseInt($(this).data('delivery-id')) === dId) {
          if (!isSelect) {
            $(this).attr('selected', true);
            isSelect = true;
          }
          return $(this).show();
        } else {
          return $(this).hide();
        }
      });
    });
    $("#form-order").on('submit', function(e) {
      var error, mess;
      mess = '';
      error = false;
      if ($("input[name='fio']").val() === '') {
        $("input[name='fio']").css('border-color', 'red');
        mess += 'Не заполнено поле ФИО<br>';
      } else {
        $("input[name='fio']").css('border-color', 'rgb(204, 204, 204)');
      }
      if ($("input[name='tel']").val() === '') {
        $("input[name='tel']").css('border-color', 'red');
        mess += 'Не заполнено поле ФИО<br>';
      } else {
        $("input[name='tel']").css('border-color', 'rgb(204, 204, 204)');
      }
      if (error) {
        e.preventDefault();
        $("#modal-error-text").html(mess);
        $("#modal-error").modal('show');
        return false;
      }
    });
    $(".buy-for-one-click").on("click", function(e) {
      var modal;
      modal = $("#buy-for-one-click");
      if (parseInt($(this).data('id')) > 0) {
        $("#buy-for-one-click").find(".input-product-id").val($(this).data('id'));
        return $("#buy-for-one-click").find(".product-name").text($(this).data('name') + ", " + $(this).data('kolvo'));
      }
    });
    $(".next-chunk").on("click", function(e) {
      return $(this).hide();
    });
    $('#button-new-order').click(function() {
      var msg;
      msg = $('#form-order').serialize();
      return $.ajax({
        url: URL + 'ajax/cart/new_order',
        method: 'POST',
        data: msg
      });
    });
    $(".compare").on("click", function(e) {
      var c, el, linkCompare;
      if ($(this).hasClass('active')) {
        c = $("#compare-count").text();
        $('#compare-' + $(this).data('id') + '-active').addClass('hide');
        $('#compare-' + $(this).data('id')).removeClass('hide');
        $("#compare-count, #compare-count-mobile").text(parseInt(c) - 1).animate({
          "font-size": "25px",
          "background-color": "#5DBA5D"
        }, 500).animate({
          "font-size": "12px",
          "background-color": "#777777"
        }, 100);
        return $.ajax({
          url: URL + 'ajax/compare/remove',
          method: 'POST',
          data: {
            id: $(this).data('id')
          }
        });
      } else {
        c = $("#compare-count").text();
        linkCompare = URL + "сравнение";
        $('#compare-' + $(this).data('id') + '-active').removeClass('hide');
        $('#compare-' + $(this).data('id')).addClass('hide');
        el = $(this);
        $("#compare-count, #compare-count-mobile").text(parseInt(c) + 1).animate({
          "font-size": "25px",
          "background-color": "#5DBA5D"
        }, 500).animate({
          "font-size": "12px",
          "background-color": "#777777"
        }, 100);
        return $.ajax({
          url: URL + 'ajax/compare/add',
          method: 'POST',
          data: {
            id: $(this).data('id')
          },
          success: function(data) {
            var imgUrl, name;
            imgUrl = URL + "uploads/api/images/categories/" + el.data("img-src");
            name = el.data('name');
            el = "<div class='alert alert-info alert-dismissable'> <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <img src='" + imgUrl + "' alt='' style='width: 50px'> Вы добавили к сравнению " + name + " </div>";
            $(el).prependTo("#alerts").animate({
              opacity: "hide",
              height: "hide"
            }, 3000);
            return console.log(el);
          }
        });
      }
    });
    $('.payment').on("click", function(e) {
      $('.payment input').prop('checked', false);
      $(this).find('input').prop('checked', true);
      if ($('.payment input:checked').data('discount') === 1) {
        $("#row-discount").removeClass('hide');
        return $('#cart-summ').text(summ - summDiscount);
      } else {
        $("#row-discount").addClass('hide');
        return $('#cart-summ').text(summ);
      }
    });
    $('.review-vote').on('click', function(e) {
      var count;
      count = parseInt($(this).data('count'));
      if ($(this).hasClass('btn-success')) {
        $(this).removeClass('btn-success').addClass('btn-default');
        count -= 1;
      } else {
        $(this).removeClass('btn-default').addClass('btn-success');
        count += 1;
      }
      $(this).data('count', count).find('.count').text(count);
      return $.ajax({
        url: URL + 'ajax/review/vote?review_id=' + $(this).data('review'),
        method: 'GET',
        success: function(data) {
          return console.log(data);
        }
      });
    });
    $('.review-category-page-vote').on('click', function(e) {
      var count;
      count = parseInt($(this).data('count'));
      console.log($(this).data('count'));
      if ($(this).hasClass('btn-success')) {
        $(this).removeClass('btn-success').addClass('btn-default');
        count -= 1;
      } else {
        $(this).removeClass('btn-default').addClass('btn-success');
        count += 1;
      }
      $(this).data('count', count).find('.count').text(count);
      return $.ajax({
        url: URL + 'ajax/review/vote_category_page?review_id=' + $(this).data('review'),
        method: 'GET',
        success: function(data) {
          return console.log(data);
        }
      });
    });
    return $('#new-review-page-add').on('click', function(e) {
      return $.ajax({
        url: URL + 'ajax/review/add_review_category_page',
        method: 'POST',
        data: {
          category_page_id: $(this).data('category-page-id'),
          site_id: $(this).data('site-id'),
          name: $("#new-review-page-name").val(),
          text: $("#new-review-page-text").val()
        },
        success: function(data) {
          $("#new-review-page-name").val('');
          $("#new-review-page-text").val('');
          return $("#reviews").prepend(data.block);
        }
      });
    });
  });

}).call(this);

//# sourceMappingURL=app.js.map
