<?php

use Illuminate\Database\Seeder;

class BillingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $billing_types = json_decode(file_get_contents(config('api.billing_types_url')));

        foreach ($billing_types as $delivery_id => $billing_group) {
            foreach ($billing_group as $billing_type_id => $billing) {

                DB::table('billing_types')->insert([
                    'api_id' => $billing_type_id,
                    'name' => $billing->title,
                    'delivery_id' => 1,
                    'rekvizity' => $billing->rekvizity,
                    'img' => $billing->img,
                    'discount' => $billing->discount,
                ]);

            }

        }

    }
}
