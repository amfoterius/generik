<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = json_decode(file_get_contents(config('api.categories_url')));
        $products = json_decode(file_get_contents(config('api.products_url')));


        foreach($categories as $category_id => $category)
        {

            if(config('api.push_images'))
            {
                file_put_contents(
                    config('api.categories_images_dir') . mb_substr($category->img_small, 1),
                    file_get_contents(config('api.categories_images_url') . mb_substr($category->img_small, 1))
                );
                file_put_contents(
                    config('api.categories_images_dir') . mb_substr($category->img_big, 1),
                    file_get_contents(config('api.categories_images_url') . mb_substr($category->img_big, 1))
                );

            }

            DB::table('product_categories')->insert([
                'api_id' => $category_id,
                'name' => $category->title,
                'subtitle' => $category->subtitle,
                'description' => $category->description,
                'img_small' => mb_substr($category->img_small, 1),
                'img_big' => mb_substr($category->img_big, 1),
                'min_price' => $category->minprice,
                'max_price' => isset($category->maxprice) ? $category->maxprice : $category->minprice,

            ]);

        }




        foreach ($products as $category_id => $products_category) {
            foreach ($products_category as $api_key => $product) {

                if(config('api.push_images'))
                {
                    file_put_contents(
                        config('api.products_images_dir') . mb_substr($product->img, 1),
                        file_get_contents(config('api.categories_images_url') . mb_substr($product->img, 1))
                    );
                }


                DB::table('products')->insert([
                    'api_id' => $api_key,
                    'category_id' => $category_id,
                    'name' => $product->title_big,
                    'description' => $product->description,
                    'kolvo' => $product->kolvo,
                    'price_per_tablet' => $product->price_per_tablet,
                    'price_per_item' => $product->price_per_item,
                    'price' => $product->price,
                    'image' => mb_substr($product->img, 1),
                    'video' => $product->video,
                ]);
            }
        }


    }
}
