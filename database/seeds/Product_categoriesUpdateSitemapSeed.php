<?php

use Illuminate\Database\Seeder;
use App\Product_category;
use App\Product_category_type;

class Product_categoriesUpdateSitemapSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach (Product_category::where('type_id', '>', 0)->get() as $category) {

            $category->updated_at = Carbon::now();
            $category->save();

        }


    }
}
