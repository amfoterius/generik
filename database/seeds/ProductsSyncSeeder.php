<?php

use Illuminate\Database\Seeder;

use App\Product;
use App\Product_category;

class ProductsSyncSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = json_decode(file_get_contents(config('api.categories_url')));
        $products = json_decode(file_get_contents(config('api.products_url')));


        foreach($categories as $category_id => $category)
        {
            $curr_product_category = Product_category::where('api_id', $category_id)->first();

            if($curr_product_category != null)
            {
                $curr_product_category->min_price = $category->minprice;
                $curr_product_category->max_price = isset($category->maxprice) ? $category->maxprice : $category->minprice;
                $curr_product_category->save();

                Log::info('Update product category: '.$category_id);


            }
            else
            {
                DB::table('product_categories')->insert([
                    'api_id' => $category_id,
                    'name' => $category->title,
                    'subtitle' => $category->subtitle,
                    'description' => $category->description,
                    'img_small' => mb_substr($category->img_small, 1),
                    'img_big' => mb_substr($category->img_big, 1),
                    'min_price' => $category->minprice,
                    'max_price' => isset($category->maxprice) ? $category->maxprice : $category->minprice,

                ]);

            }

        }




        foreach ($products as $category_id => $products_category) {

            foreach ($products_category as $api_key => $product) {

                $curr_product = Product::where('api_id', $api_key)->first();

                if($curr_product != null)
                {
                    $curr_product->kolvo = $product->kolvo;
                    $curr_product->price_per_tablet = $product->price_per_tablet;
                    $curr_product->price_per_item = $product->price_per_item;
                    $curr_product->price = $product->price;
                    $curr_product->save();

                    Log::info('Update product category: '.$category_id);


                }
                else
                {
                    DB::table('products')->insert([
                        'api_id' => $api_key,
                        'category_id' => $category_id,
                        'name' => $product->title_big,
                        'description' => $product->description,
                        'kolvo' => $product->kolvo,
                        'price_per_tablet' => $product->price_per_tablet,
                        'price_per_item' => $product->price_per_item,
                        'price' => $product->price,
                        'image' => mb_substr($product->img, 1),
                        'video' => $product->video,
                    ]);

                    Log::info('Add category: '.$product->title_big);

                }
            }
        }



    }
}
